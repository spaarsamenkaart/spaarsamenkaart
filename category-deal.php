<?php
require_once 'application-top.php';
require_once 'includes/navigation-functions.php';
function fetchCatname($catId) {
    global $db;
    $srch = new SearchBase('tbl_deal_categories');
    $srch->addCondition('cat_id', '=', $catId);
    $srch->addMultipleFields(array('cat_id', 'cat_name'));
    $rs = $srch->getResultSet();
    if (!$row = $db->fetch($rs))
        return false;
    return $row;
}

$get = getQueryStringData();
$category['type']= $get['type'];
if (is_numeric($get['cat'])) {
    $catName = $db->query("select * from tbl_deal_categories where cat_id=" . $get['cat']);
    $catrow = $db->fetch($catName);
    $cat_name = $catrow['cat_name' . $_SESSION['lang_fld_prefix']];
    $cat_code = $catrow['cat_code'];
    $cat_parent_id = $catrow['cat_parent_id'];
    /* $srch = new SearchBase('tbl_deal_to_category', 'dtc');
    //$srch->addCondition('dtc.dc_cat_id', '=', $get['cat']);
    $srch->joinTable('tbl_deal_categories', 'INNER JOIN', 'dtc.dc_cat_id=c.cat_id ', 'c');
    $srch->addCondition('c.cat_parent_id', '=', $catrow['cat_parent_id'] );

    $rs = $srch->getResultSet();
    $dealArrCat = array();

    while ($row = $db->fetch($rs)) {
        $dealArrCat[] = $row['dc_deal_id'];
        $categoryName = $row['cat_name'];
        $cat_layout = $row['cat_layout'];
    } */
}


require_once 'header.php';
?>
<!--bodyContainer start here-->

 <section class="pagebar">
    <div class="fixed_container">
        <div class="row">
           <aside class="col-md-7 col-sm-7">
                <ul class="breadcrumb">
                   <li><a href="<?php echo friendlyUrl(CONF_WEBROOT_URL);?>"><?php echo t_lang('M_TXT_HOME');?></a></li>
                   <li><a href="<?php echo friendlyUrl(CONF_WEBROOT_URL.'categories.php');?>"><?php echo t_lang('M_TXT_CATEGORIES');?></a></li>
                   <?php if( $parentcat = fetchCatname($cat_parent_id)){?>
                   <li><a href="<?php echo friendlyUrl(CONF_WEBROOT_URL . 'category-deal.php?cat=' . $parentcat['cat_id'] . '&type='.$category['type']);?>"><?php echo $parentcat['cat_name']; ?></a></li>
                   <?php } ?>
                   <li><?php $cat= fetchCatname($get['cat']); echo $cat['cat_name'];  ?></li>
               
                </ul>
                
               
            </aside>
            <aside class="col-md-5 col-sm-5 side">
                <?php require_once CONF_VIEW_PATH.'sort-filter-menu.php'; ?>
            </aside>
            
        </div>
     </div>
</section> 
<section class="page__container">
    <div class="fixed_container">
        <div class="row">
           <?php require_once CONF_VIEW_PATH.'left-filter-menu.php'; ?>
            <aside class="col-md-9">
                
                <div class="row__filter right_bar" style="display:none;">
                    <div class="row " >
                        <aside class="col-md-9 col-sm-9">
                            <ul class="tags__filter" id="filter" >
                                <li><?php echo t_lang('M_TXT_SHOW'); ?></li>
                                
                            </ul>
                        </aside>
                        <aside class="col-md-3 col-sm-3 alignright">
                            <ul class="tags__filter">
                                <li class="clear" id="allfilter"><a href="javascript:void(0);" onclick="removeFilter(this)" ><?php echo t_lang('M_TXT_CLEAR_ALL'); ?></a></li>
                            </ul>
                        </aside>
                    </div>
                </div>
                
                
                <!--items list start here-->
                
                <div class="dealsContainer">
                    
                </div>
                
                <!--items list end here-->
                
  
                
               
            </aside>
        </div>
    </div>    
</section>



<!--bodyContainer end here-->

<script>
 $(document).ready(function () {
        getalldeals(1);
        $('#topcontrol').fadeOut();
    });
 

   
    
</script>


<script type="text/javascript">
    $(document).ready(function () {
        $('.tabLink').click(function () {
            $(this).toggleClass("active");

            $('.sectiondowncontainer').slideToggle("600");
            return false;
        });
        $('html').click(function () {
            $('.sectiondowncontainer').slideUp('slow');
            if ($('.tabLink').hasClass('active')) {
                $('.tabLink').removeClass('active');
            }
        });
    });
</script>


<?php
include 'footer.php';
?>
     
