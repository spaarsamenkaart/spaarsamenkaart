<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td style="width:440px">
                <a href="#" target="_blank"><img width="100%" height="300" src="{{DEAL_IMAGE}}" style="display:block;border:1px solid #ddd" class="CToWUd">
                </a>
            </td>
            <td style="width:15px"></td>
            <td valign="top">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td style="font-size:22px;color:#333;padding:0 0 10px 0">
                                <a style="font-size:22px;color:#333;text-decoration:none" href="{{DEAL_URL}}" target="_blank"> {{DEAL_NAME}}</a>
                            </td>
                        </tr>
                        <tr>
                            <td style="font-size:14px;line-height:18px;color:#333;padding:0 0 15px 0">{{DEAL_NAME}}</td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" style="border:1px solid #ddd;border-collapse:collapse">
                                    <tbody>
                                        <tr>
                                            <td width="33%" style="border:1px solid #ddd;font-size:18px;color:#333;font-weight:bold;border-collapse:collapse;padding:10px;text-align:center">{{DEAL_SAVING}}
                                                <span style="display:block;font-weight:normal;font-size:13px;color:#666">{{TEXT_SAVING}}
                                                </span>
                                            </td>

                                            <td width="33%" style="border:1px solid #ddd;font-size:18px;color:#333;font-weight:bold;border-collapse:collapse;padding:10px;text-align:center">{{DEAL_ORIGNAL_PRICE}} <span style="display:block;font-weight:normal;font-size:13px;color:#666">{{TEXT_VALUE}}</span>
                                            </td>
                                            <td width="33%" style="border:1px solid #ddd;font-size:18px;color:#333;font-weight:bold;border-collapse:collapse;padding:10px;text-align:center">{{DEAL_OFF}}<span style="display:block;font-weight:normal;font-size:13px;color:#666">{{TEXT_OFF}}</span>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding:15px 0;font-size:24px;color:#009eba"><del style="color:#666;padding:0 15px 0 0">{{DEAL_ORIGNAL_PRICE}}</del>{{DEAL_OFFER_PRICE}}
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <a href="{{DEAL_URL}}" style="display:inline-block;padding:10px 25px;background:#cf1e36;text-transform:uppercase;text-decoration:none;color:#fff;font-size:14px;font-weight:600" target="_blank">{{TEXT_VIEW_OFFER}}</a>
                            </td>
                        </tr>

                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>