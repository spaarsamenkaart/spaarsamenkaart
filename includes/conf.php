<?php
/**
 * General sitewide configurations file
 */
/**
 * 
 * Sitewide configurations
 */
error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING & ~E_DEPRECATED);
define('CONF_DEVELOPMENT_MODE', false);
define('CONF_LIB_HALDLE_ERROR_IN_PRODUCTION', true);


define('CONF_MAIN_APP_URL', $_SERVER['SERVER_NAME']);

require_once __DIR__ . '/config.php';
//define('CONF_DB_BACKUP_DIRECTORY_FULL_PATH', '/home/xxxxxxx/database.bak.dailydeals'); /* Please create this directory manually out of public_html/webroot folder */

define('GOOGLE_MAP_KEY','AIzaSyBdEUD46utZt4WFcifCgWgy03b_ToJa6DU');

define('CONF_INSTALLATION_PATH', $_SERVER['DOCUMENT_ROOT'] . CONF_WEBROOT_URL);
$_SESSION['WYSIWYGFileManagerRequirements'] = realpath(dirname(__FILE__) . '/WYSIWYGFileManagerRequirements.php');
#Editor
$_SESSION['path_for_images'] = CONF_WEBROOT_URL.'images';
define('CONF_DATE_FIELD_TRIGGER_IMG', CONF_WEBROOT_URL . 'images/calender.png');
define('CONF_HTML_EDITOR', 'innova');
define('CONF_CKEDITOR_PATH', CONF_WEBROOT_URL . 'innova-lnk');
// ["group3", "", ["LinkDialog", "ImageDialog",  "TableDialog", "Emoticons"]],
$innova_settings = array(
    'width' => '"100%"',
    'height' => '500',
	'enableFlickr' => 'false',
    'enableYoutube' => 'false',
    'css' => '"' . CONF_WEBROOT_URL . 'innova-lnk/styles/default.css"',
	'fileBrowser' => '"' . CONF_WEBROOT_URL . 'innova-lnk/assetmanager/asset.php"',
    'groups' => '[
                        ["group1", "", ["Bold", "Italic" , "Underline", "FontDialog", "ForeColor", "TextDialog", "RemoveFormat", "Paragraph"]],
                        ["group2", "", ["Bullets", "Numbering", "JustifyLeft", "JustifyCenter", "JustifyRight"]],
						["group3", "", ["ImageDialog",  "YoutubeDialog"]],
                        ["group5", "", ["Undo", "Redo", "SourceDialog"]]
                        ]', 
    );
define('CONF_DIGITAL_PRODUCTS_PATH', $_SERVER['DOCUMENT_ROOT'].'/digital-uploads/');
define('CONF_VIEW_PATH', CONF_INSTALLATION_PATH.'includes/partial-views/');
define('CONF_FREE_WALLET_CREDIT_SCHEME', 1);
define('CONF_FREE_WALLET_CREDIT_AMOUNT', 100);
define('CONF_FREE_WALLET_CREDIT_SCHEME_ACTIVE_TILL', '');
$arr_deal_status=array('Upcoming', 'Open', 'Ended', 'Cancelled', 'Paid to Company','Pending Approval','Rejected');
$arr_nav_type=array(0=>'CMS page', 1=>'Custom HTML', 2=>'Exteranl URL');
$arr_email_notification=array(0=>'Near to Expire', 1=>'Earned Deal Buck', 2=>'Friend Buy Deal');

$arr_user_status=array('1'=>'Active','0'=>'Inactive');
$arr_sale_earning=array('1'=>'Upto 1,000','5'=>'1,000 to 5,000','25'=>'5,001 to 25,000','50'=>'25,001 to 50,000','100'=>'50,001 to 100,000','1000'=>'Above 100,000');
?>