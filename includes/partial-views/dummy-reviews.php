<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">L</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Lovish  </h3>
        <span class="datetxt">November 9, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Love it</p>
        </div>    
    </aside>
</div>

<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">A</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Avi  </h3>
        <span class="datetxt">November 16, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Awesome Quality at Amazing price</p>
        </div>    
    </aside>
</div>

<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">B</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Bhuvan  </h3>
        <span class="datetxt">November 19, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Best of all</p>
        </div>    
    </aside>
</div>

<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">F</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Fb User  </h3>
        <span class="datetxt">November 22, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Fabulous </p>
        </div>    
    </aside>
</div>

<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">D</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Donald  </h3>
        <span class="datetxt">November 29, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Din't expected it to be so good</p>
        </div>    
    </aside>
</div>
<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">Q</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Queen  </h3>
        <span class="datetxt">November 29, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Quality delivered</p>
        </div>    
    </aside>
</div>
<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">S</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Sachin  </h3>
        <span class="datetxt">November 29, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Satisfactory</p>
        </div>    
    </aside>
</div>
<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">V</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Vicky  </h3>
        <span class="datetxt">November 29, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Value for money</p>
        </div>    
    </aside>
</div>
<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">D</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Donald  </h3>
        <span class="datetxt">November 29, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Din't expected it to be so good</p>
        </div>    
    </aside>
</div>
<div class="listrepeated">
    <aside class="grid_1">
        <figure class="avtar">F</figure>
    </aside>
    <aside class="grid_2">
        <div class="ratingwrap">
            <div class="ratings"><ul><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-full.png" alt=""></li><li><img src="/images/rating-zero.png" alt=""></li></ul> </div>
        </div>
        <h3 class="name">Five   </h3>
        <span class="datetxt">November 29, 2016  10:13 am</span>
        <div class="reviewsdescription">
        <p>Five Stars </p>
        </div>    
    </aside>
</div>
