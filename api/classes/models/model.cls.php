<?php
class ModelClass {
	protected $Api = null;
	protected $db;
	protected $default_lang = '';
	public function __construct($api){
		global $db;
		$this->db = $db;
		$this->Api = $api;
                $this->default_lang = CONF_DEFAULT_LANGUAGE;
	}
	
	final protected function prepareSuccessResponse($data){
		$data = $this->cleanOutput($data);
		if(!is_array($data) || !array_key_exists('message', $data)){
			$data = array('message'=>$data);
		}
                return array('status'=>1, 'content'=>$data,'default_lang'=>$this->default_lang);
	}
	
	final protected function prepareErrorResponse($data){
		$data = $this->cleanOutput($data);
		if(!is_array($data) || !array_key_exists('message', $data)){
			$data = array('message'=>$data);
		}
                return array('status'=>0, 'content'=>$data,'default_lang'=>$this->default_lang);
	}
	
	final protected function cleanOutput($data){
        if(is_array($data)){
			$cleaned_data = array();
            foreach($data as $k => $v){
                $cleaned_data[$k] = $this->cleanOutput($v);
            }
        }else{
			$cleaned_data = trim(preg_replace(array(
				'/<(.*?)<\/(.*?)>/is',
				'/<(.*?)>/is'
			), '', $data));
        }
        return $cleaned_data;
	}
	
	final protected function validateEmail($email){
		return filter_var($email, FILTER_VALIDATE_EMAIL);
	}
	
	
}