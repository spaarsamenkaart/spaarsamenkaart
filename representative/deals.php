<?php      
require_once '../application-top.php';
require_once '../includes/navigation-functions.php';
 
include 'update-deal-status.php';
if(!isRepresentativeUserLogged()) redirectUser(CONF_WEBROOT_URL.'representative/login.php');
$rep_id = $_SESSION['logged_user']['rep_id'];
$page=(is_numeric($_REQUEST['page'])?$_REQUEST['page']:1);
	$pagesize=15;
$post=getPostedData();
//Search Form
	$rsc=$db->query("SELECT  company_id, company_name  FROM `tbl_companies` WHERE company_active=1 and company_deleted = 0 and company_rep_id=$rep_id order by company_name asc");
	$companyArray=array();
	while($arrs=$db->fetch($rsc)){

		$companyArray[$arrs['company_id']]= $arrs['company_name'];
	}
		
	$rscity=$db->query("SELECT  city_id, city_name  FROM `tbl_cities` WHERE city_active=1 and city_deleted = 0 and city_request=0 order by city_name asc");
	$cityArray=array();
	while($arrs=$db->fetch($rscity)){

		$cityArray[$arrs['city_id']]= $arrs['city_name'];
	}
	$Src_frm=new Form('Src_frm', 'Src_frm');
	$Src_frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
	$Src_frm->setFieldsPerRow(1);
	$Src_frm->captionInSameCell(false);
	$Src_frm->addTextBox(t_lang('M_FRM_KEYWORD'), 'keyword', '', '','');
	$Src_frm->addSelectBox(t_lang('M_FRM_COMPNAY_NAME'), 'deal_company', $companyArray,  $value , '', t_lang('M_TXT_SELECT'),  'deal_company');
	$Src_frm->addSelectBox(t_lang('M_TXT_CITY_NAME'), 'deal_city', $cityArray,  $value , '', t_lang('M_TXT_SELECT'),  'deal_city');
	$Src_frm->addHiddenField('','mode','search');
	$Src_frm->addHiddenField('','status',$_REQUEST['status']);
	$fld1=$Src_frm->addButton('', 'btn_cancel',  t_lang('M_TXT_CLEAR_SEARCH'), '', ' class="inputbuttons" onclick=location.href="deals.php"');
	$fld=$Src_frm->addSubmitButton('', 'btn_search', t_lang('M_TXT_SEARCH'), '', ' class="inputbuttons"')->attachField($fld1);


	

	$mainTableName='tbl_deals';
	$primaryKey='deal_id';
	$colPrefix='deal_';

 


	$frm=getMBSFormByIdentifier('frmDeal');
	 
	#$frm->setAction('?page=' . $page.'&status='. $_REQUEST['status']);

	 
	$fld=$frm->getField('btn_submit');
	$fld->html_after_field='<span style="color: #f00;">'.t_lang('M_TXT_NOTE_SERVER_TIME') . displayDate(date('Y-m-d H:i:s'), true, false) . '</span>';
	$fld=$frm->getField('deal_cities');

	$frm->removeField($fld);
	$fld1=$frm->getField('deal_company');
	if($_GET['edit']>0){
	$deal_id = $_GET['edit'];
	$fld1->extra='onchange="addAddress(this.value,'.$deal_id.');"';
	}else{
	$fld1->extra='onchange="addAddress(this.value,0);"';
	}
	$frm->addHiddenField( '', 'status', $_REQUEST['status'],   'status',  '');
	$fld=$frm->getField('deal_company');
	$fld->selectCaption='Select';
	$fld->requirements()->setRequired();
	$fld6 = $frm->getField('addAddress');
	$fld6->merge_cells=2;
 
	$frm->getField('deal_start_time')->requirements()->setDateTime();
	$frm->getField('deal_end_time')->requirements()->setDateTime();
	$fld1 = $frm->getField('btn_submit');
	$fld1->merge_cells=2;
	$fld = $frm->getField('deal_start_time');
	$fld->value=displayDate(date('Y-m-d H:i:s'), true, false);
	 
	$fld = $frm->getField('deal_end_time');

	$fld->value=displayDate(Date("y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")+10,date("Y"))),false,false,CONF_TIMEZONE);
	$fld=$frm->addButton('', 'btn_submit_cancel', 'Cancel', '',  ' class="inputbuttons" onclick=location.href="deals.php"')->attachField($fld1);
	 

	 
	$sql = $db->query("SELECT company_id FROM tbl_companies WHERE company_rep_id = ".$rep_id);
	while($company_data = $db->fetch($sql)){
		$company[] = $company_data['company_id'];
	}
				
				
	$srch=new SearchBase('tbl_deals', 'd');
	$srch->addCondition('deal_deleted', '=', 0);
	if(count($company)>0){
	$srch->addCondition('deal_company', 'IN',$company);
	}else{
	$srch->addCondition('deal_company', '<',0);
	}
	
	if($_GET['status'] !='incomplete'){
		$srch->joinTable('tbl_cities', 'INNER JOIN', 'd.deal_city=c.city_id', 'c');
		$srch->joinTable('tbl_companies', 'INNER JOIN', 'd.deal_company=company.company_id ', 'company');
		$srch->addMultipleFields(array('d.*', 'c.*', 'company.*'));
	} 
 

	if($post['mode']=='search')
	{

	 
		
		if($post['keyword'] != '') 	
		{
			
			$cnd=$srch->addDirectCondition('0');
			$cnd->attachCondition('d.deal_name'.$_SESSION['lang_fld_prefix'], 'like','%'. $post['keyword'].'%' ,'OR');
			 
		}
		
		if($post['deal_company'] != '') 	
		{
			
			$cnd=$srch->addDirectCondition('0');
			$cnd->attachCondition('d.deal_company', '=', $post['deal_company'] ,'OR');
			
		}
		if($post['deal_city'] != '') 	
		{
			
			$cnd=$srch->addDirectCondition('0');
			$cnd->attachCondition('d.deal_city', '=', $post['deal_city'] ,'OR');
			
		}
		
	 
		
		$Src_frm->fill($post);
		
	}
   $status = $_REQUEST['status'];
	if($status =='upcoming'){
	
		$srch->addCondition('deal_status', '=', 0);
		$srch->addCondition('deal_complete', '=', 1);
	}else if($status =='active'){ 
	
		$srch->addCondition('deal_status', '=', 1);
		$srch->addCondition('deal_complete', '=', 1);
	}else if($status =='expired'){
	
		$srch->addCondition('deal_status', '=', 2);
	
	}else if($status =='un-approval'){
	
		$srch->addCondition('deal_status', '=', 5);
		$srch->addCondition('d.deal_complete','=',1);
	}else if($status =='cancelled'){
	
		$srch->addCondition('deal_status', '=', 3);

	}else if($status =='rejected'){
	
		$srch->addCondition('deal_status', '=', 6);

	}else if($status =='purchased'){
	
		$srch->joinTable('tbl_order_deals', 'INNER JOIN', 'd.deal_id=od.od_deal_id', 'od');
		$srch->joinTable('tbl_orders', 'INNER JOIN', 'od.od_order_id=o.order_id and o.order_payment_status=1', 'o');
		$srch->addGroupBy('d.deal_id');

	}else if($status =='incomplete'){
		$srch->addCondition('deal_complete', '=', 0);
	}else{
	
		$srch->addCondition('deal_status', '=', 1);
	}
	if($_GET['status'] !='incomplete'){
		$srch->addOrder('c.city_name','asc');
	}
	$srch->addOrder('d.deal_start_time','desc');
	$srch->addOrder('d.deal_status');
	$srch->addOrder('d.deal_name');

	$srch->setPageNumber($page);
	$srch->setPageSize($pagesize);
	//echo  $srch->getQuery();
	$rs_listing=$srch->getResultSet();

	$pagestring='';

	$pages=$srch->pages();
	if($pages>1){
		
	
		
		$pagestring .= createHiddenFormFromPost('frmPaging', '?', array('page','status'), array('page'=>'','status'=>$_REQUEST['status']));
		$pagestring .= '<div class="pagination fr"><ul><li><a href="javascript:void(0);">'.t_lang('M_TXT_DISPLAYING_RECORDS').' ' . (($page - 1) * $pagesize + 1) . 
		' '.t_lang('M_TXT_TO').' ' . (($page * $pagesize > $srch->recordCount())?$srch->recordCount():($page * $pagesize)) . ' '.t_lang('M_TXT_OF').' ' . $srch->recordCount().'</a></li>';
		$pagestring .= '<li><a href="javascript:void(0);">'.t_lang('M_TXT_GOTO').': </a></li>
		' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
		, $srch->pages(), $page,'<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
		$pagestring .= '</div>';
	}

	$arr_listing_fields=array(
	/* 'listserial'=>'S.N.', */
	/* 'deal_status'=>'Status',
	'deal_tipped_at'=>'Tipped', */
	/* 'deal_name'=>'Name', */
	'deal_img_name'=>t_lang('M_FRM_DEAL_IMAGE'),
	'deal_name'=>t_lang('M_TXT_DEAL_TITLE'),
	/* 'company_name'=>'Company',
	'city_name'=>'City', */
	'action'=>t_lang('M_TXT_ACTION')
	);

include 'header.php';?>
 <script type="text/javascript" charset="utf-8">
 var cancelMsg = '<?php echo addslashes(t_lang('M_MSG_MESSAGE_WHEN_ADMIN_CANCEL_DEAL'));?>';
 </script>
<link href="<?php echo CONF_WEBROOT_URL;?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<script src="<?php echo CONF_WEBROOT_URL;?>js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
			$(" a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
		});
		</script>

<script type="text/javascript" src="<?php echo CONF_WEBROOT_URL;?>js/jquery.naviDropDown.1.0.js"></script>
<script type="text/javascript">
$(function(){	
	
	$('.navigation_vert').naviDropDown({
		dropDownWidth: '350px',
		orientation: 'vertical'
	});
});

</script>

 <link rel="stylesheet" type="text/css" href="plugins/jquery.jqplot.css" />
   <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="plugins/excanvas.js"></script><![endif]-->
  <!-- BEGIN: load jqplot -->
  <script language="javascript" type="text/javascript" src="plugins/jquery.jqplot.js"></script>
  <script language="javascript" type="text/javascript" src="plugins/jqplot.pieRenderer.js"></script>
  <!-- END: load jqplot -->
 <?php  
       if($_SERVER['REQUEST_METHOD']=='POST' && !isset($_POST['btn_search']) && isset($_POST['btn_submit'])){?>
<script type="text/javascript">
 
addAddress('<?php echo $post['deal_company'];?>','0');
 
</script> 
<?php } ?> 
<?php

$arr_bread=array(
'index.php'=>'<img class="home" alt="Home" src="images/home-icon.png">',
''=>t_lang('M_TXT_DEALS')
);


if($_REQUEST['status']==""){
$class = 'class="selected"';
}else{
$tabStatus = $_REQUEST['status'];
$tabClass ='class="selected"';
if($_REQUEST['status'] == 'active') $class = 'class="selected"'; else $class = '';
}

 
?>


<td class="left_portion" width="230"><div class="left_nav">
                <ul>
				    <li>    <a href="deals.php?status=active" <?php echo $class;?>><?php echo t_lang('M_TXT_ACTIVE');?> <?php echo t_lang('M_TXT_DEALS');?> </a></li>
					
					<li><a href="deals.php?status=expired" <?php if($tabStatus=='expired')echo $tabClass;?>><?php echo t_lang('M_TXT_EXPIRED');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					<li><a href="deals.php?status=upcoming" <?php if($tabStatus=='upcoming')echo $tabClass;?>><?php echo t_lang('M_TXT_UPCOMING');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					<li><a href="deals.php?status=un-approval" <?php if($tabStatus=='un-approval')echo $tabClass;?>><?php echo t_lang('M_TXT_UN_APPROVAL');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					<li><a href="deals.php?status=rejected" <?php if($tabStatus=='rejected')echo $tabClass;?>><?php echo t_lang('M_TXT_REJECTED');?></a></li>
					<li><a href="deals.php?status=cancelled" <?php if($tabStatus=='cancelled')echo $tabClass;?>><?php echo t_lang('M_TXT_CANCELLED');?></a></li>
					<li><a href="deals.php?status=purchased" <?php if($tabStatus=='purchased')echo $tabClass;?>><?php echo t_lang('M_TXT_MINIMUM_ONE_COUPON_SOLD');?></a></li>
					<li><a href="deals.php?status=incomplete" <?php if($tabStatus=='incomplete')echo $tabClass;?>><?php echo t_lang('M_TXT_INCOMPLETE');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					</ul>
                </div></td>
				
				<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
				 <?php  
				if(!isset($_GET['edit']) && $_GET['add']!='new'){
				?>
				<div class="div-inline">
				
                  <div class="page-name"><?php echo $_REQUEST['status'];?> <?php echo t_lang('M_TXT_DEALS');?> 
				 
				  <a style="margin-top:11px;" class="button gray" href="add-deals.php?page=<?php echo $page; ?>&add=new&status=<?php echo $_GET['status'];?>"><?php echo t_lang('M_TXT_ADD');?>  <?php echo t_lang('M_TXT_DEALS');?> </a>
				   
				  </div>
                  
                </div>
                 <?php  
				}
				?>
                <div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				 <?php  
				if(is_numeric($_GET['edit']) || $_GET['add']=='new'){
				?>
					<script type="text/javascript">
					$(document).ready(function(){addAddress(document.frmDeal.deal_company.value,<?php echo $_GET['edit'];?>);});</script>
					 
						<div class="box"><div class="title"> <?php echo t_lang('M_TXT_DEALS');?>  </div><div class="content"><?php echo  $frm->getFormHtml();?></div></div>
					<?php  
				}else{
				?>
				<div class="box"><div class="title"> <?php echo t_lang('M_TXT_DEALS');?>  <?php echo t_lang('M_TXT_SEARCH');?>  </div><div class="content">		<?php echo $Src_frm->getFormHtml();?></div>	 </div>	 
				 
 <div class="box">
 <div class="content"><?php echo $pagestring;?>	
 <div class="clear"></div>
 <?php require_once('inc.deal-list.php');?>
 
 
<?php }?>
</div> 
 
</td>
<?php 
include 'footer.php';
?>
