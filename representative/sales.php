<?php
require_once '../application-top.php';
$arr_common_js[] = 'js/calendar.js';
$arr_common_js[] = 'js/calendar-en.js';
$arr_common_js[] = 'js/calendar-setup.js';
$arr_common_css[] = 'css/cal-css/calendar-win2k-cold-1.css';
if (!isRepresentativeUserLogged())
    redirectUser(CONF_WEBROOT_URL . 'representative/login.php');
if (((int) $_SESSION['logged_user']['rep_id']) <= 0) {
    redirectUser(CONF_WEBROOT_URL . 'representative/login.php');
}
$rep_id = $_SESSION['logged_user']['rep_id'];
$srch =new SearchBase('tbl_representative', 'r');
	$srch->addCondition('rep_id', '=',  $rep_id);
	$srch->addFld(array('r.rep_commission'));
	$rs_listing=$srch->getResultSet();
	$rep=$db->fetch($rs_listing);
$rsc = $db->query("SELECT  company_id, IF(CHAR_LENGTH(company_name" . $_SESSION['lang_fld_prefix'] . "),company_name" . $_SESSION['lang_fld_prefix'] . ",company_name) as company_name FROM `tbl_companies` WHERE company_active=1 and company_deleted = 0 and company_rep_id = $rep_id order by company_name asc");
$companyArray = $db->fetch_all_assoc($rsc);

$settleArray = array('1' => 'Yes', '0' => 'No');
$Src_frm = new Form('Src_frm', 'Src_frm');
$Src_frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
$Src_frm->setFieldsPerRow(3);
$Src_frm->captionInSameCell(true);
$Src_frm->addTextBox(t_lang('M_FRM_DEAL_NAME'), 'deal_name', '', '', '');
$Src_frm->addSelectBox(t_lang('M_FRM_COMPNAY_NAME'), 'deal_company', $companyArray, $value, '', t_lang('M_TXT_SELECT'), 'deal_company');
$Src_frm->addSelectBox(t_lang('M_TXT_SETTLED'), 'deal_paid', $settleArray, $value, '', t_lang('M_TXT_SELECT'));
$Src_frm->addDateField(t_lang('M_FRM_DEAL_TIPPED_AT'), 'deal_tipped_at', '', 'deal_tipped_at', '');
$Src_frm->addDateField(t_lang('M_FRM_DEAL_ENDS_ON'), 'deal_end_time', '', 'deal_end_time', '');
$Src_frm->addHiddenField('', 'mode', 'search');
$fld1 = $Src_frm->addButton('', 'btn_cancel', t_lang('M_TXT_CLEAR_SEARCH'), '', ' class="inputbuttons" onclick=location.href="sales.php"');
$Src_frm->addSubmitButton('', 'btn_search', t_lang('M_TXT_SEARCH'), '', ' class="inputbuttons"')->attachField($fld1);
$page = (is_numeric($_REQUEST['page']) ? $_REQUEST['page'] : 1);
$pagesize = 15;
$srch = new SearchBase('tbl_deals', 'd');
$srch->addCondition('deal_deleted', '=', 0);
$srch->joinTable('tbl_cities', 'INNER JOIN', 'd.deal_city=c.city_id', 'c');
$srch->joinTable('tbl_companies', 'INNER JOIN', 'd.deal_company=company.company_id and company.company_rep_id=' . $_SESSION['logged_user']['rep_id'], 'company');
$srch->addMultipleFields(array('d.*', 'c.*', 'company.*'));

if (((int) $_REQUEST['deal_company']) > 0) {
    $srch->addCondition('deal_company', '=', $_REQUEST['deal_company']);
}
if ($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['btn_search'])) {
    $post = getPostedData();
    if ($post['mode'] == 'search') {
        if ($post['deal_name'] != '') {

            $cnd = $srch->addDirectCondition('0');
            $cnd->attachCondition('d.deal_name' . $_SESSION['lang_fld_prefix'], 'like', '%' . $post['deal_name'] . '%', 'OR');
        }

        if ($post['deal_company'] != '') {
            $cnd = $srch->addDirectCondition('0');
            $cnd->attachCondition('d.deal_company', '=', $post['deal_company'], 'OR');
        }
        if ($post['deal_paid'] != '') {

            $cnd = $srch->addDirectCondition('0');
            $cnd->attachCondition('d.deal_paid', '=', $post['deal_paid'], 'OR');
        }
        if ($post['deal_end_time'] != '') {

            $end_time = date('Y-m-d', strtotime($post['deal_end_time']));
            $cnd = $srch->addDirectCondition('0');
            $cnd->attachCondition("mysql_func_date(d.`deal_end_time`)", '<=', $end_time, 'OR', true);
        }

        $Src_frm->fill($post);
    }
}

$srch->addOrder('d.deal_start_time', 'desc');
$srch->addOrder('d.deal_status');
$srch->addOrder('d.deal_name');
$srch->setPageNumber($page);
$srch->setPageSize($pagesize);
$rs_listing = $srch->getResultSet();
$pagestring = '';
$pages = $srch->pages();
$pagestring .= createHiddenFormFromPost('frmPaging', '?', array('page', 'status','deal_company'), array('page' => $_REQUEST['page'], 'status' => $_REQUEST['status'],'deal_company'=>$_REQUEST['deal_company']));
$pagestring .= '<div class="pagination "><ul>';
$pageStringContent .= '<a href="javascript:void(0);">' . t_lang('M_TXT_DISPLAYING_RECORDS') . ' ' . (($page - 1) * $pagesize + 1) .
        ' ' . t_lang('M_TXT_TO') . ' ' . (($page * $pagesize > $srch->recordCount()) ? $srch->recordCount() : ($page * $pagesize)) . ' ' . t_lang('M_TXT_OF') . ' ' . $srch->recordCount() . '</a>';
$pagestring .= '<li><a href="javascript:void(0);">' . t_lang('M_TXT_GOTO') . ': </a></li>
		' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
                , $srch->pages(), $page, '<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
$pagestring .= '</div>';

$arr_listing_fields = array(
    'listserial' => t_lang('M_TXT_SR_NO'),
    'deal_name' => t_lang('M_TXT_DEAL_NAME'),
    'merchant_name' => t_lang('M_TXT_MERCHANT_NAME'),
    'deal_tipped_at' => t_lang('M_TXT_DEAL_TIPPED_AT'),
    'deal_end_time' => t_lang('M_TXT_DEAL_END_TIME'),
	'total_sales' => t_lang('M_TXT_TOTAL_SALES'),
	
    'status' => t_lang('M_FRM_STATUS'),
    'setteled' => t_lang('M_FRM_SETTLED'),
	'total_commission'=>t_lang('M_TXT_TOTAL_COMMISSION'),
);


include 'header.php';
$arr_bread = array(
    'my-account.php' => '<img alt="Home" src="images/home-icon.png">',
    'rep-report.php' => t_lang('M_TXT_COMMISSION_EARNINGS'),
    '' => t_lang('M_TXT_SALES'),
);
?>
</div>
</td>

<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread); ?>

    <div class="div-inline">
        <div class="page-name"><?php echo t_lang('M_TXT_SALES'); ?> </div>
    </div>
    <?php if ((isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0]))) { ?> 
        <div class="box" id="messages">
            <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES'); ?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide();
                    return false;"><?php echo t_lang('M_TXT_HIDE'); ?></a></div>
            <div class="content">
                <?php if (isset($_SESSION['errs'][0])) { ?>
                    <div class="redtext"><?php echo $msg->display(); ?> </div>
                    <br>
                    <br>
                    <?php
                }
                if (isset($_SESSION['msgs'][0])) {
                    ?>
                    <div class="greentext"> <?php echo $msg->display(); ?> </div>
                <?php } ?>
            </div>
        </div>
    <?php } ?> 

    <div class="box searchform_filter">
        <div class="title"> <?php echo t_lang('M_TXT_SEARCH'); ?> </div>
        <div class="content togglewrap" style="display:none;">	
            <?php echo $Src_frm->getFormHtml(); ?>
        </div>
    </div>
    <table class="tbl_data" width="100%" id="category_listing">
        <thead>
            <tr>
                <?php
                foreach ($arr_listing_fields as $val)
                    echo '<th>' . $val . '</th>';
                ?>
            </tr>
        </thead>
        <?php $totalComm=0;
        for ($listserial = ($page - 1) * $pagesize + 1; $row = $db->fetch($rs_listing); $listserial++) {
			
            echo '<tr>';
            foreach ($arr_listing_fields as $key => $val) {

                echo '<td ' . (($arr_listing_fields['deal_name'] == $val) ? 'width=30%' : ' ') . ((($arr_listing_fields['deal_tipped_at'] == $val) || ($arr_listing_fields['deal_end_time'] == $val) ) ? ' width=15%' : ' ') . (($arr_listing_fields['listserial'] == $val) ? ' width=4%' : '') . '>';
                switch ($key) {
                    case 'listserial':
                        echo $listserial;
                        break;
                    case 'deal_name':
                        echo '<strong>' . $arr_lang_name[0] . '</strong>' . ' ' . $row['deal_name'] . '<br>';
                        echo '<strong>' . $arr_lang_name[1] . '</strong>' . ' ' . $row['deal_name_lang1'];
                        break;
                    case 'merchant_name':
                        echo '<strong>' . $arr_lang_name[0] . '</strong>' . ' ' . $row['company_name'] . '<br>';
                        echo '<strong>' . $arr_lang_name[1] . '</strong>' . ' ' . $row['deal_name_lang1'];
                        break;
                    case 'deal_tipped_at':
                        if ($row['deal_tipped_at'] != '0000-00-00 00:00:00') {
                            echo displayDate($row['deal_tipped_at'], true);
                        } else {
                            echo '---';
                        }
                        break;
                    case 'deal_end_time':
                        echo displayDate($row['deal_end_time'], true);
                        break;
                    case 'status':
                        echo $arr_deal_status[$row['deal_status']];
                        break;
                    case 'total_sales':
                        $srch = new SearchBase('tbl_orders', 'o');
                        $srch->joinTable('tbl_order_deals', 'INNER JOIN', 'o.order_id=od.od_order_id ', 'od');
                        $srch->joinTable('tbl_deals', 'INNER JOIN', 'od.od_deal_id=d.deal_id ', 'd');

                        $srch->addCondition('o.order_payment_status', '!=', 0);
                        $srch->addCondition('od.od_deal_id', '=', $row['deal_id']);
                        $srch->addMultipleFields(array('od.*', 'o.*', 'd.deal_company', "SUM((od.od_qty+od.od_gift_qty)*od.od_deal_price) as totalAmount"));

                        $data = $srch->getResultSet();
                        $amountRow = $db->fetch($data);

                        if ($db->total_records($data) > 0) {
                            echo CONF_CURRENCY . number_format($amountRow['totalAmount'], 2) . CONF_CURRENCY_RIGHT;
                        }
                        break;
						case 'total_commission':
							$srch = new SearchBase('tbl_coupon_mark', 'cm');
							$srch->joinTable('tbl_order_deals', 'INNER JOIN', 'cm.cm_order_id=od.od_order_id ', 'od');
							$srch->joinTable('tbl_orders', 'INNER JOIN', 'cm.cm_order_id=o.order_id AND cm_deal_id=od_deal_id', 'o');
							$srch->addCondition('o.order_payment_status', '!=', 0); 
							$srch->addCondition('cm.cm_status', '=', 1); 
							$srch->addCondition('cm.cm_deal_id', '=', $row['deal_id']); 
							$srch->addMultipleFields(array( "SUM(od.od_deal_price) as PaidAmount")); 
							$data = $srch->getResultSet();
							$amountRow = $db->fetch($data); 
							if($db->total_records($data)>0){ 
							$rep_com=$rep['rep_commission']*$amountRow['PaidAmount']/100;
								echo  CONF_CURRENCY . number_format($rep_com, 2) . CONF_CURRENCY_RIGHT ;	 
							}
							$totalComm+=$rep_com;
						break;
                    case 'setteled':
                        if ($row['deal_paid'] == 1)
                            echo 'Yes';
                        if ($row['deal_paid'] == 0)
                            echo 'No';
                        break;
                    default:
                        echo $row[$key];
                        break;
                }
                echo '</td>';
            }
            echo '</tr>';
        }
		/*  echo '<tr><td style="text-align:center;" colspan="' . (count($arr_listing_fields)-1 ). '">' . t_lang('M_TXT_TOTAL_COMMISSION') . '</td><td>'.CONF_CURRENCY . number_format($totalComm, 2) . CONF_CURRENCY_RIGHT.'</td></tr>'; */
        if ($db->total_records($rs_listing) == 0)
            echo '<tr><td colspan="' . count($arr_listing_fields) . '">' . t_lang('M_TXT_NO_RECORD_FOUND') . '</td></tr>';
        ?>
    </table>
   <?php  if ($pages > 1) { ?>
        <div class="footinfo">
            <aside class="grid_1">
                <?php echo $pagestring; ?>	 
            </aside>  
            <aside class="grid_2"><span class="info"><?php echo $pageStringContent; ?></span></aside>
        </div>
    <?php } ?>     

</td>
<?php
include 'footer.php';
?>
