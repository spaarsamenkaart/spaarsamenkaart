<?php  
require_once '../application-top.php';
//checkAdminPermission(3);
require_once '../includes/navigation-functions.php';
if(!isRepresentativeUserLogged()) redirectUser(CONF_WEBROOT_URL.'representative/login.php');
$rep_id = $_SESSION['logged_user']['rep_id']; 

$rsc=$db->query("SELECT  *  FROM `tbl_companies` WHERE  company_rep_id=$rep_id ");
	$companyArray=array();
	while($arrs=$db->fetch($rsc)){

		$companyArray[$arrs['company_id']]= $arrs['company_id'];
	}
$company_id = $_SESSION['logged_user']['company_id'];
$page=(is_numeric($_GET['page'])?$_GET['page']:1);
$pagesize=30;

$mainTableName='tbl_company_addresses';
$primaryKey='company_address_id';
$colPrefix='company_address_';

//$company_id = $_GET['company_id'];


if(is_numeric($_GET['delete'])){


	$company_id = $_GET['company_id'];
    if(!$db->deleteRecords( $mainTableName,  array('smt'=>'company_address_id = ?', 'vals'=>array($_GET['delete']), 'execute_mysql_functions'=>false))){
        $msg->addError($db->getError());
    }
    else{
        $msg->addMsg(t_lang('M_TXT_RECORD_DELETED'));
        redirectUser('?company_id='.$company_id.'&page=' . $page);
    }
}


$frm=getMBSFormByIdentifier('frmCompanyAddresses');
$fld=$frm->getField('company_id');
$fld->value=$_GET['company_id'];
$frm->setAction('?company_id='.$company_id.'&page=' . $page);
updateFormLang($frm); 
$fld=$frm->getField('submit');
$fld->value=t_lang('M_TXT_SUBMIT');


if($_SERVER['REQUEST_METHOD']=='POST'){
    $post=getPostedData();
    if(!$frm->validate($post)){
        $errors=$frm->getValidationErrors();
        foreach ($errors as $error) $msg->addError($error);
    }
    else{
        $record=new TableRecord($mainTableName);
       /* $record->assignValues($post); */
		$arr_lang_independent_flds = array('company_id','company_address_id','company_address_zip','company_address_google_map','mode','btn_submit');
		assignValuesToTableRecord($record,$arr_lang_independent_flds,$post);
		$record->setFldValue('company_id', $company_id);
		 
        $success=($post[$primaryKey]>0)?$record->update($primaryKey . '=' . $post[$primaryKey]):$record->addNew();
        if($success){
			$msg->addMsg(T_lang('M_TXT_ADD_UPDATE_SUCCESSFULL'));
            redirectUser();
        }
        else{
            $msg->addError(t_lang('M_TXT_COULD_NOT_ADD_UPDATE') . $record->getError());
            /* $frm->fill($post); */
			fillForm($frm,$post);
        }
    }
}



if(is_numeric($_GET['edit'])){
    $record=new TableRecord($mainTableName);
	
    if(!$record->loadFromDb($primaryKey . '=' . $_GET['edit'], true)){
        $msg->addError($record->getError());
    }
    else{
        $arr=$record->getFlds();
        $arr['btn_submit']=t_lang('M_TXT_UPDATE');
        fillForm($frm,$arr);
       /*  $frm->fill($arr); */
        $msg->addMsg(t_lang('M_TXT_CHANGE_THE_VALUES'));
    }
}

$srch=new SearchBase('tbl_company_addresses', 'ca');
$srch->addCondition('company_id', '=', $company_id);
 $srch->addMultipleFields(array('ca.*'));
$srch->addFld("CONCAT(company_address_line1, '<br>', company_address_line2, '<br>', company_address_line3,  '-', company_address_zip, ' ') AS address");
$srch->addFld("CONCAT(company_address_line1_lang1, '<br>', company_address_line2_lang1, '<br>', company_address_line3_lang1,  '-', company_address_zip, ' ') AS address_lang1");
$srch->setPageNumber($page);
$srch->setPageSize($pagesize);
$rs_listing=$srch->getResultSet();

$pagestring='';

$pages=$srch->pages();
if($pages>1){
    $pagestring .= '<div class="pagination fr"><ul><li><a href="javascript:void(0);">Displaying records ' . (($page - 1) * $pagesize + 1) . ' to ' . (($page * $pagesize > $srch->recordCount())?$srch->recordCount():($page * $pagesize)) . ' of ' . $srch->recordCount().'</a></li>';
   $pagestring .= '<li><a href="javascript:void(0);">Goto Page: </a></li>' . getPageString('<li><a href="?company_id='.$_GET['company_id'].'&page=xxpagexx">xxpagexx</a> </li> ', $srch->pages(), $page,'<li class="selected"><a href="javascript:void(0);" class="active">xxpagexx</a></li>');
			$pagestring .= '</div>';
}

$arr_listing_fields=array(
'listserial'=>t_lang('M_TXT_SR_NO'),
'address'.$_SESSION['lang_fld_prefix']=>t_lang('M_TXT_ADDRESS'),
'action'=>t_lang('M_TXT_ACTION')
);

include 'header.php';
 


?>

<td class="right-portion"> 
                
                <div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                     <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="message error"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				
				<?php  
				if(is_numeric($_REQUEST['edit']) || $_REQUEST['add']=='new'){ ?>
						<div class="box"><div class="title"> <?php echo t_lang('M_TXT_COMPANY_ADDRESSES');?> </div><div class="content"><?php echo  $frm->getFormHtml();?></div></div>
					<?php }else{ ?>
								 
								 
				 
				<div class="box"><div class="title"> <?php echo t_lang('M_TXT_COMPANY_ADDRESSES');?> </div><div class="content">		 
					<?php echo $pagestring;?>		
				<div class="gap">&nbsp;</div>	
				<table class="tbl_data" width="100%">
 
<thead>
<tr>
<?php 
foreach ($arr_listing_fields as $val) echo '<th>' . $val . '</th>';
?>
</tr>
</thead>
<?php 
for($listserial=($page-1)*$pagesize+1; $row=$db->fetch($rs_listing); $listserial++){
    if($listserial%2 == 0) $even = 'even'; else $even = ''; 
	echo '<tr class=" ' . $even . ' ">';
    foreach ($arr_listing_fields as $key=>$val){
        echo '<td>';
        switch ($key){
            case 'listserial':
                echo $listserial;
                break;
			case 'address_lang1':
                echo '<strong>'.$arr_lang_name[0].'</strong>'. ' ' .$row['address'].'<br>';
				echo '<strong>'.$arr_lang_name[1].'</strong>'. ' ' .$row['address_lang1'];
                break;
            case 'action':
                
				 echo '<a href="?company_id='  . $company_id . '&edit=' . $row[$primaryKey] . '&page=' . $page . '" title="' . t_lang('M_TXT_EDIT') . '" class="btn gray">' . t_lang('M_TXT_EDIT') . '</a> ';
                echo '<a href="?company_id='  . $company_id . '&delete=' . $row[$primaryKey] . '&page=' . $page . '" title="' . t_lang('M_TXT_DELETE') . '" onclick="return(confirm(\'' . t_lang('M_MSG_REALLY_WANT_TO_DELETE_THIS_RECORD') . '\'));" class="btn delete">' . t_lang('M_TXT_DELETE') . '</a> ';
                break;
            default:
                echo $row[$key];
                break;
        }
        echo '</td>';
    }
    echo '</tr>';
}
if($db->total_records($rs_listing)==0) echo '<tr><td colspan="' . count($arr_listing_fields) . '">' . t_lang('M_TXT_NO_RECORD_FOUND') . '</td></tr>';
?>
</table>
 
				<div style="padding-top:5px;"><a href="?company_id=<?php echo $company_id ?>&page=<?php echo $page; ?>&add=new" class="btn green fr"><?php echo t_lang('M_TXT_ADD_NEW');?></a></div>
				<div class="clear"></div>
			 
				</div></div>
	<?php   } ?>
<?php 
include 'footer.php';
?>
