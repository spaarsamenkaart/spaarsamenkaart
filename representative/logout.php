<?php
require_once '../includes/login-functions.php';
require_once '../includes/conf.php';
session_start();
clearLoggedRepresentativeLoginCookie();
session_destroy();
$url = CONF_WEBROOT_URL.'representative/login.php';
header("Location: $url");
?>