<?php    
require_once '../application-top.php';
require_once '../includes/navigation-functions.php';

 if($_SESSION['cityname']!="")
	$cityname = convertStringToFriendlyUrl($_SESSION['cityname']);
 else	
    $cityname = 1;
	
if(!isRepresentativeUserLogged()) redirectUser(CONF_WEBROOT_URL.'representative/login.php');
$rep_id = $_SESSION['logged_user']['rep_id']; 
	
	$rscity=$db->query("SELECT  city_id, city_name  FROM `tbl_cities` WHERE city_active=1 and city_deleted = 0 and city_request=0 order by city_name asc");
	$cityArray=array();
	while($arrs=$db->fetch($rscity)){

		$cityArray[$arrs['city_id']]= $arrs['city_name'];
	}
 
	$Src_frm=new Form('Src_frm', 'Src_frm');
	$Src_frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
	$Src_frm->setFieldsPerRow(1);
	$Src_frm->captionInSameCell(false);
	$Src_frm->addTextBox(t_lang('M_FRM_KEYWORD'), 'keyword', '', '','');
	$Src_frm->addSelectBox(t_lang('M_TXT_CITY_NAME'), 'deal_city', $cityArray,  $value , '', t_lang('M_TXT_SELECT'),  'deal_city');
	$Src_frm->addHiddenField('','mode','search');
	$Src_frm->addHiddenField('','status',$_REQUEST['status']);
	$fld1=$Src_frm->addButton('', 'btn_cancel',  t_lang('M_TXT_CLEAR_SEARCH'), '', ' class="inputbuttons" onclick=location.href="company-deals.php"');
	$fld=$Src_frm->addSubmitButton('', 'btn_search', t_lang('M_TXT_SEARCH'), '', ' class="inputbuttons"')->attachField($fld1);
	
	
$page=(is_numeric($_REQUEST['page'])?$_REQUEST['page']:1);
$pagesize=15;
$post=getPostedData();
$mainTableName='tbl_deals';
$primaryKey='deal_id';
$colPrefix='deal_';


if(is_numeric($_GET['cancel'])){
    
    if(!$db->update_from_array($mainTableName, array($colPrefix . 'status'=>3), $primaryKey . '=' . $_GET['cancel'])){
        $msg->addError($db->getError());
    }
    else{
        $msg->addMsg(t_lang('M_TXT_SUCCESSFULLY_CANCELLED'));
        redirectUser(CONF_WEBROOT_URL.'merchant/company-deals.php?status=cancelled&page=1');
    }
}

 
/* if(is_numeric($_GET['status'])){
    $record=new TableRecord($mainTableName);
	
    $arr['deal_categories']=$db->fetch_all_assoc($rs_cats);
    if(!$record->loadFromDb($primaryKey . '=' . $_GET['status'] .'&& deal_company = '.$_SESSION['logged_user']['company_id'] . '&& deal_status = 5 ' , true)){
        $msg->addError($record->getError());
    }
    else{
        $arr=$record->getFlds();
        $arr['btn_submit']='Update';
      
		
        $rs_cats=$db->query("select dc_cat_id, dc_cat_id as cat_id from tbl_deal_to_category where  dc_deal_id=" . $_GET['status']);
        $arr['deal_categories']=$db->fetch_all_assoc($rs_cats);
        
        $frm->fill($arr);
		if($_SERVER['REQUEST_METHOD']!='POST'){ 
			$msg->addMsg('Change the values and submit.');
		}
    }
} */





$srch=new SearchBase('tbl_deals', 'd');
$srch->addCondition('deal_deleted', '=', 0);
$srch->addCondition('deal_company', '=', $_SESSION['logged_user']['company_id']);
if($_GET['status'] != 'incomplete'){
	$srch->joinTable('tbl_cities', 'INNER JOIN', 'd.deal_city=c.city_id', 'c');
	
}
$srch->joinTable('tbl_companies', 'INNER JOIN', 'd.deal_company=company.company_id', 'company');
if($_GET['status'] == 'incomplete'){
	$srch->addMultipleFields(array('d.*', 'company.*'));
	$srch->addCondition('deal_status', '=', 5);
}else{
	$srch->addMultipleFields(array('d.*', 'c.*', 'company.*'));
}



if($post['mode']=='search')
	{

	 
		
		if($post['keyword'] != '') 	
		{
			
			$cnd=$srch->addDirectCondition('0');
			$cnd->attachCondition('d.deal_name'.$_SESSION['lang_fld_prefix'], 'like','%'. $post['keyword'].'%' ,'OR');
			 
		}
		
		 
		if($post['deal_city'] != '') 	
		{
			
			$cnd=$srch->addDirectCondition('0');
			$cnd->attachCondition('d.deal_city', '=', $post['deal_city'] ,'OR');
			
		}
		
	 
		
		$Src_frm->fill($post);
		
	} 

$status = $_REQUEST['status'];
	if($status =='upcoming'){
	
		$srch->addCondition('deal_status', '=', 0);
		$srch->addCondition('deal_complete', '=', 1);
	}else if($status =='active'){
	
		$srch->addCondition('deal_status', '=', 1);
		$srch->addCondition('deal_complete', '=', 1);
	}else if($status =='expired'){
	
		$srch->addCondition('deal_status', '=', 2);
	
	}else if($status =='approval'){
	
		$srch->addCondition('deal_status', '=', 5);
		$srch->addCondition('deal_complete', '=', 1);
	}else if($status =='incomplete'){
		$srch->addCondition('deal_complete', '=', 0);
		$srch->addCondition('deal_status', '=', 5);
	}else if($status =='cancelled'){
	
		$srch->addCondition('deal_status', '=', 3);

	}else if($status =='rejected'){
	
		$srch->addCondition('deal_status', '=', 6);

	}else if($status =='new'){
	
		//$srch->addCondition('deal_status', '=', 6);

	}else{
	
		$srch->addCondition('deal_status', '=', 1);
	} 
$srch->addOrder('d.deal_start_time','desc');
$srch->addOrder('d.deal_status');
$srch->addOrder('d.deal_name');

$srch->setPageNumber($page);
$srch->setPageSize($pagesize);
 
$rs_listing=$srch->getResultSet();

$pagestring='';

$pages=$srch->pages();
$pagestring='';

	$pages=$srch->pages();
	if($pages>1){
		
	
		
		$pagestring .= createHiddenFormFromPost('frmPaging', '?', array('page','status'), array('page'=>'','status'=>$_REQUEST['status']));
		$pagestring .= '<div class="pagination fr"><ul><li><a href="javascript:void(0);">'.t_lang('M_TXT_DISPLAYING_RECORDS').' ' . (($page - 1) * $pagesize + 1) . 
		' '.t_lang('M_TXT_TO').' ' . (($page * $pagesize > $srch->recordCount())?$srch->recordCount():($page * $pagesize)) . ' '.t_lang('M_TXT_OF').' ' . $srch->recordCount().'</a></li>';
		$pagestring .= '<li><a href="javascript:void(0);">'.t_lang('M_TXT_GOTO').' </a></li>
		' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
		, $srch->pages(), $page,'<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
		$pagestring .= '</div>';
	}
/* if($status =='active'){
$arr_listing_fields=array(
'listserial'=>'S.N.',
'deal_status'=>'Status',
'deal_tipped_at'=>'Tipped',
'deal_name'=>'Name',
'city_name'=>'City',
'view_counter'=>'Deal View',
'action'=>'Action'
);
}else{
$arr_listing_fields=array(
'listserial'=>'S.N.',
'deal_status'=>'Status',
'deal_tipped_at'=>'Tipped',
'deal_name'=>'Name',
'city_name'=>'City',
'action'=>'Action'
);
} */
$arr_listing_fields=array(
	/* 'listserial'=>'S.N.', */
	/* 'deal_status'=>'Status',
	'deal_tipped_at'=>'Tipped', */
	/* 'deal_name'=>'Name', */
	'deal_img_name'=>t_lang('M_FRM_DEAL_IMAGE'),
	'deal_name'=>t_lang('M_TXT_DEAL') .' ' .t_lang('M_FRM_TITLE'),
	/* 'company_name'=>'Company',
	'city_name'=>'City', */
	'action'=>t_lang('M_TXT_ACTION')
	);
include 'header.php';







if($_REQUEST['status']==""){
$class = 'class="selected"';
}else{
$tabStatus = $_REQUEST['status'];
$class = '';
$tabClass ='class="selected"';
}

 $company_id = $_SESSION['logged_user']['company_id'];
 if(is_numeric($_REQUEST['status'])>0){
 $deal_id = $_REQUEST['status'];
 }else{
 $deal_id = 0;
 }
?>
<link href="<?php echo CONF_WEBROOT_URL;?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<script src="<?php echo CONF_WEBROOT_URL;?>js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
			$(" a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
		});
		</script>
<script type="text/javascript">
var checkAdressMsg = '<?php echo addslashes(t_lang('M_MSG_PLEASE_SELECT_AT_LEAST_ONE_ADDRESS'));?>';
var txtoops = '<?php echo addslashes(t_lang('M_TXT_INTERNAL_ERROR'));?>';
var txtreload = '<?php echo addslashes(t_lang('M_TXT_PLEASE_RELOAD_PAGE_AND_TRY_AGAIN'));?>';
</script>
<td class="left_portion" width="230"><div class="left_nav">
                <ul>
				    <li ><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=active&page=' . $page);?>" <?php if($tabStatus=='active')echo $tabClass; else echo $class;?>><?php echo t_lang('M_TXT_ACTIVE');?> </a></li>
					<li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=expired&page=1');?>" <?php if($tabStatus=='expired')echo $tabClass;?>><?php echo t_lang('M_TXT_EXPIRED');?></a></li>
					<li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=upcoming&page=1');?>" <?php if($tabStatus=='upcoming')echo $tabClass;?>><?php echo t_lang('M_TXT_UPCOMING');?></a></li>
					<li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=approval&page=1');?>" <?php if($tabStatus=='approval')echo $tabClass;?>><?php echo t_lang('M_TXT_UN_APPROVAL');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					<li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=rejected&page=1');?>" <?php if($tabStatus=='rejected')echo $tabClass;?>><?php echo t_lang('M_TXT_REJECTED');?></a></li>
					<li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=cancelled&page=1');?>" <?php if($tabStatus=='cancelled')echo $tabClass;?>><?php echo t_lang('M_TXT_CANCELLED');?></a></li>
					<!-- <li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=purchased');?>" <?php if($tabStatus=='purchased')echo $tabClass;?>><?php echo t_lang('M_TXT_MINIMUM_ONE_COUPON_SOLD');?></a></li> -->
					 
					
					<li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/company-deals.php?status=incomplete&page=1');?>" <?php if($tabStatus=='incomplete')echo $tabClass;?>><?php echo t_lang('M_TXT_INCOMPLETE');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
			   <li><a href="<?php echo (CONF_WEBROOT_URL.'merchant/add-deals.php?add=new&page=1');?>" <?php if($_GET['status']=='new')echo 'class="active"';?>><?php echo t_lang('M_TXT_ADD_NEW');?> <?php echo t_lang('M_TXT_DEAL');?></a></li>
				</ul>
                </div></td>
				
				<td class="right-portion"><?php //echo getAdminBreadCrumb($arr_bread);?>
                
                <div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg1->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				 
 
 
				<div class="box"><div class="title"> <?php echo ucfirst($_REQUEST['status']);?> <?php echo t_lang('M_TXT_DEALS');?>  </div><div class="content">		<?php echo $Src_frm->getFormHtml();?>
				<?php echo $pagestring;?>		
			<div class="gap">&nbsp;</div>	
			 
			
			 <?php require_once('inc.deal-list.php');?> 
</div></div>
			 
		
</td>
 <?php 
  include 'footer.php';
  ?>
