<?php      
require_once '../application-top.php';
require_once '../includes/navigation-functions.php';
if(!isRepresentativeUserLogged()) redirectUser(CONF_WEBROOT_URL.'representative/login.php');
$rep_id = $_SESSION['logged_user']['rep_id']; 

$post=getPostedData();
//Search Form
$Src_frm=new Form('Src_frm', 'Src_frm');
$Src_frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
$Src_frm->setFieldsPerRow(1);
$Src_frm->captionInSameCell(false);
$Src_frm->addTextBox(t_lang('M_TXT_VOUCHER_CODE'), 'order_id', '', '','');
$Src_frm->addTextBox(t_lang('M_FRM_EMAIL_ADDRESS'), 'user_email', '', '','');
 
$Src_frm->addHiddenField('','mode','search');
$fld1=$Src_frm->addButton('', 'btn_search',  t_lang('M_TXT_CLEAR_SEARCH'), '', ' class="inputbuttons" onclick=location.href="tipped-members.php?deal_id='.$_GET['deal_id'].'"');
$fld=$Src_frm->addSubmitButton('', 'btn_cancel', t_lang('M_TXT_SEARCH'), '', ' class="inputbuttons"')->attachField($fld1);

$page = (is_numeric($_GET['page'])?$_GET['page']:1);
$pagesize = 30;


$rsc=$db->query("SELECT  *  FROM `tbl_companies` WHERE  company_rep_id=$rep_id ");
	$companyArray=array();
	while($arrs=$db->fetch($rsc)){

		$companyArray[$arrs['company_id']]= $arrs['company_id'];
	}
	
	

/* get records from db */
$srch = new SearchBase('tbl_coupon_mark', 'cm');
$srch->joinTable('tbl_order_deals', 'INNER JOIN', "cm.cm_order_id=od.od_order_id AND od.od_voucher_suffixes LIKE CONCAT('%', cm.cm_counpon_no, '%')", 'od');
if($_GET['deal_id'] > 0){
	$srch->addCondition('od.od_deal_id', '=', $_GET['deal_id']);
}

if($_GET['status'] == 'active') $srch->addCondition('cm.cm_status', '=',0);
if($_GET['status'] == 'used') $srch->addCondition('cm.cm_status', '=',1);
if($_GET['status'] == 'expired') $srch->addCondition('cm.cm_status', '=',2);
$srch->addCondition('order_payment_status','>',0);
$srch->joinTable('tbl_deals', 'INNER JOIN', 'od.od_deal_id = d.deal_id ', 'd');
$srch->addCondition('d.deal_company','IN',$companyArray);

$srch->joinTable('tbl_orders', 'INNER JOIN', 'od.od_order_id = o.order_id', 'o');
$srch->joinTable('tbl_users', 'INNER JOIN', 'o.order_user_id=u.user_id', 'u');
$srch->addFld('CASE WHEN d.voucher_valid_from <= now()   THEN 1 ELSE 0 END as canUse');
$srch->addFld('CASE WHEN  d.voucher_valid_till >= now() and cm.cm_status=0 THEN 1 ELSE 0 END as active');
$srch->addFld('CASE WHEN  cm.cm_status=1 THEN 1 ELSE 0 END as used');
$srch->addFld('CASE WHEN  (d.voucher_valid_till < now()  and cm.cm_status=0) || cm.cm_status=2  THEN 1 ELSE 0 END as expired');
 
/** search mode **/
if( $post['mode']=='search' ) {

	if( $post['order_id'] != '' ) {
		$id = $post['order_id'];
		$length = strlen($id);
		
		if($length > 13) {
			$order_id = substr($id, 0, 13);
			$LastVouvherNo = ($length-13);
			$voucher_no = substr($id, 13, $LastVouvherNo);
		} else {
			$order_id = $post['order_id'];
		}
		
		$cnd=$srch->addDirectCondition('0');
	    /* $cnd->attachCondition('od.od_order_id', '=', $order_id ,'OR '); */
		$cnd->attachCondition('od.od_order_id', 'like', '%' . $order_id . '%','OR ');
		$cnd->attachCondition('cm.cm_counpon_no', 'like', '%' .$voucher_no . '%','AND'); 
	}
	
	if($post['user_email'] != '') {		
		$cnd=$srch->addDirectCondition('0');
		$cnd->attachCondition('u.user_email', '=', $post['user_email'] ,'OR');		 
	}
	
	 
	
	$Src_frm->fill($post);	
}
/** search mode ends **/

$srch->addMultipleFields(array('od.od_order_id', 'od.od_to_name', 'u.user_name', 'u.user_email', 'o.order_date', 'o.order_payment_mode', 'o.order_payment_status','o.order_payment_capture', 'cm.cm_counpon_no','cm.cm_status','cm.cm_id','d.deal_id','d.deal_instant_deal','d.voucher_valid_from','d.voucher_valid_till'));

if ($_GET['mode'] != 'downloadcsv') {
	$srch->setPageNumber($page);
	$srch->setPageSize($pagesize);
}

$srch->addOrder('o.order_date', 'desc');
$result = $srch->getResultSet();
 /* echo $srch->getQuery(); */
 
$pagestring='';

$pages=$srch->pages();

	if($pages>1){
		 
		$pagestring .= '<div class="pagination fr"><ul><li><a href="javascript:void(0);">'.t_lang('M_TXT_DISPLAYING_RECORDS').' ' . (($page - 1) * $pagesize + 1) . 
		' '.t_lang('M_TXT_TO').' ' . (($page * $pagesize > $srch->recordCount())?$srch->recordCount():($page * $pagesize)) . ' '.t_lang('M_TXT_OF').' ' . $srch->recordCount().'</a></li>';
		$pagestring .= '<li><a href="javascript:void(0);">'.t_lang('M_TXT_GOTO').': </a></li>
		' . getPageString('<li><a href="?deal_id=' . $_REQUEST['deal_id'] . '&page=xxpagexx"  >xxpagexx</a> </li> '
		, $srch->pages(), $page,'<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
		$pagestring .= '</div>';

	}

	$arr_listing_fields=array(
		'listserial'=>t_lang('M_TXT_SR_NO'),
		'user_name'.$_SESSION['lang_fld_prefix']=>t_lang('M_TXT_USER_NAME'),
		'order_id'=>t_lang('M_TXT_VOUCHER_CODE'),
		'user_email'=>t_lang('M_FRM_EMAIL_ADDRESS'),
		'od_qty'=>t_lang('M_TXT_QUANTITY'),
		'order_date'=>t_lang('M_TXT_ORDRED_DATE'),
		'od_to_name'=>t_lang('M_TXT_GIFTED_TO_FRIEND'),
		'order_payment_status'=>t_lang('M_TXT_PAYMENT_STATUS'),
		'cm_status'=>t_lang('M_TXT_VOUCHER_STATUS')
	);

 


 

$arr_listing = array(
	'user_name'.$_SESSION['lang_fld_prefix']=>t_lang('M_TXT_USER_NAME'),
	'order_id'=>t_lang('M_TXT_VOUCHER_CODE'),
	'user_email'=>t_lang('M_FRM_EMAIL_ADDRESS'),
	'od_qty'=>t_lang('M_TXT_QUANTITY'),
	'order_date'=>t_lang('M_TXT_ORDRED_DATE'),
	'od_to_name'=>t_lang('M_TXT_GIFTED_TO_FRIEND'),
	'order_payment_status'=>t_lang('M_TXT_PAYMENT_STATUS'),
	'cm_status'=>t_lang('M_TXT_VOUCHER_STATUS')
);

if ($_GET['mode'] == 'downloadcsv') {
	
	$fname=time() . '_coupons.csv';
 	header("Pragma: public");
	header("Expires: 0");
	header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
	header("Cache-Control: private",false); 
	header("Content-Type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=\"".$fname."\";" );
	header("Content-Transfer-Encoding: binary");  
	$fp=fopen('../temp-xls/' . $fname, 'w+');
	
	if(!$fp) die(t_lang('M_TXT_FILE_NOT_CREATED'));
	fputcsv($fp, $arr_listing);
 
	while ($row=$db->fetch($result)) {
		$arr=array();
		foreach ($arr_listing as $key=>$val){
			switch ($key)
			{
			case 'order_id':
				$arr[]= $row['od_order_id'].$row['cm_counpon_no'];
				break;
			case 'user_name':
				$arr[]=$row[$key];
				break;
			case 'user_email':
				$arr[]=$row[$key];
				break;
			case 'od_qty':
				$arr[] = 1;
				break;
			case 'order_date':
				$arr[]=$row[$key];
				break;
			case 'od_to_name':
				$arr[]=$row[$key];
				break;	
			case 'order_payment_status':
				if($row[$key]== 1){$arr[] =  t_lang('M_TXT_PAID');}
				else if($row[$key]== 0){$arr[] =  t_lang('M_TXT_PENDING');}
				else if($row[$key]== 3){$arr[] =  t_lang('M_TXT_AUTHORIZED');}
				else {$arr[] =  t_lang('M_TXT_REFUND_SENT');}
                break;
			case 'cm_status':
				if($row['used'] == 1 ){
					$arr[] = t_lang('M_TXT_USED');
				}
				
				if($row['expired'] == 1  and $row['active'] == 0){
					$db->query("update tbl_coupon_mark set cm_status = 2 where cm_id=" . $row['cm_id']);
					$arr[] = t_lang('M_TXT_EXPIRED');
				}
				if($row['active'] == 1){
					$arr[] = t_lang('M_TXT_UNUSED');
				}
				break;
			default:
				$arr[]= $row[$key];
				break;
			}
		}
		
		if(count($arr)>0) fputcsv($fp, $arr);
	}  
	
	fclose($fp);
	header("Content-Length: ".filesize('../temp-xls/' . $fname));
	readfile('../temp-xls/' . $fname);
	exit;
}

	if($_GET['used']!=""){
		$cm_id = $_GET['used'];
		
		$srch = new SearchBase('tbl_coupon_mark', 'cm');
		$srch->joinTable('tbl_order_deals', 'INNER JOIN', "cm.cm_order_id=od.od_order_id AND od.od_voucher_suffixes LIKE CONCAT('%', cm.cm_counpon_no, '%')", 'od');
		
		$srch->addCondition('cm_id','=',$cm_id);
		$srch->addCondition('order_payment_status','>',0);
		$srch->joinTable('tbl_deals', 'INNER JOIN', 'od.od_deal_id = d.deal_id AND d.deal_company='.$_SESSION['logged_user']['company_id'], 'd');

		$srch->joinTable('tbl_orders', 'INNER JOIN', 'od.od_order_id = o.order_id', 'o');
		$srch->joinTable('tbl_users', 'INNER JOIN', 'o.order_user_id=u.user_id', 'u');
		$srch->addFld('CASE WHEN d.voucher_valid_from <= now()   THEN 1 ELSE 0 END as canUse');
		$srch->addFld('CASE WHEN  d.voucher_valid_till >= now() and cm.cm_status=0 THEN 1 ELSE 0 END as active');
		$srch->addFld('CASE WHEN  cm.cm_status=1 THEN 1 ELSE 0 END as used');
		$srch->addFld('CASE WHEN  (d.voucher_valid_till < now()  and cm.cm_status=0) || cm.cm_status=2  THEN 1 ELSE 0 END as expired');
		 

		$srch->addMultipleFields(array('od.od_order_id', 'od.od_to_name', 'u.user_name', 'u.user_email', 'o.order_date', 'o.order_payment_mode', 'o.order_payment_status','o.order_payment_capture', 'cm.cm_counpon_no','cm.cm_status','cm.cm_id','d.deal_id','d.deal_instant_deal','d.voucher_valid_from','d.voucher_valid_till'));
 

		$srch->addOrder('o.order_date', 'desc');
		$result = $srch->getResultSet();
		//echo $srch->getQuery();
		$row=$db->fetch($result);
		if($row['active'] == 1){
					if($row['canUse'] == 1){
						voucherUsed($cm_id);
					}else{
						$msg->addError(t_lang('M_MSG_VOUCHER_IS_NOT_ACTIVE_TO_USE'));
					}
		}
		 
		redirectUser('tipped-members.php?deal_id='.$_GET['deal_id'].'&page='.$_GET['page'].'&status='.$_GET['status']);
	}
	
	 
	
	if($_GET['order']!=""){
		$order_id = $_GET['order'];
		$rs2=$db->query("select * from tbl_order_deals where od_order_id='" . $order_id . "'");
				while($row2=$db->fetch($rs2)){
					$totalQuantity +=  ($row2['od_qty']+$row2['od_gift_qty']);
					$priceQty = $row2['od_deal_price'];
					$deal_id = $row2['od_deal_id'];
				}
				
				
		$srch1 = new SearchBase('tbl_orders', 'o');
		$srch1->joinTable('tbl_users', 'INNER JOIN', "o.order_user_id=u.user_id ", 'u');
		$srch1->joinTable('tbl_order_deals', 'INNER JOIN', "o.order_id=od.od_order_id ", 'od');
		$srch1->joinTable('tbl_deals', 'INNER JOIN', "od.od_deal_id=d.deal_id ", 'd');
		$srch1->addCondition('o.order_payment_status','=',3);
		$srch1->addCondition('d.deal_instant_deal','=',1);
		$srch1->addCondition('o.order_id','=',$order_id);
		$srch1->addCondition('o.order_payment_mode','=',4);
		$srch1->addCondition('o.order_payment_capture','=',0);
		$result1 = $srch1->getResultSet();
		$row1=$db->fetch($result1);
		if($db->total_records($result1)>0){

			require_once '../site-classes/order.cls.php';
			require_once '../site-classes/deal-info.cls.php';
			require_once ("../cim-xml/vars.php");
			require_once ("../cim-xml/util.php");
			if(CONF_PAYMENT_PRODUCTION==0) {$payMode = 'testMode';}else{$payMode = 'liveMode';}
			//build xml to post
			$content =
				"<?xml version=\"1.0\" encoding=\"utf-8\"?>" .
				"<createCustomerProfileTransactionRequest xmlns=\"AnetApi/xml/v1/schema/AnetApiSchema.xsd\">" .
				MerchantAuthenticationBlock().
				"<transaction>".
				"<profileTransCaptureOnly>".
				"<amount>" . number_format((($row1['od_deal_price']*$totalQuantity)), 2, '.', '') . "</amount>". // should include tax, shipping, and everything.
				"<tax>
				<amount>0.00</amount>
				<name>WA state sales tax</name>
				<description>Washington state sales tax</description>
				</tax>".
				
				"<lineItems>".
				"<itemId>" . $row1['deal_id'] . "</itemId>".
				"<name>name of item sold</name>".
				"<description>Description of item sold</description>".
				"<quantity>" . ($totalQuantity) . "</quantity>".
				"<unitPrice>" . number_format($priceQty, 2, '.', '') . "</unitPrice>".
				"<taxable>false</taxable>".
				"</lineItems>".
				"<customerProfileId>" . $row1['user_customer_profile_id'] . "</customerProfileId>".
				"<customerPaymentProfileId>" . $row1['order_payment_profile_id'] . "</customerPaymentProfileId>".
				 
				"<order>".
				"<invoiceNumber>".$order_id."</invoiceNumber>".
				"</order>".
				"<taxExempt>false</taxExempt>
				<recurringBilling>false</recurringBilling>
				<cardCode>000</cardCode>
				<approvalCode>".$row1['order_approval_code']."</approvalCode>".
				"</profileTransCaptureOnly>".
				"</transaction>".
				"</createCustomerProfileTransactionRequest>";
		
			 
			 $response = send_xml_request($content);
			 echo '<pre>'.$content.'</pre>';

			$parsedresponse = parse_api_response($response);
			if ("Ok" == $parsedresponse->messages->resultCode) {
			
				 if (isset($parsedresponse->directResponse)) {
			 
					$directResponseFields = explode(",", $parsedresponse->directResponse);
					$responseCode = $directResponseFields[0]; // 1 = Approved 2 = Declined 3 = Error
					$responseReasonCode = $directResponseFields[2]; // See http://www.authorize.net/support/AIM_guide.pdf
					$responseReasonText = $directResponseFields[3];
					$approvalCode = $directResponseFields[4]; // Authorization code
					$transId = $directResponseFields[6];
				
				
				
				 
				}
				$arr=array(
					 'ot_order_id'=>$order_id,
					 'ot_transaction_id'=>$transId,
					 'ot_transaction_status'=>1,
					 'ot_gateway_response'=>var_export($response, true)
				);
				 
				if(!$db->insert_from_array('tbl_order_transactions', $arr)){
					$msg->addMsg(t_lang('M_ERROR_TRANSACTION_NOT_UPDATED') . $transId);
				}
				
				$db->query("UPDATE tbl_orders set order_payment_capture=1 , order_payment_status=1 where order_id='".$order_id."'");
				
			}
		
		}else{
			$msg->addError(t_lang('M_ERROR_INVALID_REQUEST'));
		}
		redirectUser( CONF_WEBROOT_URL .'merchant/tipped-members.php?deal_id='.$_GET['deal_id'].'&page='.$_GET['page']);
		
		
		
	}

include 'header.php';
 

 
 
?> 
<?php $deal_specific = ''; if(intval($_GET['deal_id']) > 0) $deal_specific = '&deal_id='.intval($_GET['deal_id']); ?> 
<td class="left_portion" width="230"><div class="left_nav">
                <ul>
				    <li ><a href="tipped-members.php?mode=downloadcsv&deal_id=<?php echo $_GET['deal_id']; ?>" ><?php echo t_lang('M_TXT_DOWNLOAD_CSV');?> </a></li>
					<li ><a href="tipped-members.php?status=active<?php echo $deal_specific; ?>" <?php if($_GET['status']=='active') echo 'class="selected"';?>><?php echo t_lang('M_TXT_ACTIVE');?> </a></li>
					<li ><a href="tipped-members.php?status=used<?php echo $deal_specific; ?>" <?php if($_GET['status']=='used') echo 'class="selected"';?>><?php echo t_lang('M_TXT_USED');?></a></li>
					<li ><a href="tipped-members.php?status=expired<?php echo $deal_specific; ?>" <?php if($_GET['status']=='expired') echo 'class="selected"';?>><?php echo t_lang('M_TXT_EXPIRED');?> </a></li>
					<li ><a href="tipped-members.php?<?php echo $deal_specific; ?>" <?php if(!isset($_GET['status'])) echo 'class="selected"';?>><?php echo t_lang('M_TXT_ALL_VOUCHERS');?> </a></li>
					</ul>
                </div></td>
<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
                
                <div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				 
				 <div class="box"><div class="title"> <?php echo t_lang('M_TXT_TIPPED_MEMBERS_LISTING');?> </div><div class="content">		<?php echo $Src_frm->getFormHtml();?>
	<?php echo $pagestring;?>		
<div class="gap">&nbsp;</div>	

 
<table class="tbl_data" width="100%">
<thead>
<tr>
 <th colspan="9"><span style="color:#ff0000;"><?php t_lang('M_TXT_UNDER_VOUCHER_STATUS_MARK_USED');?></span>  </th> 
 </tr>
 <tr>
<?php 
foreach ($arr_listing_fields as $val) echo '<th>' . $val . '</th>';
?>
</tr>
</thead>
<?php  
for($listserial=($page-1)*$pagesize+1; $row=$db->fetch($result); $listserial++){
	
    foreach ($arr_listing_fields as $key=>$val){
	
        echo '<td>';
        switch ($key){
            case 'listserial':
                echo $listserial;
                break;
            case 'order_payment_status':
				if($row[$key]== 1){
					echo t_lang('M_TXT_PAID');
					 
				}else if($row[$key]== 0){
					echo t_lang('M_TXT_PENDING');
				}else if($row[$key]== 3){
					echo t_lang('M_TXT_AUTHORIZED');
				}else{
					$db->query("update tbl_coupon_mark set cm_status = 3 where cm_id=" . intval($row['cm_id']));
					echo t_lang('M_TXT_REFUND_SENT');
				}
                break;
           
			case 'order_id':
					echo $row['od_order_id'].$row['cm_counpon_no'];					
	                break;	
			case 'order_date':
					echo displayDate($row['order_date'],true);					
	                break;	
			case 'cm_status':
				
				 
				
				
				  if($row['active'] == 1){
					if($row['canUse'] == 1){
					echo '<a   href="?deal_id='.$_GET['deal_id'].'&used='.$row['cm_id'].'&page='.$_GET['page'].'&status='.$_GET['status'].'" title="'.t_lang('M_TXT_MARK_USED').'" onclick="  return (confirm(\'' . t_lang('M_MSG_COUPON_CHANGE_STATUS_TO_USE') . '\'));" class="btn delete">'.t_lang('M_TXT_MARK_USED');
					if($row['deal_instant_deal'] == 1 && $row['order_payment_mode'] == 4 && $row['order_payment_capture'] == 0  ){
						echo ' * ';
					}
					echo '</a> ';
					
					
					}else{
					echo '<a   href="javascript:void(0);" onclick="alert(\'' . t_lang('M_MSG_VOUCHER_IS_NOT_ACTIVE_TO_USE') . '\')" class="btn delete">'.t_lang('M_TXT_MARK_USED').'</a> ';  
					}
				}
				if($row['used'] == 1 ){
					echo t_lang('M_TXT_USED').'  ';
					if($row['deal_instant_deal'] == 1 && $row['order_payment_mode'] == 4 && $row['order_payment_capture'] == 0  ){
						echo '<a   href="?deal_id='.$_GET['deal_id'].'&page='.$_GET['page'].'&order='.$row['od_order_id'].'"  class="btn delete">'.t_lang('M_TXT_CAPTURE_PAYMENT').'</a> ';
					}
					
					if($row['deal_instant_deal'] == 1 && $row['order_payment_mode'] == 4 && $row['order_payment_capture'] == 1  ){
						echo '*';
					}  
				}
				
				if($row['expired'] == 1  and $row['active'] == 0){
					$db->query("update tbl_coupon_mark set cm_status = 2 where cm_id=" . intval($row['cm_id']));
					echo t_lang('M_TXT_EXPIRED');
				} 
				
				echo '<a   href="voucher-detail.php?id='.$row['od_order_id'].$row['cm_counpon_no'].'" target="_blank" title="'.t_lang('M_TXT_VOUCHER_DETAIL').'" class="btn delete">'.t_lang('M_TXT_VOUCHER_DETAIL');
				break;			
			case 'od_qty':					
	                echo  1;					
	                break;
					
			
           
            default:
                echo $row[$key];
                break;
        }
        echo '</td>';
    }
    echo '</tr>';
}
if($db->total_records($result)==0) echo '<tr><td colspan="' . count($arr_listing_fields) . '">' . t_lang('M_TXT_NO_RECORD_FOUND') . '</td></tr>';
?>
</table>
</div></div>
</td>
<?php 
include 'footer.php';
?>
