<?php      
require_once '../application-top.php';
require_once '../includes/navigation-functions.php';
 
include 'update-deal-status.php';
if(!isRepresentativeUserLogged()) redirectUser(CONF_WEBROOT_URL.'representative/login.php');
$rep_id = $_SESSION['logged_user']['rep_id'];

$rsc=$db->query("SELECT  company_id, company_name  FROM `tbl_companies` WHERE company_active=1 and company_deleted = 0 and company_rep_id=$rep_id order by company_name asc");
	$companyArray=array();
	while($arrs=$db->fetch($rsc)){

		$companyArray[$arrs['company_id']]= $arrs['company_name'];
	}
	
$_REQUEST['step']=(is_numeric($_REQUEST['step'])?$_REQUEST['step']:1);
$post=getPostedData();
 	$mainTableName='tbl_deals';
	$primaryKey='deal_id';
	$colPrefix='deal_';
	if($_REQUEST['step']==2){
		$frm=getMBSFormByIdentifier('frmDealLocation');
		$fld=$frm->getField('deal_company');
		$fld->selectCaption='Select';  
		$fld->options=$companyArray;
	}
	if($_REQUEST['step']==3){
		$frm=getMBSFormByIdentifier('frmDealVoucher');
	}
	if($_REQUEST['step']==4){
		$frm=getMBSFormByIdentifier('frmDealCategory');
	}
	if($_REQUEST['step']==5){
		$frm=getMBSFormByIdentifier('frmDealSeo');
	}
	if($_REQUEST['step']==6){
		$frm=getMBSFormByIdentifier('frmDealCharity');
		$fld=$frm->getField('deal_charity');
		$fld->selectCaption='';  
		$fld->options=array_merge(array('0'=>'Select'),$fld->options);
		
	}
	if($_REQUEST['step']==7){
		$frm=getMBSFormByIdentifier('frmDealSettings');
		$fld=$frm->getField('deal_image');
		$frm->removeField($fld);
	}
	if($_REQUEST['step']==1 || !isset($_REQUEST['step'])){
		$frm=getMBSFormByIdentifier('frmDeal');
		$fld=$frm->getField('deal_redeeming_instructions');
		 
		$fld->requirements()->setCustomErrorMessage(t_lang('M_TXT_REDEEMING_INSTRUCTION_MANDATORY'));  
		$fld=$frm->getField('btn_submit');
		$fld->html_after_field='<span style="color: #f00;">'.t_lang('M_TXT_NOTE_SERVER_TIME') . displayDate(date('Y-m-d H:i:s'), true, false) . '</span>';
		$fld=$frm->getField('deal_cities');

		$frm->removeField($fld);
		$fld1=$frm->getField('deal_company');
		if($_GET['edit']>0){
			$deal_id = $_GET['edit'];
			$fld1->extra='onchange="addAddress(this.value,'.$deal_id.');"';
		}else{
			$fld1->extra='onchange="addAddress(this.value,0);"';
		}
		
		$frm->addHiddenField( '', 'status', $_REQUEST['status'],   'status',  '');
		$fld=$frm->getField('deal_company');
		$fld->selectCaption='Select';
		$fld->requirements()->setRequired();
		$fld6 = $frm->getField('addAddress');
		$fld6->merge_cells=2;
		 
		/* removing fields for the Step 1*/
		/*$fld=$frm->getField('deal_city');
		$frm->removeField($fld);*/
		/* $fld=$frm->getField('deal_company');
		$frm->removeField($fld); 
		$fld=$frm->getField('attachCompany');
		$frm->removeField($fld);
		$fld=$frm->getField('addAddress');
		$frm->removeField($fld);*/
		$fld=$frm->getField('setCoupons');
		$frm->removeField($fld);
		$fld=$frm->getField('voucher_valid_from');
		$frm->removeField($fld);
		$fld=$frm->getField('voucher_valid_till');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_min_coupons');
		$frm->removeField($fld);
		/* $fld=$frm->getField('deal_max_coupons');
		$frm->removeField($fld); */
		$fld=$frm->getField('deal_min_buy');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_max_buy');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_charity');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_charity_discount');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_charity_discount_is_percent');
		$frm->removeField($fld);
		 $fld=$frm->getField('basicInfo');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_categories');
		$frm->removeField($fld);
		$fld=$frm->getField('metaInfo');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_meta_title');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_meta_keywords');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_meta_description');
		$frm->removeField($fld);
		$fld=$frm->getField('defaultSettings');
		$frm->removeField($fld);
		 
		$fld=$frm->getField('deal_side_deal');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_instant_deal');
		$frm->removeField($fld);
		$fld=$frm->getField('deal_recent_deal');
		$frm->removeField($fld); 
		
		
		
		 
		/* removing fields for the Step 1*/ 
	  

		$frm->getField('deal_start_time')->requirements()->setDateTime();
		$frm->getField('deal_end_time')->requirements()->setDateTime();
		$fld1 = $frm->getField('btn_submit');
		$fld1->merge_cells=2;
		$fld = $frm->getField('deal_start_time');
		$fld->value=displayDate(date('Y-m-d H:i:s'), true, false);
		 
		$fld = $frm->getField('deal_end_time');

		$fld->value=displayDate(Date("y-m-d H:i:s",mktime(0,0,0,date("m"),date("d")+10,date("Y"))),false,false,CONF_TIMEZONE);
		$fld=$frm->addButton('', 'btn_submit_cancel', t_lang('M_TXT_CANCEL'), '',  ' class="inputbuttons" onclick=location.href="deals.php"')->attachField($fld1);
		
		$fixed_discount_req= new FormFieldRequirement('deal_discount', t_lang('M_TXT_DISCOUNT'));
	$fixed_discount_req->setFloatPositive(true);
	$fixed_discount_req->setCompareWith( 'deal_original_price', 'lt', t_lang('M_FRM_ORIGINAL_PRICE'));	
	$percent_discount_req= new FormFieldRequirement('deal_discount', t_lang('M_TXT_DISCOUNT'));
	$percent_discount_req->setFloatPositive(true);
	$percent_discount_req->setRange( 0, 100);
	$fld=$frm->getField('deal_discount_is_percent');
	$fld->id="deal_discount_is_percent";
	$fld_req=$fld->requirements();
	$fld_req->addOnChangerequirementUpdate( '1', 'ne', 'deal_discount', $fixed_discount_req);
	$fld_req->addOnChangerequirementUpdate( '1', 'eq', 'deal_discount', $percent_discount_req);
	}
	$frm->addHiddenField('','step',$_REQUEST['step']);
	$frm->addHiddenField('','deal_id',$_REQUEST['edit']);
	updateFormLang($frm); 
	$fld=$frm->getField('deal_company');
	$fld->selectCaption='Select'; 
	$fld->field_caption =t_lang('M_FRM_COMPANY');  
	$fld->options=$companyArray;
	
	$fld=$frm->getField('deal_city');
	$fld->field_caption =t_lang('M_FRM_CITY'); 
		
		
		
	if(is_numeric($_GET['edit'])){
	
		if(!repPermission($rep_id,$_GET['edit'])){
			$msg->addError(t_lang('M_ERROR_INVALID_REQUEST'));
		}else{ 
		 

			$record=new TableRecord($mainTableName);
			
			/* if(!$record->loadFromDb($primaryKey . '=' . $_GET['edit'], true)){ */
			if(!$record->loadFromDb($primaryKey . '=' . $_GET['edit'] , true)){
				$msg->addError($record->getError());
			}
			else{
				$arr=$record->getFlds();
				$arr['btn_submit']=t_lang('M_TXT_UPDATE');
			  
				
				$rs_cats=$db->query("select dc_cat_id, dc_cat_id as cat_id from tbl_deal_to_category where  dc_deal_id=" . $_GET['edit']);
				$arr['deal_categories']=$db->fetch_all_assoc($rs_cats);
				
				/* $frm->fill($arr);
				 
				$msg->addMsg('Change the values and submit.'); */
				 
				fillForm($frm,$arr);
			    
				$msg->addMsg(t_lang('M_TXT_CHANGE_THE_VALUES'));
				 
			}
		} 
	}

	if($_SERVER['REQUEST_METHOD']=='POST' && !isset($_POST['btn_search']) && isset($_POST['btn_submit'])){
		$post=getPostedData();
	 
		if(!$frm->validate($post)){
			$errors=$frm->getValidationErrors();
			foreach ($errors as $error) $msg->addError($error);
		}
		else{
			$record=new TableRecord($mainTableName);
			/* $record->assignValues($post); */
			$arr_lang_independent_flds = array('deal_id','deal_company','deal_city','deal_start_time','deal_end_time','voucher_valid_from','voucher_valid_till','deal_min_coupons','deal_max_coupons','deal_min_buy','deal_max_buy','deal_side_deal','deal_instant_deal','deal_main_deal','deal_recent_deal','deal_original_price','deal_discount','deal_discount_is_percent','deal_charity','deal_charity_discount','deal_charity_discount_is_percent','deal_bonus','deal_commission_percent','deal_featured','deal_addedon','deal_tipped_at','deal_status','deal_deleted','deal_complete','mode','btn_submit');
			assignValuesToTableRecord($record,$arr_lang_independent_flds,$post);
			if($post['deal_id']=="")	{
				$record->setFldValue('deal_status',5);
			}
			
			if(!($post[$primaryKey]>0)) $record->setFldValue('deal_addedon',date('Y-m-d H:i:s'), false);
			$total_dac_address_capacity = count($post['dac_address_capacity']);
			
			$totalCapacity = 0;
			
			for($i=0;$i<$total_dac_address_capacity;$i++){
				if($post['dac_address_capacity'][$i] != "" ){
					$capacityCount++;
					if(($post['dac_address_capacity'][$i]) <= ($post['deal_max_buy']) && $post['dac_address_capacity'][$i]!=0 && ($post['dac_address_capacity'][$i]) >= ($post['deal_min_buy']) ) {
						$checkVar ='true';
						
					} 
					$totalCapacity += $post['dac_address_capacity'][$i];
			
				}
			}
			
			if($post['step']==1  || $post['step']==2  || $post['step']==3  ){	
				if($post['dac_address_id']!="" && $totalCapacity > 0 ){
				
				if( $totalCapacity >= $post['deal_min_coupons'] && $totalCapacity == ($post['deal_max_coupons']) && $checkVar=='' ){
				
					 
						if($post[$primaryKey]>0) $success = $record->update($primaryKey . '=' . $post[$primaryKey]);
					
					 
					 
						if($post[$primaryKey]=='') $success = $record->addNew(); 
					 
				 
					if($success){
						$deal_id=($post[$primaryKey]>0)?$post[$primaryKey]:$record->getId();
						 
						
						############## CODE FOR INSERT COMPANY ID AND ADDRESS ID FOR MULTIPLE LOCATION ###############
						
						$db->query("delete from tbl_deal_address_capacity where dac_deal_id=" . $deal_id);
						 $count = 0;
						
						 foreach($_POST['dac_address_capacity'] as $key=>$val){
							 if($_POST['dac_address_capacity'][$key] != 0)$dac_address_capacity[] = $val;
								
						 }
						 
						if(is_array($_POST['dac_address_id'])){
						
						
							foreach ($_POST['dac_address_id'] as $addressid){
							 
								$db->insert_from_array('tbl_deal_address_capacity', array('dac_deal_id'=>$deal_id, 'dac_address_id'=>$addressid, 'dac_address_capacity'=>$dac_address_capacity[$count]));
								
						$count++;
							}
							
							 
							
						}
						
						############## CODE FOR INSERT COMPANY ID AND ADDRESS ID FOR MULTIPLE LOCATION ###############
						
						 
						 
						if($post['status']=='upcoming'){
							if($post['deal_main_deal']!=""){
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>$post['deal_main_deal']), 'deal_id=' . $deal_id)) dieJsonError($db->getError());
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>0), 'deal_city=' . $post['deal_city'].' and deal_id!='.$deal_id.' and deal_status = 0')) dieJsonError($db->getError());
							}
						}
						 
						 
						 if($post['status']=='active'){
							 if($post['deal_main_deal']!=""){
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>$post['deal_main_deal']), 'deal_id=' . $deal_id)) dieJsonError($db->getError());
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>0), 'deal_city=' . $post['deal_city'].' and deal_id!='.$deal_id.' and deal_status > 0')) dieJsonError($db->getError());
							 }
						 }
						  
						############### DISPLAY AS MAIN DEAL #################
						
						############### DISPLAY AS RECENT DEAL #################
						
						if($post['deal_recent_deal']!=1){
							if(!$db->update_from_array('tbl_deals', array('deal_recent_deal'=>0), 'deal_id=' . $deal_id)) dieJsonError($db->getError());
						}
						
						############### DISPLAY AS RECENT DEAL #################
						
						if($post['step']==3){
							if(!$db->update_from_array('tbl_deals', array('deal_complete'=>1), 'deal_id=' . $deal_id)) dieJsonError($db->getError());
						}
						$msg->addMsg(t_lang('M_TXT_ADD_UPDATE_SUCCESSFULL'));
						if($_REQUEST['step']==1){
							redirectUser('add-deals.php?edit=' . $deal_id . '&page='.$page.'&status='.$get['status'].'&step=3');
						}else{
							redirectUser('add-deals.php?edit=' . $deal_id . '&page='.$page.'&status='.$get['status'].'&step='.($_REQUEST['step']+1));	
						}
						
						
					}
					else{
						$msg->addError(t_lang('M_TXT_COULD_NOT_ADD_UPDATE') . $record->getError());
						/* $frm->fill($post); */
						fillForm($frm,$post);
						 
					}
				}else{
					$msg->addError(unescape_attr(t_lang('M_MSG_DEAL_CAPACITY_EXCEED')));
					/* $frm->fill($post); */
					fillForm($frm,$post);
					#redirectUser('deals.php?edit='.$post['deal_id'].'&page='.$page.'&status='.$get['status']);
				}
			 
				}else{
					//$msg->addError(t_lang('M_MSG_PLEASE_SELECT_AT_LEAST_ONE_ADDRESS'));
					$msg->addError(t_lang('M_MSG_Please_select_at_least_one_address_and_add_a_valid_capacity'));
					/* $frm->fill($post); */
					fillForm($frm,$post);
					
					#redirectUser('deals.php?edit='.$post['deal_id'].'&page='.$page.'&status='.$get['status']);
				}
			}else if($post['step']==4 ){
				 
						if($post[$primaryKey]>0) $success = $record->update($primaryKey . '=' . $post[$primaryKey]);
					 
					 
						if($post[$primaryKey]=='') $success = $record->addNew(); 
					 
				 
					if($success){
						$deal_id=($post[$primaryKey]>0)?$post[$primaryKey]:$record->getId();
						
						if($post['step']==4){	
							
							if(count($post['deal_categories']) > 0){
							$db->query("delete from tbl_deal_to_category where dc_deal_id=" . $deal_id);
								if(is_array($post['deal_categories'])){
									foreach ($post['deal_categories'] as $catid){
										$db->insert_from_array('tbl_deal_to_category', array('dc_deal_id'=>$deal_id, 'dc_cat_id'=>$catid));
									}
								}
								redirectUser('add-deals.php?edit=' . $deal_id . '&page='.$page.'&status='.$get['status'].'&step='.($_REQUEST['step']+1));
							}else{
								$msg->addError(t_lang('M_TXT_PLAESE_CHOOSE_ATLEAST_ONE_CATEGORY'));
							}	
						}
					}
					else{
						$msg->addError(t_lang('M_TXT_COULD_NOT_ADD_UPDATE') . $record->getError());
						/* $frm->fill($post); */
						fillForm($frm,$post);
						 
					}
			}else{
				
					 
						if($post[$primaryKey]>0) $success = $record->update($primaryKey . '=' . $post[$primaryKey]);
					
					 
					 
						if($post[$primaryKey]=='') $success = $record->addNew(); 
					  
				 
					if($success){
						$deal_id=($post[$primaryKey]>0)?$post[$primaryKey]:$record->getId();
						
						if($post['step']==4){	
							$db->query("delete from tbl_deal_to_category where dc_deal_id=" . $deal_id);
							if(count($post['deal_categories']) > 0){
								if(is_array($post['deal_categories'])){
									foreach ($post['deal_categories'] as $catid){
										$db->insert_from_array('tbl_deal_to_category', array('dc_deal_id'=>$deal_id, 'dc_cat_id'=>$catid));
									}
								}
							}else{
								$msg->addError(t_lang('M_TXT_PLAESE_CHOOSE_ATLEAST_ONE_CATEGORY'));
							}	
						}
						 
						
						if(is_uploaded_file($_FILES['deal_image']['tmp_name'])){
							$ext=strtolower(strrchr($_FILES['deal_image']['name'], '.'));
							if(!in_array($ext, array('.gif', '.jpg', '.jpeg', '.png'))){
								$msg->addError(t_lang('M_TXT_DEAL').' '.t_lang('M_ERROR_IMAGE_COULD_NOT_SAVED_NOT_SUPPORTED'));
							}
							else{
								$flname=time() . '_' . $_FILES['deal_image']['name'];
								if(!move_uploaded_file($_FILES['deal_image']['tmp_name'], '../deal-images/' . $flname)){
									$msg->addError(t_lang('M_TXT_FILE_COULD_NOT_SAVE'));
								}
								else{
									$getImg=$db->query("select * from tbl_deals where deal_id='".$deal_id."'");
									$imgRow=$db->fetch($getImg);
									unlink('../deal-images/'.$imgRow['deal_img_name'.$_SESSION['lang_fld_prefix']]);
									$db->update_from_array('tbl_deals', array('deal_img_name'=>$flname), 'deal_id=' . $deal_id);
								}
							}
						}
						
						############### DISPLAY AS MAIN DEAL #################
						 
						if($post['status']=='upcoming'){
							if($post['deal_main_deal']!=""){
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>$post['deal_main_deal']), 'deal_id=' . $deal_id)) dieJsonError($db->getError());
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>0), 'deal_city=' . $post['deal_city'].' and deal_id!='.$deal_id.' and deal_status = 0')) dieJsonError($db->getError());
							}
						}
						 
						 
						 if($post['status']=='active'){
							 if($post['deal_main_deal']!=""){
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>$post['deal_main_deal']), 'deal_id=' . $deal_id)) dieJsonError($db->getError());
								if(!$db->update_from_array('tbl_deals', array('deal_main_deal'=>0), 'deal_city=' . $post['deal_city'].' and deal_id!='.$deal_id.' and deal_status > 0')) dieJsonError($db->getError());
							 }
						 }
						  
						############### DISPLAY AS MAIN DEAL #################
						
						############### DISPLAY AS RECENT DEAL #################
						
						if($post['deal_recent_deal']!=1){
							if(!$db->update_from_array('tbl_deals', array('deal_recent_deal'=>0), 'deal_id=' . $deal_id)) dieJsonError($db->getError());
						}
						
						############### DISPLAY AS RECENT DEAL #################
						
						############### DISPLAY AS MAIN DEAL #################
						if($post['deal_id']=="")	{
							########## Email #####################
							$headers  = 'MIME-Version: 1.0' . "\r\n";
							$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

							$headers .= 'From: ' . CONF_SITE_NAME . ' <' . CONF_EMAILS_FROM . '>' . "\r\n";
							 $rs=$db->query("select * from tbl_email_templates where tpl_id=33");
							 $row_tpl=$db->fetch($rs);
						
							$message=$row_tpl['tpl_message'.$_SESSION['lang_fld_prefix']];
							$subject=$row_tpl['tpl_subject'.$_SESSION['lang_fld_prefix']];
							$arr_replacements=array(
							'xxcompany_namexx' => $_SESSION['logged_user']['rep_fname'].' '.$_SESSION['logged_user']['rep_lname'],
							'xxdeal_namexx' => $post['deal_name'],
							 
							'xxsite_namexx' => CONF_SITE_NAME,
							'xxserver_namexx'=>$_SERVER['SERVER_NAME'],
							'xxwebrooturlxx'=>CONF_WEBROOT_URL,
							'xxsite_urlxx'=>'http://'.$_SERVER['SERVER_NAME'].CONF_WEBROOT_URL
							);
							foreach ($arr_replacements as $key=>$val){
								$subject=str_replace($key, $val, $subject);
								$message=str_replace($key, $val, $message);
							}
							if($row_tpl['tpl_status'] == 1){
								mail(CONF_SITE_OWNER_EMAIL, $subject , emailTemplate($message), $headers);
							}	
	 
							##############################################
						}
						$msg->addMsg(T_lang('M_TXT_ADD_UPDATE_SUCCESSFULL'));
						if($_REQUEST['step'] == 7){
							redirectUser('deals.php?page='.$page.'&status='.$get['status']);
						}else{
							redirectUser('add-deals.php?edit=' . $deal_id . '&page='.$page.'&status='.$get['status'].'&step='.($_REQUEST['step']+1));
						}
					}
					else{
						$msg->addError(t_lang('M_TXT_COULD_NOT_ADD_UPDATE') . $record->getError());
						/* $frm->fill($post); */
						fillForm($frm,$post);
						 
					}
			
			}			
			
			 
			
		}
	}

	 
 
 
include 'header.php';?> 


<link href="<?php echo CONF_WEBROOT_URL;?>css/prettyPhoto.css" rel="stylesheet" type="text/css" />
<script src="<?php echo CONF_WEBROOT_URL;?>js/jquery.prettyPhoto.js" type="text/javascript" charset="utf-8"></script>
<script type="text/javascript" charset="utf-8">
		$(document).ready(function(){
			$(" a[rel^='prettyPhoto']").prettyPhoto({theme:'facebook'});
		});
		</script>
<script type="text/javascript" src="<?php echo CONF_WEBROOT_URL;?>js/jquery.hoverIntent.minified.js"></script>
<script type="text/javascript" src="<?php echo CONF_WEBROOT_URL;?>js/jquery.naviDropDown.1.0.js"></script>
<script type="text/javascript">
var checkAdressMsg = '<?php echo addslashes(t_lang('M_MSG_PLEASE_SELECT_AT_LEAST_ONE_ADDRESS'));?>';
var txtoops = '<?php echo addslashes(t_lang('M_TXT_INTERNAL_ERROR'));?>';
var txtreload = '<?php echo addslashes(t_lang('M_TXT_PLEASE_RELOAD_PAGE_AND_TRY_AGAIN'));?>';
$(function(){	
	
	$('.navigation_vert').naviDropDown({
		dropDownWidth: '350px',
		orientation: 'vertical'
	});
	
	$('#deal_discount_is_percent').trigger('change');
});

</script>


 <link rel="stylesheet" type="text/css" href="plugins/jquery.jqplot.css" />
   <!--[if lt IE 9]><script language="javascript" type="text/javascript" src="plugins/excanvas.js"></script><![endif]-->
  <!-- BEGIN: load jqplot -->
  <script language="javascript" type="text/javascript" src="plugins/jquery.jqplot.js"></script>
  <script language="javascript" type="text/javascript" src="plugins/jqplot.pieRenderer.js"></script>
  <!-- END: load jqplot -->
 <?php  
       if($_SERVER['REQUEST_METHOD']=='POST' && !isset($_POST['btn_search']) && isset($_POST['btn_submit'])){?>
<script type="text/javascript">
 
addAddress('<?php echo $post['deal_company'];?>','0');
 
</script> 
<?php } ?> 
<?php

$arr_bread=array(
'index.php'=>'<img class="home" alt="Home" src="images/home-icon.png">',
''=>t_lang('M_TXT_DEALS')
);


if($_REQUEST['status']==""){
$class = 'class="selected"';
}else{
$tabStatus = $_REQUEST['status'];
$tabClass ='class="selected"';
if($_REQUEST['status'] == 'active') $class = 'class="selected"'; else $class = '';
}

 
?>


<td class="left_portion" width="230"><div class="left_nav">
                <ul>
				    <li>    <a href="deals.php?status=active" <?php echo $class;?>><?php echo t_lang('M_TXT_ACTIVE');?> <?php echo t_lang('M_TXT_DEALS');?> </a></li>
					
					<li><a href="deals.php?status=expired" <?php if($tabStatus=='expired')echo $tabClass;?>><?php echo t_lang('M_TXT_EXPIRED');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					<li><a href="deals.php?status=upcoming" <?php if($tabStatus=='upcoming')echo $tabClass;?>><?php echo t_lang('M_TXT_UPCOMING');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					<li><a href="deals.php?status=un-approval" <?php if($tabStatus=='un-approval')echo $tabClass;?>><?php echo t_lang('M_TXT_UN_APPROVAL');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					<li><a href="deals.php?status=rejected" <?php if($tabStatus=='rejected')echo $tabClass;?>><?php echo t_lang('M_TXT_REJECTED');?></a></li>
					<li><a href="deals.php?status=cancelled" <?php if($tabStatus=='cancelled')echo $tabClass;?>><?php echo t_lang('M_TXT_CANCELLED');?></a></li>
					<li><a href="deals.php?status=purchased" <?php if($tabStatus=='purchased')echo $tabClass;?>><?php echo t_lang('M_TXT_MINIMUM_ONE_COUPON_SOLD');?></a></li>
					<li><a href="deals.php?status=incomplete" <?php if($tabStatus=='incomplete')echo $tabClass;?>><?php echo t_lang('M_TXT_INCOMPLETE');?>  <?php echo t_lang('M_TXT_DEALS');?></a></li>
					</ul>
                </div></td>
				
				<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
				 <?php  
				if(!isset($_GET['edit']) && $_GET['add']!='new'){
				?>
				<div class="div-inline">
				
                  <div class="page-name"><?php echo $_REQUEST['status'];?> <?php echo t_lang('M_TXT_DEALS');?> 
				  <?php  ?>
				  <a style="margin-top:11px;" class="button gray" href="?page=<?php echo $page; ?>&add=new&status=<?php echo $_GET['status'];?>"><?php echo t_lang('M_TXT_ADD');?>  <?php echo t_lang('M_TXT_DEALS');?></a>
				  <?php  ?>
				  </div>
                  <?php echo $pagestring;?>	
                </div>
                 <?php  
				}
				?>
                <div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				 <?php  
				if(is_numeric($_GET['edit']) || $_GET['add']=='new'){
				if(!isset($_GET['edit'])) $edit = 0; else  $edit = $_GET['edit'];
				?>
					<?php if( $_REQUEST['step']==1){?>
					<script type="text/javascript">
					$(document).ready(function(){addAddress(document.frmDeal.deal_company.value,<?php echo $edit;?>);});</script>
					<?php } ?>
					<?php if($_REQUEST['step']==2 ){?>
					<script type="text/javascript">
					$(document).ready(function(){addAddress(document.frmDealLocation.deal_company.value,<?php echo $edit;?>);});</script>
					<?php } ?>
					<?php if($_REQUEST['step']==3){?>
					<script type="text/javascript">
					$(document).ready(function(){addAddress(document.frmDealVoucher.deal_company.value,<?php echo $edit;?>);});</script>
					<?php } ?>
					<?php 

					 ?>
						<div class="box">
							<div class="content">
								<div class="tabsholder">
								  <ul class="tabs">
								  <?php 
								  
								if($_REQUEST['step']==1) {
									if($_GET['edit']>0){
										$checkImg1 = '<img alt="" src="images/checkmark.png">'; 
									}else{
										$checkImg1 = '<img alt="" src="images/checkmark_red.png">'; 
									}
								}else{
									if($_GET['edit']>0){
										$checkImg1 = '<img alt="" src="images/checkmark.png">'; 
									}else{
										$checkImg1 = '<img alt="" src="images/checkmark_red.png">'; 
									}
								}
								
								if($_REQUEST['step']==2) {
									if($arr['deal_city'] > 0 && $arr['deal_company'] > 0){
										$checkImg2 = '<img alt="" src="images/checkmark.png">';
									}else{
										$checkImg2 = '<img alt="" src="images/checkmark_red.png">';
									}
								}else{
									if($arr['deal_city'] > 0 && $arr['deal_company'] > 0){
										$checkImg2 = '<img alt="" src="images/checkmark.png">';
									}else{
										$checkImg2 = '<img alt="" src="images/checkmark_red.png">';
									}
								}
								
								if($_REQUEST['step']==3) {
									if($arr['deal_min_coupons'] > 0 && $arr['deal_max_buy'] > 0){
										$checkImg3 = '<img alt="" src="images/checkmark.png">';
									}else{
										$checkImg3 = '<img alt="" src="images/checkmark_red.png">';
									}
								}else{
									if($arr['deal_min_coupons'] > 0 && $arr['deal_max_buy'] > 0){
										$checkImg3 = '<img alt="" src="images/checkmark.png">';
									}else{
										$checkImg3 = '<img alt="" src="images/checkmark_red.png">';
									}
								
								}	
								/* if($_REQUEST['step']==3) $checkImg3 = '<img alt="" src="images/checkmark.png">'; else $checkImg3 = '<img alt="" src="images/checkmark_d.png">'; */
								
								if($_REQUEST['step']==4){
									if(count($arr['deal_categories'])>0){
										$checkImg4 = '<img alt="" src="images/checkmark.png">'; 
									}else{
										$checkImg4 = '<img alt="" src="images/checkmark_red.png">'; 
									}
								}else {
									if(count($arr['deal_categories'])>0){
										$checkImg4 = '<img alt="" src="images/checkmark.png">'; 
									}else{
										$checkImg4 = '<img alt="" src="images/checkmark_red.png">'; 
									}
								}
								
								
								if($_REQUEST['step']==5){
									$checkImg5 = '<img alt="" src="images/checkmark.png">';
								}else{
									if($arr['deal_meta_title']==""){
										$checkImg5 = '<img alt="" src="images/checkmark_d.png">';
									}else{
										$checkImg5 = '<img alt="" src="images/checkmark.png">';
									}
								}	
								if($_REQUEST['step']==6){
									$checkImg6 = '<img alt="" src="images/checkmark.png">'; 
								}else{
									if($arr['deal_charity']>0 || $arr['deal_charity_discount']>0){
										$checkImg6 = '<img alt="" src="images/checkmark.png">';
									}else{
										$checkImg6 = '<img alt="" src="images/checkmark_d.png">';
									}	
								}	
								if($_REQUEST['step']==7) $checkImg7 = '<img alt="" src="images/checkmark.png">'; else $checkImg7 = '<img alt="" src="images/checkmark_d.png">';
								if($_REQUEST['step']==2){
									
								}
								
								if($_GET['edit']>0){
									if($_REQUEST['step']==1) $link1 = 'javascript:void(0);'; else $link1 = '?edit='.$_GET['edit'].'&step=1';
									if($_REQUEST['step']==2) $link2 = 'javascript:void(0);'; else $link2 = '?edit='.$_GET['edit'].'&step=2';
									if($_REQUEST['step']==3) $link3 = 'javascript:void(0);'; else $link3 = '?edit='.$_GET['edit'].'&step=3';
									if($_REQUEST['step']==4) $link4 = 'javascript:void(0);'; else $link4 = '?edit='.$_GET['edit'].'&step=4';
									if($_REQUEST['step']==5) $link5 = 'javascript:void(0);'; else $link5 = '?edit='.$_GET['edit'].'&step=5';
									if($_REQUEST['step']==6) $link6 = 'javascript:void(0);'; else $link6 = '?edit='.$_GET['edit'].'&step=6';
									if($_REQUEST['step']==7) $link7 = 'javascript:void(0);'; else $link7 = '?edit='.$_GET['edit'].'&step=7';
								}else{
									$link1 = 'javascript:void(0);'; 
									$link2 = 'javascript:void(0);'; 
									$link3 = 'javascript:void(0);'; 
									$link4 = 'javascript:void(0);';  
									$link5 = 'javascript:void(0);';  
									$link6 = 'javascript:void(0);';  
									$link7 = 'javascript:void(0);';  
								}
								?>
								  
									<li ><a href="<?php echo $link1;?>" <?php if($_REQUEST['step']==1 || !isset($_REQUEST['step'])) echo 'class="current"';?>  ><?php echo t_lang('M_TXT_FRIST_STEP');?> <span><?php echo $checkImg1;?></span> </a></li>
									<?php /* <li><a href="<?php echo $link2;?>"  <?php if($_REQUEST['step']==2) echo 'class="current"';?>> <?php echo t_lang('M_TXT_LOCATION');?> <span><?php echo $checkImg2;?></span></a></li> */?>
									<li><a href="<?php echo $link3;?>" <?php if($_REQUEST['step']==3) echo 'class="current"';?>>  <?php echo t_lang('M_TXT_VOUCHER_SETTINGS');?> <span><?php echo $checkImg3;?></span></a></li>
									<li><a href="<?php echo $link4;?>" <?php if($_REQUEST['step']==4) echo 'class="current"';?>>  <?php echo t_lang('M_FRM_CATEGORIES');?> <span><?php echo $checkImg4;?></span></a></li>
									<li><a href="<?php echo $link5;?>" <?php if($_REQUEST['step']==5) echo 'class="current"';?>>  <?php echo t_lang('M_TXT_SEO');?> <span><?php echo $checkImg5;?></span></a></li>
									<li><a href="<?php echo $link6;?>" <?php if($_REQUEST['step']==6) echo 'class="current"';?>>  <?php echo t_lang('M_TXT_CHARITY_AND_COMMISION');?> <span><?php echo $checkImg6;?></span></a></li>
									<li><a href="<?php echo $link7;?>" <?php if($_REQUEST['step']==7) echo 'class="current"';?>>  <?php echo t_lang('M_TXT_DISPLAY_SETTINGS');?> <span><?php echo $checkImg7;?></span></a></li>
								  </ul>
								<div class="contents">
								<?php if($_REQUEST['step']==1){?>
								<div class="tabscontent" id="content1"><?php echo  $frm->getFormHtml();?></div>
								<?php } ?>
								<?php if($_REQUEST['step']==2){?>
								<div class="tabscontent" id="content1" ><?php echo  $frm->getFormHtml();?></div>
								<?php } ?>
								<?php if($_REQUEST['step']==3){?>
								<div class="tabscontent" id="content1" ><?php echo  $frm->getFormHtml();?></div>
								<?php } ?>
								<?php if($_REQUEST['step']==4){?>
								<div class="tabscontent" id="content1" ><?php echo  $frm->getFormHtml();?></div>
								<?php } ?>
								<?php if($_REQUEST['step']==5){?>
								<div class="tabscontent" id="content1" ><?php echo  $frm->getFormHtml();?></div>
								<?php } ?>
								<?php if($_REQUEST['step']==6){?>
								<div class="tabscontent" id="content1" ><?php echo  $frm->getFormHtml();?></div>
								<?php } ?>
								<?php if($_REQUEST['step']==7){?>
								<div class="tabscontent" id="content1" ><?php echo  $frm->getFormHtml();?></div>
								<?php } ?>
								</div>
								</div>
							</div>
						</div>
					<?php  
				} ?>
</div> 
 
</td>
<?php 
include 'footer.php';
?>
