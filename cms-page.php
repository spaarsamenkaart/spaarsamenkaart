<?php 
require_once 'application-top.php';

require_once 'header.php';
?>

<!--bodyContainer start here-->
<section class="pagebar center">
    <div class="fixed_container">
        <div class="row">
            <aside class="col-md-12">
                <h3><?php echo $page_name;?></h3>
                <ul class="grids__half list__inline positioned__right">  
                    <li><a href="javascript:void(0)" class="themebtn link__filter"><?php echo t_lang('M_TXT_QUICK_LINKS');?></a></li>
                </ul>
            </aside>
           
        </div>
     </div>
</section>
    
    
    
    <section class="page__container">
        <div class="fixed_container">
            <div class="row">
                <div class="col-md-3">
                        <div class="filter__overlay"></div>
                        <div class="fixed__panel section__filter">

                            <div id="fixed__panel">
                                <div class="block__bordered">
                                    <h5><?php echo t_lang('M_TXT_QUICK_LINKS');?></h5>
                                    <ul class="links__vertical uppercase">
                                        <?php echo printNav(0,8); ?>
                                    </ul>
                                </div>
                                
                            </div>    
                        </div>    
                    </div>
                
                <div class="col-md-9">
                   
                    <div class="container__cms">
                         <?php  
                            $rs1=$db->query("select * from tbl_extra_values");
                            while($row1=$db->fetch($rs1)){
                                 define(strtoupper($row1['extra_conf_name']), parseSpecialStrings($row1['extra_conf_val'.$_SESSION['lang_fld_prefix']]));
                                
                            }
             		$cms_page=$db->query("Select  nl_id,nl_cms_page_id from tbl_nav_links where nl_deleted=0 ");
                            $row=$db->fetch_all_assoc($cms_page);
                                
                            if(in_array($_GET['id'],$row)){
                                    
                                    
                                    
                            if($_GET['id']!="" || isset($_GET['id'])){
                                
                                $srch=new SearchBase('tbl_cms_contents', 'cmsc');
                                $srch->joinTable('tbl_cms_pages', ' JOIN', "page.page_id = cmsc.cmsc_page_id ", 'page');
                                $srch->addCondition( 'page.page_deleted', '=',0 );
                                $srch->addCondition('cmsc.cmsc_page_id', '=',$_GET['id'] );
                                $srch->addCondition( 'cmsc.cmsc_content_delete', '=',0 );
                                $srch->addOrder( 'cmsc.cmsc_display_order', 'asc');  
                                $rs=$srch->getResultSet();
                                        
                                while ($row=$db->fetch($rs)){
                                    $cms_galley_id = $row['cmsc_id'];
                                
                                    $cms_content = $row['cmsc_content'.$_SESSION['lang_fld_prefix']];
                                    $cmsc_type = $row['cmsc_type'];
                                    
                                    $srch1=new SearchBase('tbl_cms_gallery_items', 'cmsgi');
                                    
                                    $srch1->addCondition('cmsgi.cmsgi_gallery_id', '=',$cms_galley_id );
                                    $srch1->addOrder( 'cmsgi.cmsgi_display_order', 'asc');  
                                    $rs1=$srch1->getResultSet();
                                                    
                                        if($cmsc_type==0){?>
                                        
                                            <?php 
                                                if($cms_content!="") {
                                                    echo  $cms_content;
                                                }
                                        }
                                        
                                       /*  $hide_Border=$db->query("Select * from tbl_cms_gallery_items where cmsgi_gallery_id = $cms_galley_id order by cmsgi_display_order asc");
                                        $div_result=$db->fetch($hide_Border);
                                        $Count_rows = mysqli_num_rows ($rs1);
                                        if($div_result['cmsgi_gallery_id']!=""){
                                            
                                            if($cmsc_type==1){
                                            if($Count_rows > 0)  ?>
                                            <div class="clear"></div>
                                            <div id="container"><h4> <?php echo t_lang('M_TXT_IMAGE_GALLERY');?></h4>
                                            <?php if($cms_content!=""){
                                                        echo $cms_content;
                                                    }?>
                                            <div id="gallery" class="ad-gallery">
                                            <div class="ad-image-wrapper"></div>
                                            <div class="ad-controls"></div>
                                            <div class="ad-nav">

                                            <div class="ad-thumbs">
                                            <ul class="ad-thumb-list">
                              <?php 
                                                
                                                
                                                while ($row=$db->fetch($rs1)){?>
                                                
                                                    <li><a  href="<?php echo CONF_WEBROOT_URL; ?>upload-image-gallery/<?=$row['cmsgi_file_path']?>"><img class="fixwidth" src="<?php echo CONF_WEBROOT_URL; ?>upload-image-gallery/thumb/<?php echo $row['cmsgi_thumb_path'];?>" alt="<?php echo $row['cmsgi_desc'.$_SESSION['lang_fld_prefix']];?>" title="<?php echo $row['cmsgi_title'.$_SESSION['lang_fld_prefix']];?>" <?php echo (($row['cmsgi_url']!='')?'data-ad-link="'.$row['cmsgi_url'].'"':''); ?>></a></li>
                                                    <?php 
                                                } 
                                                    echo '</ul>';?>
                                                                                    </div></div></div></div>
                                                                                    <?php 
                                            } 
                                        
                                        }
                                        
                                        if($div_result['cmsgi_gallery_id']!=""){
                                            if($cmsc_type==2){
                                                if($Count_rows > 0) echo '<ul class="teaser_content"><h3>' . t_lang('M_TXT_VIDEO_GALLERY') . '</h3>';
                                                if($cms_content!="") {
                                                        echo $cms_content;
                                                    }
                                                while ($row=$db->fetch($rs1)){ 
                                                $CatArray = array('vid'=>$row['cmsgi_file_path']);
                                                $url = getPageUrl('video.php',$CatArray);
                                                ?>
                                                
                                                    <li><a rel="facebox" href="<?php echo $url;?>"><img class="fixwidth" src="<?php echo CONF_WEBROOT_URL; ?>upload-image-gallery/thumb/<?=$row['cmsgi_thumb_path']?>" alt="" class="imagedisplay"></a>
                                                    <?php if($row['cmsgi_title'.$_SESSION['lang_fld_prefix']]!="") { echo '<h4>'.$row['cmsgi_title'.$_SESSION['lang_fld_prefix']].'</h4>';}?>
                                                    <?php if($row['cmsgi_desc'.$_SESSION['lang_fld_prefix']]!="") { echo '<p>'.$row['cmsgi_desc'.$_SESSION['lang_fld_prefix']].'</p>';}?>
                                                    <?php echo '</li>'; 
                                                } 
                                                if($Count_rows > 0) echo '</ul >';//echo '</div>';
                                            } 
                                     
                                        }
                                                            
                                }
                             */
                            if($db->total_records($rs)==0 ) {
                            echo '<h2>'.t_lang('M_TXT_CONTENT_COMING_SOON').'</h2>';
                            }
                        }
                        }
                        }else{
                                //$msg->addError(t_lang('M_TXT_PAGE_NOT_FOUND'));
                                redirectUser(friendlyUrl(CONF_WEBROOT_URL.'404.php'));
                                exit;
                        }	
                            
		
		?>     	 
                                       
               		
                    </div>
                    
                   
                </div>
            </div>    
       </div>    
    </section>
 
         
        <!--bodyContainer end here-->
<?php   include 'footer.php';     ?>