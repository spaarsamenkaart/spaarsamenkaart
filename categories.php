<?php
require_once 'application-top.php';
require_once 'includes/navigation-functions.php';
require_once 'header.php';


$rs = fetchCategories('both',0);
$deal_cat_arr = $db->fetch_all($rs);
?>
<section class="pagebar">
    <div class="fixed_container">
        <div class="row">
            <aside class="col-md-7 col-sm-7">
                <h3><?php echo t_lang('M_TXT_CATEGORIES'); ?></h3>
                <ul class="breadcrumb">
                   <li><a href="<?php friendlyUrl(CONF_WEBROOT_URL.'home.php'); ?>"><?php echo t_lang('M_TXT_HOME'); ?></a></li>
                   <li><?php echo t_lang('M_TXT_CATEGORIES'); ?></li>
                </ul>
            </aside>
           
        </div>
     </div>
</section>

<section class="page__container">
    <div class="fixed_container">
        <div class="row">
            <div class="col-md-12">
                <div class="cell" >
                <?php foreach ($deal_cat_arr as $key => $value) {
                                            ?>
                   <div class="cell__item" >
                            <div class="cell__list">
                                <div class="cell__head"><a <?php if ($_GET['cat'] == $value['cat_id']) { ?> class="active"<?php } else { ?> class="" <?php } ?> href="<?php echo friendlyUrl(CONF_WEBROOT_URL . 'category-deal.php?cat=' . $value['cat_id'] . '&type=both') ?>"><?php echo $value['cat_name' . $_SESSION['lang_fld_prefix']]; ?></a></div> 
                                <div class="cell__body">
                              
                                        
                                          <?php
                                                if ($value['cat_id']) {
                                                    echo $subCatArray = fetchProduct_categories($value['cat_id']);
                                                }
                                                ?>
                             
                                </div>
                            </div>
                        </div>
                          <?php } ?>
                       </div>
                   </div> 
                </div>
           </div>     
</section>                   
<?php

function fetchProduct_categories($parent_id) {
    global $db;

    $srch = new SearchBase('tbl_deal_categories', 'c');
    $srch->joinTable('tbl_deal_to_category', 'LEFT JOIN', 'dtc.dc_cat_id=c.cat_id ', 'dtc');
    $srch->joinTable('tbl_deals', 'INNER JOIN', 'dtc.dc_deal_id=d.deal_id ', 'd');
    $srch->addCondition('c.cat_parent_id', '=', $parent_id);
  //  $srch->addCondition('d.deal_type', '=', 1);
    $srch->addCondition('d.deal_status', '=', 1);
    $srch->addCondition('d.deal_complete', '=', 1);
    $srch->addOrder('c.cat_name', 'asc');
    $groupBy = 'c.cat_id';
    $findFld = array("c.cat_id,c.cat_name" . $_SESSION['lang_fld_prefix'] . "");
    $srch->addMultipleFields($findFld);
    $srch->addGroupBy($groupBy);
    $rs = $srch->getResultSet();
    $rows = $db->fetch_all_assoc($rs);
    $srch->getQuery();
    $str = '';



    $str.='<ul class="list__vertical links" >';
    foreach ($rows as $key1 => $val1) {
        $subCat = fetchProduct_categories($key1);
        strlen($subCat);

        if (strlen($subCat) > 40) {
            $url = friendlyUrl(CONF_WEBROOT_URL . 'category-deal.php?cat=' . $key1 . '&type=both');
            $str.= '<li class="parent_id_' . $key1 . '" ><a href="' . $url . '">' . $val1 . '</a>';
            $str.= $subCat;
        } else {

            $url = friendlyUrl(CONF_WEBROOT_URL . 'category-deal.php?cat=' . $key1 . '&type=both');
            $str.= '<li class="parent_id_' . $key1 . '" ><a href="' . $url . '">' . $val1 . '</a>';
        }
        $str.='</li>';
    }

    $str.='</ul>';


    return $str;
}
?>
<script src="<?php echo CONF_WEBROOT_URL; ?>js/masonry.pkgd.js"></script> 
<script type="text/javascript">

$('.cell').masonry({
  itemSelector: '.cell__item',
}); 
    
 /* for select city form */		 			 
$('.add__newcity-link').click(function(){
  $(this).toggleClass("active");
  $('.citysearch__form').slideToggle("600");
});      
    
</script> 
<?php
include 'footer.php';
?>
