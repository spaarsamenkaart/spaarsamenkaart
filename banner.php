<div class="innerbanner">
				<img src="<?php echo CONF_WEBROOT_URL;?>images/innerbanner.jpg" alt="">
				<div class="contentWrap">
					<div class="cell">
						<h2><?php echo t_lang('M_TXT_DISCOVER_THE_BEST_OF_YOUR_CITY');?></h2>
                        <p><?php echo t_lang('M_TXT_INTRODUCING_YOU_TO_THE_MOST_INTERESTING_EXPERIENCES_AT_THE_BEST_LOCAL_BUSINESSES');?></p>
					</div>
				</div>
           </div>