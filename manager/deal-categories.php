<?php   
require_once 'application-top.php';
checkAdminPermission(5);
$post=getPostedData();
$mainTableName='tbl_deal_categories';
$primaryKey='cat_id';
$colPrefix='cat_';
$page=(is_numeric($_REQUEST['page'])?$_REQUEST['page']:1);
$Src_frm=new Form('Src_frm', 'Src_frm');
$Src_frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
$Src_frm->setFieldsPerRow(3);
$Src_frm->captionInSameCell(true);
$Src_frm->addTextBox(t_lang('M_FRM_KEYWORD'), 'keyword', '', '','');
$Src_frm->addHiddenField('','mode','search');
$fld1=$Src_frm->addButton('&nbsp;', 'btn_cancel', t_lang('M_TXT_CLEAR_SEARCH'), '', ' class="inputbuttons" onclick=location.href="deal-categories.php"');
$fld=$Src_frm->addSubmitButton('&nbsp;', 'btn_search',  t_lang('M_TXT_SEARCH'), '', ' class="inputbuttons"')->attachField($fld1);


if(is_numeric($_GET['delete'])){
	if((checkAdminAddEditDeletePermission(5,'','delete')) ){
		/* function write in the site-function.php */
		deleteCategory($_GET['delete']);
		redirectUser('?page=' . $page);
	}else{
		die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
	}	
}

if($_SESSION['lang_fld_prefix'] == '_lang1')
$get_cat_name = 'IF(CHAR_LENGTH(cat_name_lang1),cat_name_lang1,cat_name) as cat_name';
else $get_cat_name = 'cat_name';
if(isset($_GET['edit']) && ($_GET['edit']>0)){
	$edit=$_GET['edit'];
	
	$rs=$db->query("select cat_code from tbl_deal_categories where cat_id=" . $edit);
	if(!$row=$db->fetch($rs)) die('Invalid Request');
	$code=$row['cat_code'];
	$rsc=$db->query("SELECT cat_id, ".$get_cat_name.",cat_code,cat_parent_id FROM `tbl_deal_categories` WHERE `cat_code` NOT LIKE '$code%' ORDER BY cat_code asc, cat_display_order asc");
}else{
	$rsc=$db->query("SELECT  cat_id, ".$get_cat_name.",cat_code,cat_parent_id FROM `tbl_deal_categories` ORDER BY cat_code asc, cat_display_order asc");
}

$parentArray=array();
$parentArray[0]='Select';

while($arrs=$db->fetch($rsc)){

$checkCode = strlen($arrs['cat_code'])/5;
if( $checkCode == 1  ){

$arrow = "";
}
if( $checkCode > 1  ){
$arrow = "->";
}
	$parentArray[$arrs['cat_id']]=str_repeat($arrow,$checkCode -1 )." ".$arrs['cat_name'];
}

$frm=getMBSFormByIdentifier('frmDealCategories');
$fld =$frm->getField('cat_name');
$fld->setUnique('tbl_deal_categories','cat_name'.$_SESSION['lang_fld_prefix'],'cat_id','cat_id','cat_id');
$fld =$frm->getField('cat_image');
$fld->field_caption =unescape_attr(t_lang('M_FRM_CATEGORY_IMAGE'));
$frm->removeField($fld);
$fld = $frm->getField('cat_bg_image');
$frm->removeField($fld);
$fld = $frm->getField('cat_layout');
$frm->removeField($fld);
$fld1 = $frm->getField('cat_parent_id');
$fld1->field_caption=t_lang('M_TXT_PARENT_CATEGORY');
$fld1->options=$parentArray;
$fld1 = $frm->getField('btn_submit');
$fld=$frm->addButton('', 'btn_submit_cancel', t_lang('M_TXT_CANCEL'), '',  ' class="inputbuttons" onclick=location.href="deal-categories.php"')->attachField($fld1);
$frm->setAction('?page=' . $page);
updateFormLang($frm);

if(is_numeric($_GET['edit'])){
	if((checkAdminAddEditDeletePermission(5,'','edit')) ){
		$record=new TableRecord($mainTableName);
		if(!$record->loadFromDb($primaryKey . '=' . $_GET['edit'], true)){
			$msg->addError($record->getError());
		}
		else{
			$arr=$record->getFlds();
			$arr['btn_submit']=t_lang('M_TXT_UPDATE');
			$frm->addHiddenField('','oldname',$arr['cat_name']);
			fillForm($frm,$arr);
		   /*  $frm->fill($arr); */
			$msg->addMsg(t_lang('M_TXT_CHANGE_THE_VALUES'));
		}
	}else{
		die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
	}	
	
}

if($_SERVER['REQUEST_METHOD']=='POST' && !isset($_POST['btn_search']) && isset($_POST['btn_submit'])){
    $post=getPostedData();
    if(!$frm->validate($post)){
        $errors=$frm->getValidationErrors();
        foreach ($errors as $error) $msg->addError($error);
    }
    else{
        $record=new TableRecord($mainTableName);
        /* $record->assignValues($post); */
		$arr_lang_independent_flds = array('company_id','cat_display_order','cat_code','cat_parent_id','cat_is_featured','cat_layout','mode','btn_submit');
			$data['new_name']=$post['cat_name'];
			
			}
		assignValuesToTableRecord($record,$arr_lang_independent_flds,$post);
		if((checkAdminAddEditDeletePermission(5,'','edit')) ){
			if($post[$primaryKey]>0) $success = $record->update($primaryKey . '=' . $post[$primaryKey]);
		}

		if((checkAdminAddEditDeletePermission(5,'','add')) ){
			if($post[$primaryKey]=='') $success = $record->addNew(); 
		}
		
		
		#$success=($post[$primaryKey]>0)?$record->update($primaryKey . '=' . $post[$primaryKey]):$record->addNew();
        if($success){
			
			$cat_id=($post[$primaryKey]>0)?$post[$primaryKey]:$record->getId();
			if($post[$primaryKey]==''){
				$code=str_pad($cat_id, 5, '0', STR_PAD_LEFT);
				$db->query("update tbl_deal_categories set cat_code='$code' where cat_id=$cat_id");
			}	
			if(is_uploaded_file($_FILES['cat_image']['tmp_name'])){
                $ext=strtolower(strrchr($_FILES['cat_image']['name'], '.'));
                if(!in_array($ext, array('.gif', '.jpg', '.jpeg','.png'))){
                    $msg->addError(t_lang('M_TXT_CATEGORY_IMAGE').' '.t_lang('M_ERROR_IMAGE_COULD_NOT_SAVED_NOT_SUPPORTED'));
                }
                else{
                    $flname=time() . '_' . $_FILES['cat_image']['name'];
                    if(!move_uploaded_file($_FILES['cat_image']['tmp_name'], '../category-images/' . $flname)){
                        $msg->addError(t_lang('M_TXT_FILE_COULD_NOT_SAVE'));
                    }
                    else{
						$getImg=$db->query("select * from tbl_deal_categories where cat_id='".$cat_id."'");
						$imgRow=$db->fetch($getImg);
						unlink('../category-images/'.$imgRow['cat_image'.$_SESSION['lang_fld_prefix']]);
                        $db->update_from_array('tbl_deal_categories', array('cat_image'.$_SESSION['lang_fld_prefix']=>$flname), 'cat_id=' . $cat_id);
                    }
                }
            }
			
			if(is_uploaded_file($_FILES['cat_bg_image']['tmp_name'])){
                $ext=strtolower(strrchr($_FILES['cat_bg_image']['name'], '.'));
                if(!in_array($ext, array('.gif', '.jpg', '.jpeg','.png'))){
                    $msg->addError('Category background image could not be saved. Only gif, jpg and jpeg images are supported.');
                }
                else{
                    $flname=time() . '_' . $_FILES['cat_bg_image']['name'];
                    if(!move_uploaded_file($_FILES['cat_bg_image']['tmp_name'], '../background-images/' . $flname)){
                        $msg->addError('File could not be saved.');
                    }
                    else{
						$getImg=$db->query("select * from tbl_deal_categories where cat_id='".$cat_id."'");
						$imgRow=$db->fetch($getImg);
						unlink('../background-images/'.$imgRow['cat_bg_image'.$_SESSION['lang_fld_prefix']]);
                        $db->update_from_array('tbl_deal_categories', array('cat_bg_image'.$_SESSION['lang_fld_prefix']=>$flname), 'cat_id=' . $cat_id);
                    }
                }
            }
			
			$rs=$db->query("select * from tbl_deal_categories where cat_id=" . $cat_id);
			if(!$row=$db->fetch($rs)) die('Invalid Request');
			$old_code=$row['cat_code'];
			
			$new_code=getDealCategoryCode($cat_id, $row['cat_parent_id']);
			$qry="update tbl_deal_categories set cat_code=REPLACE(cat_code, '" . $old_code . "', '" . $new_code . "')";
			if(!$db->query($qry)){
				$msg->addError($db->getError());
			}
            $msg->addMsg(T_lang('M_TXT_ADD_UPDATE_SUCCESSFULL'));
            redirectUser();
        }
        else{
            $msg->addError(t_lang('M_TXT_COULD_NOT_ADD_UPDATE') . $record->getError());
            /* $frm->fill($post); */
			fillForm($frm,$post);
        }
    }


 	$srch = new SearchBase('tbl_deal_categories', 'm');
	$srch->joinTable('tbl_deal_categories', 'LEFT OUTER JOIN', 'm.cat_parent_id = p.cat_id', 'p');
	$srch->addMultipleFields(array('m.*', "CONCAT(CASE WHEN m.cat_parent_id = 0 THEN '' ELSE LPAD(p.cat_display_order, 7, '0') END, LPAD(m.cat_display_order, 7, '0')) AS display_order"));
	$srch->addOrder('display_order');

if($post['mode']=='search')
{
	if($post['keyword'] != '') 	
	{
		$cnd=$srch->addDirectCondition('0');
		$cnd->attachCondition('m.cat_name'.$_SESSION['lang_fld_prefix'], 'like','%'. $post['keyword'].'%' ,'OR');
	}
	$Src_frm->fill($post);
}
$pagesize=30;
	$srch->setPageNumber($page);
	$srch->setPageSize($pagesize);
$rs_listing=$srch->getResultSet();
 
$pagestring='';

$pages=$srch->pages();

$pagestring .= createHiddenFormFromPost('frmPaging', '?', array('page'), array('page'=>''));
$pagestring .= '<div class="pagination "><ul>';
$pageStringContent='<a href="javascript:void(0);">' . t_lang('M_TXT_DISPLAYING_RECORDS') . ' ' . (($page - 1) * $pagesize + 1) .
		' ' . t_lang('M_TXT_TO') . ' ' . (($page * $pagesize > $srch->recordCount()) ? $srch->recordCount() : ($page * $pagesize)) . ' ' . t_lang('M_TXT_OF') . ' ' . $srch->recordCount() . '</a>';
$pagestring .= '<li><a href="javascript:void(0);">' . t_lang('M_TXT_GOTO') . ': </a></li>
	' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
				, $srch->pages(), $page, '<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
$pagestring .= '</div>';

$arr_listing_fields=array(
'listserial'=>t_lang('M_TXT_SR_NO'),
'cat_name'.$_SESSION['lang_fld_prefix']=>t_lang('M_FRM_NAME'),
//'cat_display_order'=>t_lang('M_TXT_MANAGE_CHILD_DISPLAY_ORDER'),
'action'=>t_lang('M_TXT_ACTION')
);

include 'header.php';
$arr_bread=array(
'index.php'=>'<img alt="Home" src="images/home-icon.png">',
'deals.php'=>t_lang('M_TXT_DEALS').'/'.t_lang('M_TXT_PRODUCTS'),
''=>t_lang('M_FRM_DEAL_CATEGORIES')
);


?>
<script type="text/javascript">
	$(document).ready(function() {
	//Table DND call
		$('#category_listing').tableDnD({
			onDrop: function(table, row) {
				var order= $.tableDnD.serialize('id');
					 callAjax('cms-ajax.php', order+'&mode=REORDER_CATEGORY', function(t){
						$.facebox(t);
					 });
			}

		});
	}); 
txtcatdel = "<?php echo t_lang('M_TXT_CATEGORY_DELETION_NOT_ALLOWED');?>";	
txtsuredel = "<?php echo t_lang('M_TXT_ARE_YOU_SURE_TO_DELETE');?>";	
</script>


</div></td>
<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
                
			<div class="div-inline">
				<div class="page-name"><?php echo t_lang('M_FRM_DEAL_CATEGORIES');?>
					<ul class="actions right">
					   <li class="droplink">
							<a href="javascript:void(0)"><i class="ion-android-more-vertical icon"></i></a>
							<div class="dropwrap">
								<ul class="linksvertical">
									<?php if((checkAdminAddEditDeletePermission(5,'','add')) ){?>
										<li><a href="?page=<?php echo $page; ?>&add=new"><?php echo t_lang('M_TXT_ADD_NEW_CATEGORY');?></a></li>
									<?php } ?>
									<li>  <a href="category-display-order.php" ><?php echo t_lang('M_TXT_MANAGE_DISPLAY_ORDER'); ?></a> </li>
								</ul>
							</div>
						</li>
					</ul>
				</div>
			</div>
				
			<div class="clear"></div>
			
			<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
			
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				
				<?php  
				if(is_numeric($_REQUEST['edit']) || $_REQUEST['add']=='new'){
					if((checkAdminAddEditDeletePermission(3,'','add')) ||(checkAdminAddEditDeletePermission(3,'','edit')) ){?>
						<div class="box"><div class="title"> <?php echo t_lang('M_FRM_DEAL_CATEGORIES');?> </div><div class="content"><?php echo  $frm->getFormHtml();?></div></div>
					<?php }else{
						die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
					}
				}else{
				?>
								 
							 
				 
				<div class="box searchform_filter"><div class="title"> <?php echo t_lang('M_FRM_DEAL_CATEGORIES');?> </div><div class="content togglewrap" style="display:none;">	<?php echo $Src_frm->getFormHtml();?>			
				</div></div>
				<div class="box tablebox">				
				<table class="tbl_data" width="100%" id="category_listing1">
					<thead>
					<tr>
					<?php 
					foreach ($arr_listing_fields as $val) echo '<th>' . $val . '</th>';
					?>
					</tr>
					</thead>
<?php 
for($listserial=($page-1)*$pagesize+1; $row=$db->fetch($rs_listing); $listserial++){
	if($listserial%2 == 0) $even = 'even'; else $even = ''; 
    echo '<tr class=" ' . $even . ' " ' . (($row[$colPrefix . 'active']=='0')?' class="inactive"':'') . ' id = '.$row['cat_id'].'>';
    foreach ($arr_listing_fields as $key=>$val){
        echo '<td>';
        switch ($key){
            case 'listserial':
                echo $listserial;
                break;
			case 'cat_name_lang1':
				$level=strlen($row['cat_code'])/5 - 1;
				for($i=0; $i<$level; $i++) echo '&mdash;&raquo;&nbsp;';
				echo '<strong>'.$arr_lang_name[0].'</strong>'. ' ' .$row['cat_name'].'<br>';
				echo '<strong>'.$arr_lang_name[1].'</strong>'. ' ' .$row['cat_name_lang1'];
                break;
			case 'cat_name':
				$level=strlen($row['cat_code'])/5 - 1;
				for($i=0; $i<$level; $i++) echo '&mdash;&raquo;&nbsp;';
				echo $row['cat_name'];
				
                break;
            /* case 'cat_display_order':
				if(($row['children']>0) || $count == 1){
					echo '<a href="category-display-order.php?cat_id=' . $row[$primaryKey]  .'" class="btn gray">' .t_lang('M_TXT_MANAGE_CHILD_DISPLAY_ORDER') .'</a>';
				}
				 break; */
				
			case 'action':
				 
				echo '<ul class="actions">';
				if((checkAdminAddEditDeletePermission(5,'','edit')) ){
					echo '<li><a href="?edit=' . $row[$primaryKey] . '" title="' . t_lang('M_TXT_EDIT') . '"><i class="ion-edit icon"></i></a></li>';
				}
				
				if(!($row['children']>0)){
					if((checkAdminAddEditDeletePermission(5,'','delete')) ){
						echo '<li><a href="javascript:void(0);" title="' . t_lang('M_TXT_DELETE') . '" onclick="deleteCategory(' . $row[$primaryKey] . ');"><i class="ion-android-delete icon"></i></a></li>';
						#echo '<a href="?delete=' . $row[$primaryKey] . '" title="Delete" onclick="return(confirm(\'Are you sure to delete?\'));" class="btn delete">Delete</a> ';
					}
				} 
				echo '</ul>';
                break;
            default:
                echo $row[$key];
                break;
        }
        echo '</td>';
    }
    echo '</tr>';
}
if($db->total_records($rs_listing)==0) echo '<tr><td colspan="' . count($arr_listing_fields) . '">' . t_lang('M_TXT_NO_RECORD_FOUND') . '</td></tr>';
?>
</table>
<?php if ($srch->pages() > 1) {  ?>
			<div class="footinfo">
				<aside class="grid_1">
					<?php echo $pagestring; ?>	 
				</aside>  
				<aside class="grid_2"><span class="info"><?php echo $pageStringContent; ?></span></aside>
			</div>
		<?php 
		} ?>

</div>

				
	<?php   } ?>
	</td>
<?php 
include 'footer.php';
?>
