<?php
require_once 'application-top.php';

checkAdminPermission(9);

$page=(is_numeric($_GET['page'])?$_GET['page']:1);
$pagesize=50;

$mainTableName='tbl_admin';
$primaryKey='admin_id';
$colPrefix='admin_';

if(is_numeric($_GET['delete']) && $_GET['delete']>1 && $_GET['delete'] != $_SESSION['admin_logged']['admin_id']){ 
	// id 1 is superadmin and can not be deleted
    //if(!$db->update_from_array($mainTableName, array($colPrefix . 'deleted'=>1), $primaryKey . '=' . $_GET['delete'])){
	if((checkAdminAddEditDeletePermission(9,'','delete')) ){
		deleteAdminUser($_GET['delete']);
		redirectUser('?page=' . $page);
	}else{
		die(t_lang("M_TXT_UNAUTHORIZED_ACCESS"));
	}
	
}


//$frm=getMBSFormByIdentifier('frmCompany');
$frm = new Form('frmAdmin');
$frm->setAction('?page=' . $page);
$frm->setJsErrorDisplay('afterfield');
$frm->setTableProperties('width="100%" border="0" cellspacing="0" cellpadding="0" class="tbl_form"'); 
$frm->addHiddenField('', 'admin_id', '', 'admin_id');
$fld = $frm->addRequiredField(t_lang('M_TXT_USERNAME'), 'admin_username');
$fld->requirements()->setUsername();
$fld->setUnique($mainTableName, 'admin_username', $primaryKey, 'admin_id', 'admin_id');
$fld = $frm->addPasswordField(t_lang('M_FRM_PASSWORD'), 'admin_password');
$fld = $fld->requirements()->setRequired();
$frm->addRequiredField(t_lang('M_FRM_NAME'), 'admin_name');
$fld1 = $frm->addEmailField(t_lang('M_FRM_EMAIL_ADDRESS'), 'admin_email');
$fld1->requirements()->setRequired();
$fld1->setUnique($mainTableName, 'admin_email', $primaryKey, 'admin_id', 'admin_id');

if ($_SESSION['admin_logged']['admin_id'] != 1){
	$srch = new SearchBase('tbl_admin_permissions');
	$srch->addCondition('ap_admin_id', '=', $_SESSION['admin_logged']['admin_id']);
	
	$rs = $srch->getResultSet();
	$arr_my_permissions = array();
	while($row = $db->fetch($rs)) $arr_my_permissions[$row['ap_permission_id']] = $row;
	
	 
}


$srch = new SearchBase('tbl_permissions', 'p');
$srch->addOrder('permission_order');
$srch->addMultipleFields('permission_id', 'permission_name'.$_SESSION['lang_fld_prefix']);
/* $rs_permissions = $srch->getResultSet(); */
$rs_permissions = $db->query('select permission_id , permission_name'.$_SESSION['lang_fld_prefix'].' from tbl_permissions order by permission_order'); 
$arr_permissions = $db->fetch_all_assoc($rs_permissions);

//print_r($arr_permissions);
foreach($arr_permissions as $key=>$val){
	//$fld = $frm->addCheckbox($val, 'permission_view[' . $key . ']', 1, );
	if ($_SESSION['admin_logged']['admin_id'] != 1){
		$enabled = is_array($arr_my_permissions[$key]);
	}
	else {
		$enabled = true;
	}
	$fld = $frm->addCheckbox($val, 'ap_permission_view[' . $key . ']', 1, '', (($enabled)?'':' disabled="disabled"'));
	$fld = $frm->getField('ap_permission_view[' . $key . ']');
	$fld->html_before_field='<strong>&nbsp;&nbsp;&nbsp;&nbsp;'.t_lang('M_TXT_VIEW').':&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	 
	
	if ($_SESSION['admin_logged']['admin_id'] != 1){
		$enabled = ($arr_my_permissions[$key]['ap_permission_add']==1);
	}
	$fld1 = $frm->addCheckbox($val, 'ap_permission_add[' . $key . ']', 1, '', (($enabled)?'':' disabled="disabled"'));
	$fld1 = $frm->getField('ap_permission_add[' . $key . ']');
	$fld1->html_before_field='<strong>&nbsp;&nbsp;&nbsp;&nbsp;'.t_lang('M_TXT_ADD').':&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	$fld->attachField($fld1);
	
	
	
	if ($_SESSION['admin_logged']['admin_id'] != 1){
		$enabled = ($arr_my_permissions[$key]['ap_permission_edit']==1);
	}
	$fld1 = $frm->addCheckbox($val, 'ap_permission_edit[' . $key . ']', 1, '', (($enabled)?'':' disabled="disabled"'));
	$fld1 = $frm->getField('ap_permission_edit[' . $key . ']');
	$fld1->html_before_field='<strong>&nbsp;&nbsp;&nbsp;&nbsp;'.ucfirst(t_lang('M_TXT_EDIT')).':&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	$fld->attachField($fld1);
	
	if ($_SESSION['admin_logged']['admin_id'] != 1){
		$enabled = ($arr_my_permissions[$key]['ap_permission_delete']==1);
	}
	$fld1 = $frm->addCheckbox($val, 'ap_permission_delete[' . $key . ']', 1, '', (($enabled)?'':' disabled="disabled"'));
	$fld1 = $frm->getField('ap_permission_delete[' . $key . ']');
	$fld1->html_before_field='<strong>&nbsp;&nbsp;&nbsp;&nbsp;'.	ucfirst(strtolower(t_lang('M_TXT_DELETE'))).':&nbsp;&nbsp;&nbsp;&nbsp;</strong>';
	$fld->attachField($fld1);
	
}

$fld = $frm->addHtml('<span style="color:red;">' . t_lang('M_TXT_ADMIN_NOTE') . ' </span>','','');
$fld->merge_caption=2;

$frm->addSubmitButton('', 'btn_submit', t_lang('M_TXT_SUBMIT'),'','class="inputbuttons"');

if(is_numeric($_GET['edit']) && $_GET['edit']>1 && $_GET['edit'] != $_SESSION['admin_logged']['admin_id']){
	if((checkAdminAddEditDeletePermission(9,'','edit')) ){
		editAdminUser($_GET['edit']);
		/* function is placed in the site-function.php file */
	}else{
		die(t_lang("M_TXT_UNAUTHORIZED_ACCESS"));
	}
}

if($_SERVER['REQUEST_METHOD']=='POST'){
    $post=getPostedData();
	if(!$frm->validate($post)){
		$errors=$frm->getValidationErrors();
		foreach ($errors as $error) $msg->addError($error);
	}else{
		addAdminUser($post);
		/* function is placed in the site-function.php file */
	}
}


$srch=new SearchBase($mainTableName);
$srch->addCondition('admin_id', '>', 1);
$srch->addCondition('admin_id', '!=', $_SESSION['admin_logged']['admin_id']);
$srch->addOrder('admin_name');

$srch->setPageNumber($page);
$srch->setPageSize($pagesize);
$rs_listing=$srch->getResultSet();

$pagestring='';

$pages=$srch->pages();

	$pagestring .= '<div class="pagination "><ul>';
	$pageStringContent='<a href="javascript:void(0);">' . t_lang('M_TXT_DISPLAYING_RECORDS') . ' ' . (($page - 1) * $pagesize + 1) .
			' ' . t_lang('M_TXT_TO') . ' ' . (($page * $pagesize > $srch->recordCount()) ? $srch->recordCount() : ($page * $pagesize)) . ' ' . t_lang('M_TXT_OF') . ' ' . $srch->recordCount() . '</a>';
	$pagestring .= '<li><a href="javascript:void(0);">' . t_lang('M_TXT_GOTO') . ': </a></li>
		' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
					, $srch->pages(), $page, '<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
	
	//$pagestring .= '<li>'.t_lang('M_TXT_GOTO').': </li>' . getPageString('<li><a href="?page=xxpagexx">xxpagexx</a> </li> ', $srch->pages(), $page,'<li class="selected"><b>xxpagexx</b></li>');
	
	
	$pagestring .= '</div><div class="tblheading" style="margin:-4px 0px;padding:5px 0 6px 6px;"> &nbsp;</div>';

$arr_listing_fields=array(
'listserial'=>t_lang('M_TXT_SR_NO'), 
'admin_name'=>t_lang('M_FRM_NAME'),
'admin_username'=>t_lang('M_TXT_USERNAME'),
'admin_email'=>t_lang('M_FRM_EMAIL'),
'action'=>t_lang('M_TXT_ACTION')
);

include 'header.php';
$arr_bread=array(
'index.php'=>'<img class="home" alt="Home" src="images/home-icon.png">',
''=>t_lang('M_TXT_ADMIN_USERS')
);
 
?>
</div></td>
<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
                
               <div class="div-inline">
					<div class="page-name"><?php echo t_lang('M_TXT_ADMIN_USERS'); ?> 
						<?php if(checkAdminAddEditDeletePermission(9,'','add')){?> 
							<ul class="actions right">
							   <li class="droplink">
									<a href="javascript:void(0)"><i class="ion-android-more-vertical icon"></i></a>
									<div class="dropwrap">
										<ul class="linksvertical">
											<li>  
											<a href="?page=<?php echo $page; ?>&add=new"><?php echo t_lang('M_TXT_ADD_NEW');?></a>
										</li>
										</ul>
									</div>
								</li>
							</ul>
						<?php } ?> 
					</div>
				</div>
				
				<div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                   <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="message error"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				
				<?php  
				if(is_numeric($_REQUEST['edit']) || $_REQUEST['add']=='new'){
				 

					if((checkAdminAddEditDeletePermission(9,'','add')) ||(checkAdminAddEditDeletePermission(9,'','edit')) ){?>
						<div class="box"><div class="title"><?php echo t_lang('M_TXT_ADMIN_USERS');?> </div><div class="content"><?php echo  $frm->getFormHtml();?></div></div>
					<?php }else{
						die(t_lang("M_TXT_UNAUTHORIZED_ACCESS"));
					}
				}else{
				?>

<table class="tbl_data" width="100%">
<thead>
<tr>
<?php 
foreach ($arr_listing_fields as $val) echo '<th>' . $val . '</th>';
?>
</tr>
</thead>
<?php 
for($listserial=($page-1)*$pagesize+1; $row=$db->fetch($rs_listing); $listserial++){
    echo '<tr' . (($row[$colPrefix . 'active']=='0')?' class="inactive"':'') . '>';
    foreach ($arr_listing_fields as $key=>$val){
        echo '<td ' . (($key==action)?  'width="20%"':'') . '>';
        switch ($key){
            case 'listserial':
                echo $listserial;
                break;
            case 'action':
                echo '<ul class="actions">';
				
				echo '<li><a href="?edit=' . $row[$primaryKey] . '&page=' . $page . '" title="' . t_lang('M_TXT_EDIT') . '"><i class="ion-edit icon"></i></a></li>';
                echo '<li><a href="?delete=' . $row[$primaryKey] . '&page=' . $page . '" title="' . t_lang('M_TXT_DELETE') . '" onclick="return(confirm(\'' . t_lang('M_MSG_REALLY_WANT_TO_DELETE_THIS_RECORD') . '\'));"><i class="ion-android-delete icon"></i></a></li>';
                
				echo '</ul>';
				break;
            default:
                echo $row[$key];
                break;
        }
        echo '</td>';
    }
    echo '</tr>';
}
if($db->total_records($rs_listing)==0) echo '<tr><td colspan="' . count($arr_listing_fields) . '">' . t_lang('M_TXT_NO_RECORD_FOUND') . '</td></tr>';
?>
</table>
	<?php if ($srch->pages() > 1) {  ?>
		<div class="footinfo">
			<aside class="grid_1">
				<?php echo $pagestring; ?>	 
			</aside>  
			<aside class="grid_2"><span class="info"><?php echo $pageStringContent; ?></span></aside>
		</div>
	<?php 
	}
} ?>

<strong><?php echo t_lang('M_TXT_SUPER_ADMIN_NOTE');?></strong>
</td>
<?php 
include 'footer.php';
?>
