var txtcatdel;
var txtsuredel;
function deleteCategory(category,page) {
	
	callAjax('common-ajax.php', 'mode=deleteCategory&category='+category, function(t) {
		
		var total_deals = parseInt(t);
		
		if ( total_deals > 0 ) {
			alert(txtcatdel);
		} else {
			if ( confirm(txtsuredel) ) {
				document.location.href = '?delete='+category+'&page='+page;
			}
		}
	});
}