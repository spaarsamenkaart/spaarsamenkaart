var txtdelcomp;
var txtnotallowed;
var txtinactive;
var txtstatusup;

function deleteCompany(company,page) {
	
	callAjax('company-ajax.php', 'mode=deleteCompany&company='+company, function(t) {
	 
		var total_deals = parseInt(t);
		
		if ( total_deals > 0 ) {
			alert(txtnotallowed);
		} else {
			if ( confirm(txtdelcomp) ) {
				document.location.href = '?delete='+company+'&page='+page;
			}
		}
	});
}

function activeCompany(company_id,status){
	
	var status;
	var company_id;
 	if(status=='0') 
	{
		callAjax('company-ajax.php', 'mode=disapproveUser&company_id='+company_id, function(t){
			if(t == 1){
				//$("#comment-status"+company_id).html('<img src="../images/ajax2.gif" alt="Loading...">');
				$.facebox(txtstatusup);
				$(document).bind('close.facebox', function() {
					window.location.reload(true);
				});
			}else{
				alert(txtinactive);
				$('#original_span'+company_id).html('');
				$('#original_span'+company_id).html('<span class="statustab addmarg" id="comment-status'+company_id+'" onclick="activeCompany('+company_id+',0);"><span class="switch-labels" data-off="Active" data-on="Inactive"></span><span class="switch-handles"></span></span>');
			}
			
		});
	}
 	if(status=='1'){
		//$('#comment-status'+company_id).html('<img src="../images/ajax2.gif" alt="Loading...">');
		callAjax('company-ajax.php', 'mode=approveUser&company_id='+company_id, function(t){
			 
			if(t == 1){
				$.facebox(txtstatusup);
				$(document).bind('close.facebox', function() {
					window.location.reload(true);
				});
			}else{
				alert(txtinactive);
				$('#original_span'+company_id).html('');
				$('#original_span'+company_id).html('<span class="statustab addmarg active" id="comment-status'+company_id+'" onclick="activeCompany('+company_id+',1);"><span class="switch-labels" data-off="Active" data-on="Inactive"></span><span class="switch-handles"></span></span>');
			}
		});
	}

}

 function companyChangePassword(company_id){
	jQuery.facebox(function() {
			callAjax('company-ajax.php', 'mode=changePassword&company_id='+company_id, function(t){
			$.facebox(t);
		});
	});
	
} 

function submitChangePassword(frm,v){ 
	var v;
	v.validate();
	if(!v.isValid()) return;
	var data=getFrmData(frm);
	jQuery.facebox(function() {
	callAjax('company-ajax.php', data, function(t){$.facebox(t);});
	});
	  
}

function companyLocation(company){
	jQuery.facebox(function() {
		callAjax('company-ajax.php', 'mode=companyLocations&company='+company, function(t) {
			$.facebox(t);
		});
	});
}

function addTransaction(company){
	jQuery.facebox(function() {
		callAjax('company-ajax.php', 'mode=addTransaction&company='+company, function(t) {
			$.facebox(t);
		});
	});	
}

function submitAddTransaction(frm,v){ 
	var v;
	v.validate();
	if(!v.isValid()) return;
	var data=getFrmData(frm);
	//$.facebox('Processing...');
	callAjax('company-ajax.php', data, function(t){$.facebox(t);});
	  
}