<?php
require_once 'application-top.php';
require_once '../includes/navigation-functions.php';
checkAdminPermission(4);
$mainTableName = 'tbl_tax_rates';
$primaryKey = 'taxrate_id';
$colPrefix = 'trate_';
$page = (is_numeric($_REQUEST['page']) ? $_REQUEST['page'] : 1);
$Src_frm = new Form('Src_frm', 'Src_frm');
$Src_frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
$Src_frm->setFieldsPerRow(3);
$Src_frm->captionInSameCell(true);
$Src_frm->addTextBox(t_lang('M_TXT_TAX_NAME'), 'zone', $_REQUEST['zone'], '', '');
$Src_frm->addHiddenField('', 'mode', 'search');
$Src_frm->addHiddenField('', 'status', $_REQUEST['status']);
$fld1 = $Src_frm->addButton('', 'btn_cancel', t_lang('M_TXT_CLEAR_SEARCH'), '', ' class="medium" onclick=location.href="tax-rate.php"');
$fld = $Src_frm->addSubmitButton('', 'search', t_lang('M_TXT_SEARCH'), '', ' class="medium"')->attachField($fld1);
if (is_numeric($_REQUEST['delete'])) {


    if (checkAdminAddEditDeletePermission(4, '', 'delete')) {
		
		$srch = new SearchBase('tbl_tax_rules', 'tr');
			$srch->addCondition('taxrule_taxrate_id', '=', $_REQUEST['delete']);
			$rs= $srch->getResultSet();
		$total_record=	$srch->recordCount();
		if($total_record==0){
        $db->query("delete from tbl_tax_rates  WHERE taxrate_id =" . $_REQUEST['delete']);
        $msg->addMsg(t_lang('M_TXT_RECORD_DELETED'));

        redirectUser('?page=' . $page . '&status=' . $_REQUEST['status']);
		}else{
			 $msg->addError(t_lang('M_TXT_RECORD_CAN_NOT_DELETE').'. '.t_lang('M_TXT_MULTIPLE_TAX_CLASSES_ATTACH_WITH_THIS_TAX_RATE.'));

        redirectUser('?page=' . $page . '&status=' . $_REQUEST['status']);
		}
    } else {
        die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
    }
}



$frm = new Form('taxrate_frm', 'taxrate_frm');
$frm->setTableProperties('border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
$frm->setFieldsPerRow(1);
$frm->captionInSameCell(false);
$frm->setAction('?page=' . $page);
$fld = $frm->addRequiredField(t_lang('M_TXT_TAX_NAME'), 'taxrate_name', '', 'tax_name');
$fld->setUnique('tbl_tax_rates', 'taxrate_name', 'taxrate_id', 'taxrate_id', 'taxrate_id');
$frm->addFloatField(t_lang('M_FRM_TAX_RATE'), 'taxrate_tax_rate', '', 'tax_rate')->requirements()->setRequired();
$srch = new SearchBase('tbl_tax_geo_zones', 'tgz');
$srch->addCondition('tgz.geozone_active', '=', '1');
$srch->doNotCalculateRecords();
$srch->doNotLimitRecords();
$srch->addFld('geozone_id');
$srch->addFld('geozone_name');
$srch->addOrder('geozone_name');
$rs = $srch->getResultSet();
$arr_options = $db->fetch_all_assoc($rs);
$frm->addSelectBox(t_lang('M_FRM_ZONE'), 'taxrate_geozone_id', $arr_options, '', '', t_lang('M_TXT_SELECT'), 'taxrate_geozone_id');
$status = array(1 => t_lang('M_TXT_ACTIVE'), 0 => t_lang('M_TXT_INACTIVE'));
$frm->addSelectBox(t_lang('M_FRM_STATUS'), 'taxrate_active', $status, '');
$frm->setJsErrorDisplay('afterfield');
$frm->addSubmitButton('', 'btn_submit', t_lang('M_TXT_SUBMIT'), '', 'class="medium"');
updateFormLang($frm);

$selected_state = 0;
if (is_numeric($_REQUEST['edit'])) {
    if (checkAdminAddEditDeletePermission(4, '', 'edit')) {
        $record = new TableRecord('tbl_tax_rates');

        if (!$record->loadFromDb('taxrate_id=' . $_REQUEST['edit'], true)) {
            $msg->addError($record->getError());
        } else {
            $arr = $record->getFlds();
            $frm->addHiddenField('', 'taxrate_id', $_REQUEST['edit']);
            fillForm($frm, $arr);
            $msg->addMsg(t_lang('M_TXT_CHANGE_THE_VALUES'));
        }
    } else {
        die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
    }
}

if (isset($_POST['btn_submit'])) {
    $post = getPostedData();
    print_r($post);
    if (!$frm->validate($post)) {
        $errors = $frm->getValidationErrors();
        foreach ($errors as $error)
            $msg->addError($error);
    } else {
        $record = new TableRecord('tbl_tax_rates');
        $arr_lang_independent_flds = array('taxrate_id', 'taxrate_name', 'taxrate_tax_rate', 'taxrate_geozone_id', 'taxrate_active', 'btn_submit');

        assignValuesToTableRecord($record, $arr_lang_independent_flds, $post);
        if ((checkAdminAddEditDeletePermission(4, '', 'edit'))) {
            if (((int) $post['taxrate_id']) > 0 || $post['taxrate_id'] == "0")
                $success = $record->update('taxrate_id' . '=' . $post['taxrate_id']);
        }
        if ((checkAdminAddEditDeletePermission(4, '', 'add'))) {
            if ($post['taxrate_id'] == '') {
                $success = $record->addNew();
            }
        }

        if ($success) {
            $taxrate_id = ($post[$primaryKey] >= 0) ? $post[$primaryKey] : $record->getId();

            $msg->addMsg(t_lang('M_TXT_ADD_UPDATE_SUCCESSFULL'));
            redirectUser('?page=' . $page . '&status=' . $_REQUEST['status']);
        } else {
            $msg->addError(t_lang('M_TXT_COULD_NOT_ADD_UPDATE') . $record->getError());
            fillForm($frm, $arr);
        }
    }
}


$srch = new SearchBase('tbl_tax_rates', 'tr');
if ($_REQUEST['status'] == 'active') {
    $srch->addCondition('taxrate_active', '=', 1);
} else if ($_REQUEST['status'] == 'deactive') {
    $srch->addCondition('taxrate_active', '=', 0);
} else {
    $srch->addCondition('taxrate_active', '=', 1);
}

if ($_POST['zone']) {
    $srch->addCondition('taxrate_name', 'LIKE', '%' . $_POST['zone'] . '%');
}

$srch->joinTable('tbl_tax_geo_zones', 'INNER JOIN', 'gz.geozone_id=tr.taxrate_geozone_id', 'gz');
$srch->addMultipleFields(array('gz.*', 'tr.*'));
$srch->addOrder('taxrate_name');
$page = (is_numeric($_REQUEST['page']) ? $_REQUEST['page'] : 1);
$pagesize = 30;
$srch->setPageNumber($page);
$srch->setPageSize($pagesize);
$rs_listing = $srch->getResultSet();

$pagestring = '';

$pages = $srch->pages();


    $pagestring .= createHiddenFormFromPost('frmPaging', '?', array('page', 'status', 'zone'), array('page' => '', 'status' => $_REQUEST['status'], 'zone' => $_REQUEST['zone']));
    $pagestring .= '<div class="pagination "><ul>';
	$pageStringContent='<a href="javascript:void(0);">' . t_lang('M_TXT_DISPLAYING_RECORDS') . ' ' . (($page - 1) * $pagesize + 1) .
            ' ' . t_lang('M_TXT_TO') . ' ' . (($page * $pagesize > $srch->recordCount()) ? $srch->recordCount() : ($page * $pagesize)) . ' ' . t_lang('M_TXT_OF') . ' ' . $srch->recordCount() . '</a>';
    $pagestring .= '<li><a href="javascript:void(0);">' . t_lang('M_TXT_GOTO') . ': </a></li>
		' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
                    , $srch->pages(), $page, '<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
    $pagestring .= '</div>';


$arr_listing_fields = array(
    'taxrate_name' => t_lang('M_FRM_NAME'),
    'taxrate_tax_rate' => t_lang('M_TXT_TAX_RATE'),
    'taxrate_geozone_id' => t_lang('M_FRM_ZONE'),
    'action' => t_lang('M_TXT_ACTION')
);

include 'header.php';
$arr_bread = array(
    'index.php' => '<img alt="Home" src="images/home-icon.png">',
	'deals.php'=>t_lang('M_TXT_DEALS').'/'.t_lang('M_TXT_PRODUCTS'),
    '' => t_lang('M_TXT_TAX_RATE')
);
?>

        <ul class="nav-left-ul">
            <li>    <a <?php if ($_REQUEST['status'] == 'active') echo 'class="selected"'; ?> href="tax-rate.php?status=active"><?php echo t_lang('M_TXT_ACTIVE_TAX_RATE_LISTING'); ?></a></li>
            <li>    <a <?php if ($_REQUEST['status'] == 'deactive') echo 'class="selected"'; ?> href="tax-rate.php?status=deactive"><?php echo t_lang('M_TXT_INACTIVE_TAX_RATE_LISTING'); ?></a></li>

        </ul>
    </div></td>

<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread); ?>
	
	<div class="div-inline">
		<div class="page-name"><?php echo t_lang('M_TXT_TAX_RATE'); ?> 
			<?php if (checkAdminAddEditDeletePermission(4, '', 'add')) { ?>
				<ul class="actions right">
				   <li class="droplink">
						<a href="javascript:void(0)"><i class="ion-android-more-vertical icon"></i></a>
						<div class="dropwrap">
							<ul class="linksvertical">
								<li>  
									<a href="?page=<?php echo $page; ?>&add=new" ><?php echo t_lang('M_TXT_ADD_NEW'); ?></a>
								</li>
							</ul>
						</div>
				   </li>
				</ul>
			<?php } ?>
		</div>
	</div>
	
    <div class="clear"></div>
<?php if ((isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0]))) { ?> 
        <div class="box" id="messages">
            <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES'); ?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide();
                    return false;"><?php echo t_lang('M_TXT_HIDE'); ?></a></div>
            <div class="content">
        <?php if (isset($_SESSION['errs'][0])) { ?>
                    <div class="redtext"><?php echo $msg->display(); ?> </div>
                    <br>
                    <br>
                    <?php
                }
                if (isset($_SESSION['msgs'][0])) {
                    ?>
                    <div class="greentext"> <?php echo $msg1->display(); ?> </div>
                <?php } ?>
            </div>
        </div>
            <?php } ?> 
			
            <?php
            if (is_numeric($_REQUEST['edit']) || $_REQUEST['add'] == 'new') {
                ?>
			
        <?php if ((checkAdminAddEditDeletePermission(4, '', 'add')) || (checkAdminAddEditDeletePermission(4, '', 'edit'))) { ?>
            <div class="box"><div class="title"> <?php echo t_lang('M_TXT_TAX_RATE'); ?> </div><div class="content"><?php echo $frm->getFormHtml(); ?></div></div>
            <?php
        } else {
            die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
        }
    } else {
        ?>




        <div class="box searchform_filter"><div class="title"> <?php echo t_lang('M_TXT_TAX_RATE'); ?> </div><div class="content togglewrap" style="display:none;">
		<?php echo $Src_frm->getFormHtml(); ?></div></div>
 	

                <table class="tbl_data" width="100%">
                    <thead>
                        <tr>
                <?php
                foreach ($arr_listing_fields as $val)
                    echo '<th>' . $val . '</th>';
                ?>
                        </tr>
                    </thead>
                            <?php
                            while ($row = $db->fetch($rs_listing)) {
                                echo '<tr' . (($row['zone_active'] == 0) ? ' class="inactive"' : '') . '>';
                                foreach ($arr_listing_fields as $key => $val) {
                                    echo '<td ' . (($key == action) ? 'width="20%"' : '') . '>';
                                    switch ($key) {
                                        case 'taxrate_name':
                                            echo $row['taxrate_name'] . '<br>';
                                            break;
                                        case 'taxrate_tax_rate':
                                            echo $row['taxrate_tax_rate'] . ' % <br>';
                                            break;
                                        case 'taxrate_geozone_id':
                                            echo $row['geozone_name'] . '<br>';
                                            break;
                                        case 'action':
                                            echo '<ul class="actions">';
											
											if ($_REQUEST['status'] == 'active') {
                                                if (checkAdminAddEditDeletePermission(4, '', 'edit')) {
                                                    echo '<li><a href="?edit=' . $row['taxrate_id'] . '&page=' . $page . '&status=' . $_REQUEST['status'] . '" title="' . t_lang('M_TXT_EDIT') . '"><i class="ion-edit icon"></i></a></li> ';
                                                }
                                                if (checkAdminAddEditDeletePermission(4, '', 'delete')) {
                                                    echo '<li><a href="javascript:void(0);" title="' . t_lang('M_TXT_DELETE') . '" onclick="deleteZone(' . $row['taxrate_id'] . ',' . $page . ');"><i class="ion-android-delete icon"></i></a></li>';
                                                }
                                            } else if ($_REQUEST['status'] == 'deleted') {
                                                if (checkAdminAddEditDeletePermission(4, '', 'delete')) {
                                                    echo '<li><a href="?restore=' . $row['taxrate_id'] . '&page=' . $page . '&status=' . $_REQUEST['status'] . '" title="' . t_lang('M_TXT_RESTORE') . '"><i class="ion-archive icon"></i></a></li>';

                                                    echo '<li><a href="?deletePer=' . $row['taxrate_id'] . '&page=' . $page . '&status=' . $_REQUEST['status'] . '" title="' . t_lang('M_TXT_DELETE_PERMANENTLY') . '" onclick="return(confirm(\'' . t_lang('M_MSG_REALLY_WANT_TO_DELETE_THIS_RECORD') . '\'));"><i class="ion-ios-trash icon"></i></a></li>';
                                                }
                                            } else {
                                                if (checkAdminAddEditDeletePermission(4, '', 'edit')) {
                                                    echo '<li><a href="?edit=' . $row['taxrate_id'] . '&page=' . $page . '&status=' . $_REQUEST['status'] . '" title="' . t_lang('M_TXT_EDIT') . '"><i class="ion-edit icon"></i></a></li>';
                                                }
                                                if (checkAdminAddEditDeletePermission(4, '', 'delete')) {
                                                    echo '<li><a href="?delete=' . $row['taxrate_id'] . '&page=' . $page . '&status=' . $_REQUEST['status'] . '" title="' . t_lang('M_TXT_DELETE') . '" onclick="return(confirm(\'' . t_lang('M_MSG_REALLY_WANT_TO_DELETE_THIS_RECORD') . '\'));"><i class="ion-android-delete icon"></i></a></li>';
                                                }
                                            }

											echo '</ul>';
                                            break;
                                        default:
                                            echo $row[$key];
                                            break;
                                    }
                                    echo '</td>';
                                }
                                echo '</tr>';
                            }
                            if ($db->total_records($rs_listing) == 0)
                                echo '<tr><td colspan="' . count($arr_listing_fields) . '">' . t_lang('M_TXT_NO_RECORD_FOUND') . '</td></tr>';
                            ?>
                </table>

                   
            
            <?php } ?>
			<?php  if (!isset($_GET['edit']) && $_GET['add'] != 'new' && ($srch->pages() > 1)) {  ?>
			<div class="footinfo">
					<aside class="grid_1">
						<?php echo $pagestring; ?>	 
					</aside>  
					<aside class="grid_2"><span class="info"><?php echo $pageStringContent; ?></span></aside>
			   </div>
			<?php } ?>   

</td>
            <?php
            include 'footer.php';
            ?>
