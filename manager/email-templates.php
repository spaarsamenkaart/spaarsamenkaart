<?php 
require_once 'application-top.php';
checkAdminPermission(7);
$page=(is_numeric($_GET['page'])?$_GET['page']:1);
$pagesize=20;

$mainTableName='tbl_email_templates';
$primaryKey='tpl_id';
$colPrefix='tpl_';

$frm=getMBSFormByIdentifier('frmEmailTemplate');

$fld = $frm->getField('tpl_message');
$fld->html_before_field='<div class="frm-editor">';
$fld->html_after_field='</div>';
$frm->setAction('?');
$fld = $frm->getField('btn_submit');
$fld->value = t_lang('M_TXT_SUBMIT');
updateFormLang($frm);
if(is_numeric($_GET['edit'])){
if(checkAdminAddEditDeletePermission(7,'','edit')){
    $record=new TableRecord($mainTableName);
    if(!$record->loadFromDb($primaryKey . '=' . $_GET['edit'], true)){
        $msg->addError($record->getError());
    }
    else{
        $arr=$record->getFlds();
        
        $arr_replacements=explode(',', $arr['tpl_replacements']);
        $arr['tpl_replacements']='';
        foreach ($arr_replacements as $val){
            $val=trim($val);
            if($val=='') continue;
            $arr['tpl_replacements'] .= 'xx' . $val . 'xx => ' . $val . "\r\n";
        }
        $arr['tpl_replacements']=nl2br(trim($arr['tpl_replacements']));
        /* $frm->fill($arr); */
		fillForm($frm,$arr);
        $msg->addMsg(t_lang('M_TXT_CHANGE_THE_VALUES'));
    }
	}else{
	die('Unauthorized Access.');
	}
}

if(is_numeric($_GET['id'])){
	if(checkAdminAddEditDeletePermission(7,'','edit')){
		if($_REQUEST['status'] == 'on'){
			$status = 1;
		}
		if($_REQUEST['status'] == 'off'){
			$status = 0;
		}
		if(isset($status)){
			$db->query('update tbl_email_templates set tpl_status ='.$status .' where tpl_id='.$_GET['id']);
			redirectUser('?');
		}	
    }else{
		die('Unauthorized Access.');
	}
}

if($_SERVER['REQUEST_METHOD']=='POST'){
    $post=getPostedData();
    if(!$frm->validate($post)){
        $errors=$frm->getValidationErrors();
        foreach ($errors as $error) $msg->addError($error);
    }
    else{
        $record=new TableRecord($mainTableName);
        /* $record->assignValues($post); */
		$arr_lang_independent_flds = array('tpl_id','tpl_status','tpl_replacements','mode','btn_submit');
		assignValuesToTableRecord($record,$arr_lang_independent_flds,$post);
        
        if(checkAdminAddEditDeletePermission(7,'','edit')){
        $success=($post[$primaryKey]>0)?$record->update($primaryKey . '=' . $post[$primaryKey]):$record->addNew();
		}
        if($success){
            $msg->addMsg(T_lang('M_TXT_ADD_UPDATE_SUCCESSFULL'));
            redirectUser();
        }
        else{
            $msg->addError('Could not add/update! Error: ' . $record->getError());
            $frm->fill($post);
        }
        
    }
}

$srch=new SearchBase($mainTableName);

$srch->setPageNumber($page);
$srch->setPageSize($pagesize);
$rs_listing=$srch->getResultSet();

$pagestring='';

$pages=$srch->pages();

$pagestring .= '<div class="pagination "><ul>';
$pageStringContent='<a href="javascript:void(0);">Displaying records ' . (($page - 1) * $pagesize + 1) . ' to ' . (($page * $pagesize > $srch->recordCount())?$srch->recordCount():($page * $pagesize)) . ' of ' . $srch->recordCount().'</a>';
$pagestring .= '<li><a href="javascript:void(0);">'.t_lang('M_TXT_GOTO_PAGE').'</a></li>' . getPageString('<li><a href="?page=xxpagexx">xxpagexx</a> </li> ', $srch->pages(), $page,'<li class="selected"><a href="javascript:void(0);" class="active">xxpagexx</a></li>');
$pagestring .= '</div>';


$arr_listing_fields=array(
'listserial'=>'S.N.',
'tpl_name'=>t_lang('M_FRM_NAME'),
'tpl_subject'.$_SESSION['lang_fld_prefix']=>t_lang('M_TXT_SUBJECT'),
'tpl_status'=>t_lang('M_FRM_STATUS'),
'action'=>t_lang('M_TXT_ACTION')
);

include 'header.php';
$arr_bread=array(
'index.php'=>'<img class="home" alt="Home" src="images/home-icon.png">',
'configurations.php'=>t_lang('M_TXT_SETTINGS'),
''=>t_lang('M_TXT_EMAIL_TEMPLATES')
);

?>
<ul class="nav-left-ul">
				 <li><a href="configurations.php" ><?php echo t_lang('M_TXT_GENERAL_SETTINGS');?></a></li>
						<li><a href="payment-settings.php"><?php echo t_lang('M_TXT_PAYMENT_GATEWAY_SETTINGS');?></a></li>
						<li><a href="email-templates.php" class="selected"><?php echo t_lang('M_TXT_EMAIL_TEMPLATES');?></a></li>
						 
						<li><a href="language-managment.php"><?php echo t_lang('M_TXT_LANGUAGE_MANAGEMENT');?></a></li>
                                                <li><a href="cities.php" ><?php echo t_lang('M_TXT_CITIES_MANAGEMENT');?></a></li>
						<li><a href="database-backup.php" ><?php echo t_lang('M_TXT_DATABASE_BACKUP_RESTORE');?></a></li>
					</ul>
                </div></td>
        <td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
                
				<div class="div-inline">
					<div class="page-name"><?php echo t_lang('M_TXT_EMAIL_TEMPLATES');?></div>
				</div>
				
                <div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="message error"><?php echo $msg1->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?>	

				<?php  
				if(is_numeric($_REQUEST['edit'])){
			 
					if(checkAdminAddEditDeletePermission(7,'','edit')){?>
						<div class="box"><div class="title"> <?php echo t_lang('M_TXT_EMAIL_TEMPLATES');?> </div><div class="content"><?php echo  $frm->getFormHtml();?></div></div>
					<?php }else{
						die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
					}
				}else{
				?>
	
<table class="tbl_data" width="100%">
<thead>
<tr>
<?php 
foreach ($arr_listing_fields as $val) echo '<th>' . $val . '</th>';
?>
</tr>
</thead>
<?php 
for($listserial=($page-1)*$pagesize+1; $row=$db->fetch($rs_listing); $listserial++){
    echo '<tr' . (($row[$colPrefix . 'active']=='0')?' class="inactive"':'') . '>';
    foreach ($arr_listing_fields as $key=>$val){
        echo '<td>';
        switch ($key){
            case 'listserial':
                echo $listserial;
                break;
            case 'tpl_status':
					echo $row[$key]=='1' ? '<span class="label label-primary">'.t_lang('M_TXT_ACTIVE').'</span>' : '<span class="label label-danger">'.t_lang('M_TXT_INACTIVE').'</span>';
                break;
			case 'tpl_subject_lang1':
					echo '<strong>'.$arr_lang_name[0].'</strong>'. ' ' .$row['tpl_subject'].'<br>';
					echo '<strong>'.$arr_lang_name[1].'</strong>'. ' ' .$row['tpl_subject_lang1'];
                break;
            case 'action':
				echo '<ul class="actions">';
				
				if(checkAdminAddEditDeletePermission(7,'','edit')){
					echo '<li><a href="?edit=' . $row[$primaryKey] . '&page=' . $page . '" title="'.t_lang('M_TXT_EDIT').'"><i class="ion-edit icon"></i></a></li>
					<li><a rel="facebox" href=email-template-preview.php?tpl_id=' . $row[$primaryKey] . ' title="'. t_lang('M_TXT_PREVIEW') .'"><i class="ion-eye icon"></i></a></li>';
				}
				if($row['tpl_status'] == 1){
					echo '<li><a title="'. t_lang('M_TXT_SEND_MAIL_OFF') .'"  href="?id=' . $row[$primaryKey] . '&status=off"><i class="ion-android-checkbox-blank icon"></i></a></li>';
				}
				
				if($row['tpl_status'] == 0){
					echo '<li><a title="'. t_lang('M_TXT_SEND_MAIL_ON') .'"  href="?id=' . $row[$primaryKey] . '&status=on"><i class="ion-android-checkbox icon"></i></a></li>';
				}
                
				echo '</ul>';				
				break;
            default:
                echo $row[$key];
                break;
        }
        echo '</td>';
    }
    echo '</tr>';
}
if($db->total_records($rs_listing)==0) echo '<tr><td colspan="' . count($arr_listing_fields) . '">'.t_lang('M_TXT_NO_RECORD_FOUND').'</td></tr>';
?>
</table>

	<?php if($srch->pages() > 1) {?>
		<div class="footinfo">
			<aside class="grid_1">
				<?php echo $pagestring; ?>	 
			</aside>  
			<aside class="grid_2"><span class="info"><?php echo $pageStringContent; ?></span></aside>
		</div>
	<?php 
	} 
} ?>
</td>
<?php 
include 'footer.php';
?>
