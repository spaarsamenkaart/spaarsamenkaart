<?php       
require_once 'application-top.php';
require_once '../includes/navigation-functions.php';
checkAdminPermission(15);
$srch=new SearchBase('tbl_referral_history', 'rh');
$srch->addFld('rh.*');
$rs_listing=$srch->getResultSet();
$pagestring='';
$pages=$srch->pages();
$arr_listing_fields=array(
'listserial'=>t_lang('M_TXT_S_N'),
'rh_amount'=>t_lang('M_TXT_AMOUNT'),
'rh_credited_to'=>t_lang('M_TXT_CREDITTED_TO'),
'rh_referral_user_id'=>t_lang('M_TXT_REFERRED_USER'), 
'rh_transaction_date'=>t_lang('M_TXT_TRANSACTION_DATE')
);

include 'header.php';
$arr_bread=array(
'index.php'=>'<img class="home" alt="Home" src="images/home-icon.png">',
'javascript:void(0);'=>t_lang('M_TXT_REPORTS'),
''=>t_lang('M_TXT_REFERRAL_COMMISSION_TRANSACTION')
);
  
?>

<ul class="nav-left-ul">
				   <li>    <a href="referral-history.php" class="selected"><?php echo t_lang('M_TXT_REFERRAL_COMMISSION_TRANSACTION');?></a></li>
				   
				   
					</ul>
                </div></td>
				
				<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
                
				<div class="div-inline">
					<div class="page-name"><?php echo t_lang('M_TXT_REFERRAL_HISTORY');?> </div>
				</div>


			   <div class="clear"></div>
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				 <?php  
				if($_GET['mode']=='pay'){
					if(checkAdminAddEditDeletePermission(8,'','add')){?>
				 
						<div class="box"><div class="title"> <?php echo t_lang('M_TXT_PAY_TO_AFFILIATE');?> </div><div class="content"><?php echo  $frm->getFormHtml();?></div></div>
					<?php  
					}
				}else{
				?>

<table class="tbl_data" width="100%">
<thead>
<tr>
<tr>
  <?php
		foreach ($arr_listing_fields as $key=>$val) echo '<th' . (($key=='rh_amount' || $key=='rh_transaction_date')?'  width="15%"':'')  . '>' . $val . '</th>';
		?>
		
   
  </tr>
  <?php
	 
 
	$arr = array_reverse($arr);
	 
	$listserial=($page-1)*$pagesize+1;
	$balanceNew = 0;
	while($row=$db->fetch($rs_listing)){
		echo '<tr>';
		foreach ($arr_listing_fields as $key=>$val){
		 
			
	        echo '<td '. (($key=='added' || $key=='used')?' ':'') .'>';
	        switch ($key){
	            case 'listserial':
	                echo $listserial;
	                break;
	            case 'rh_transaction_date':
	                echo displayDate($row[$key], true , true, '');
	                break;
					
				case 'rh_credited_to':
	                $CreditUser = $db->query("select * from tbl_users where user_id=".intval($row['rh_credited_to']));
					$rowCredit = $db->fetch($CreditUser);
					if($db->total_records($CreditUser)>0){
					echo $rowCredit['user_name'].'<br>'.$rowCredit['user_email']; 
					}
	                break;
				case 'rh_referral_user_id':
	                $referralUser = $db->query("select * from tbl_users where user_id=".intval($row['rh_referral_user_id']));
					$rowRefer = $db->fetch($referralUser);
					if($db->total_records($referralUser)>0){
					echo '<strong>'.t_lang('M_TXT_USER_NAME').': </strong>'.$rowRefer['user_name'].'<br><strong>'.t_lang('M_TXT_EMAIL_ADDRESS').': </strong>'.$rowRefer['user_email'].'<br><strong>'.t_lang('M_TXT_REGISTRATION_DATE').': </strong>'.displayDate($rowRefer['user_regdate'], true , true, ''); 
					}
	                break;
				case 'rh_amount':
					echo CONF_CURRENCY . number_format(($row['rh_amount']), 2) . CONF_CURRENCY_RIGHT;	
					break;
				 
	            default:
	                echo $row[$key];
	                break;
	        }
	        echo '</td>';
	    }
		 
	    echo '</tr>';
		$listserial++;
	}
	if($db->total_records($rs_listing)==0) { 
		echo '<tr><td colspan="' . count($arr_listing_fields) . '" >'.t_lang('M_TXT_NO_RECORD_FOUND').'</td></tr>';
	}
	?>
</table>

<?php } ?>

 </td>
<?php 
include 'footer.php';
?>
