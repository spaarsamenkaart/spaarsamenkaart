<?php     
require_once 'application-top.php';
require_once '../includes/navigation-functions.php';
checkAdminPermission(8);
$pagesize=30;
$page=(is_numeric($_REQUEST['page'])?$_REQUEST['page']:1);
$_REQUEST['status']=(isset($_REQUEST['status'])?$_REQUEST['status']:'approved');
$post=getPostedData();

$Src_frm=new Form('Src_frm', 'Src_frm');
$Src_frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
$Src_frm->setFieldsPerRow(4);
$Src_frm->captionInSameCell(true);
$Src_frm->addTextBox(t_lang('M_FRM_KEYWORD'), 'keyword', '', '','');
$Src_frm->addSelectBox(t_lang('M_FRM_STATUS'), 'rep_status',  $arr_user_status,'', 'style="width: 160px;"', '--Select--','');
$Src_frm->addSelectBox(t_lang('M_FRM_SALES'), 'sales_earning',  $arr_sale_earning,'', 'style="width: 160px;"', '--Select--','');
$Src_frm->addHiddenField('','mode','search');
$Src_frm->addHiddenField('','status',$_REQUEST['status']);
$fld1=$Src_frm->addButton('&nbsp;', 'btn_cancel', t_lang('M_TXT_CLEAR_SEARCH'), '', ' class="medium" onclick=location.href="representative.php"');
$fld=$Src_frm->addSubmitButton('&nbsp;', 'search', t_lang('M_TXT_SEARCH'), '', ' class="medium"')->attachField($fld1);

if(isset($_GET['delete']) && $_GET['delete']!=""){
	$rs=$db->query("select rep_id from tbl_representative where rep_id=" . $_GET['delete']);
	$rowAffiliate=$db->fetch($rs);
	$rs1=$db->query("select rwh_rep_id from tbl_representative_wallet_history where rwh_rep_id=" . $_GET['delete']);
	$rowAffiliate1=$db->fetch($rs);
	if( ($rowAffiliate['rep_id'] == $_GET['delete'])) {
		if(checkAdminAddEditDeletePermission(8,'','delete')){
			$rep_id = $_GET['delete'];
			$db->query("delete from tbl_representative  WHERE rep_id =$rep_id ");
			$db->query("delete from tbl_representative_wallet_history  WHERE rwh_rep_id =$rep_id ");
			$msg->addMsg(t_lang("M_TXT_RECORD_DELETED"));
			redirectUser('?page=' . $page);
		}else{
			die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
		}
	}else{
			die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
	}
}

/* $frm=getMBSFormByIdentifier('frmAffiliate');
$frm->setAction('?page=' . $page);

updateFormLang($frm); 
$fld = $frm->getField('submit');
$fld->value=t_lang('M_TXT_SUBMIT');
$fld->field_caption='&nbsp;'; */
$rscountry=$db->query("select country_id, country_name from tbl_countries where country_status='A' order by country_name");
	$countryArray=array();
	while($arrs=$db->fetch($rscountry)){

		$countryArray[$arrs['country_id']]= $arrs['country_name'];
	}
	

	
$frm= new Form('frmRepresentative','frmRepresentative');
$frm->setTableProperties(' border="0" cellspacing="0" cellpadding="0" class="tbl_form" width="100%"');
$frm->setFieldsPerRow(1);
$frm->captionInSameCell(false);
$frm->setJsErrorDisplay('afterfield');
$frm->setValidatorJsObjectName('frmValidator');
  
$frm->addRequiredField('M_FRM_FIRST_NAME','rep_fname','','rep_fname',''); 
$frm->addTextBox('M_FRM_LAST_NAME','rep_lname','','rep_lname','');
$frm->addTextBox('M_FRM_BUSINESS_NAME','rep_bussiness_name','','rep_bussiness_name','');
$frm->addRequiredField('M_FRM_ADDRESS_LINE1','rep_address_line1','','rep_address_line1',''); 
$frm->addTextBox('M_FRM_ADDRESS_LINE2','rep_address_line2','','rep_address_line2','');
$frm->addTextBox('M_FRM_ADDRESS_LINE3','rep_address_line3','','rep_address_line3','');


$frm->addSelectBox( 'M_FRM_COUNTRY', 'rep_country', $countryArray,'','onchange="updateStates(this.value);" class="medium"','', 'rep_country');
$fld = $frm->addHtml( 'M_FRM_STATE', 'state', '<span id="spn-state"></span>',false);

$frm->addTextBox('M_FRM_CITY','rep_city','','rep_city','');
$frm->addTextBox('M_FRM_COMMISSION','rep_commission','','rep_commission','');
$frm->addTextBox('M_FRM_ZIP_CODE','rep_zipcode','','rep_zipcode','');
$fld = $frm->addEmailField('M_FRM_EMAIL_ADDRESS','rep_email_address','','rep_email_address','');
$fld->setUnique( 'tbl_representative', 'rep_email_address', 'rep_id', 'rep_id', 'rep_id');
$fld->Requirements()->setRequired();
$frm->addPasswordField('M_FRM_PASSWORD','rep_password','','rep_password','')->Requirements()->setRequired();
$frm->addTextBox('M_FRM_PHONE_NO','rep_phone','','rep_phone','');
$frm->addTextBox('M_FRM_PAYPAL_ID','rep_paypal_id','','rep_paypal_id','');
$frm->addHiddenField('','rep_id','','rep_id',''); 
 

$fld=$frm->addSubmitButton('', 'btn_submit', t_lang('M_TXT_SUBMIT'), '', ' class="medium"');
$selected_state=0;
updateFormLang($frm);
 	

if(is_numeric($_GET['edit'])){


$fld = $frm->getField('rep_password');
$frm->removeField($fld);

 


$frm->addHiddenField('','rep_password','');



	if(checkAdminAddEditDeletePermission(8,'','edit')){
		$record=new TableRecord('tbl_representative');
		
		if(!$record->loadFromDb('rep_id='.$_GET['edit'], true)){
			$msg->addError($record->getError());
		}
		else{
			$arr=$record->getFlds();
			
			$rs=$db->query("select state_country from tbl_states where state_id=" . $arr['rep_state']);
			$row=$db->fetch($rs);
			$arr['rep_country']=$row['state_country'];
			$arr['btn_submit']=t_lang('M_TXT_UPDATE');
			$selected_state=$arr['rep_state'];
			fillForm($frm,$arr);
		    /*  $frm->fill($arr); */
			$msg->addMsg(t_lang('M_TXT_CHANGE_THE_VALUES'));
			
			}
	}else{
		die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
	}
}
 
if(isset($_POST['btn_submit'])){
    $post=getPostedData();
    if(!$frm->validate($post)){
        $errors=$frm->getValidationErrors();
        foreach ($errors as $error) $msg->addError($error);
    }
    else{
        $record=new TableRecord('tbl_representative');
        /* $record->assignValues($post); */
		$arr_lang_independent_flds = array('rep_id','rep_city','rep_state','rep_country','rep_zipcode','rep_email_address','rep_password','rep_phone','rep_code','rep_status','rep_payment_mode','mode','rep_paypal_id','btn_submit');
		assignValuesToTableRecord($record,$arr_lang_independent_flds,$post);
		
		
		if((checkAdminAddEditDeletePermission(8,'','edit')) ){
			
			if($post['rep_id']>0) $success = $record->update('rep_id' . '=' . $post['rep_id']); 
	    
		}
		if((checkAdminAddEditDeletePermission(8,'','add')) ){
			$code = mt_rand(0,9999999999);
			$record->setFldValue('rep_code',$code);
			
			if($post['rep_password']==""){
				$record->setFldValue('rep_password',md5($code));	
			}else{
				$record->setFldValue('rep_password',md5($post['rep_password']));
				$code = $post['rep_password'];
			}
			/* $record->setFldValue('rep_status',1); */
			if($post['rep_id']=='') $success=$record->addNew();
		} 
		 $rep_id=($post['rep_id']>0)?$post['rep_id']:$record->getId();
		 
			
		#$success=($post['rep_id']>0)?$record->update('rep_id' . '=' . $post['rep_id']):$record->addNew();
        if($success){
			if($post['rep_id']=="")	{
				########## Email #####################
				/* $headers  = 'MIME-Version: 1.0' . "\r\n";
				$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

				$headers .= 'From: ' . CONF_SITE_NAME . ' <' . CONF_EMAILS_FROM . '>' . "\r\n"; */
				$rs=$db->query("select * from tbl_email_templates where tpl_id=38");
				$row_tpl=$db->fetch($rs);
				
				$message=$row_tpl['tpl_message'.$_SESSION['lang_fld_prefix']];
				$subject=$row_tpl['tpl_subject'.$_SESSION['lang_fld_prefix']];
				$arr_replacements=array(
				'xxcompany_namexx' => $post['rep_fname'].' '.$post['rep_lname'],                   
				'xxuser_namexx' => $post['rep_email_address'], 
				'xxemail_addressxx' => $post['rep_email_address'],                    
				'xxpasswordxx' => $code,
				'xxloginurlxx' => 'http://'.$_SERVER['SERVER_NAME'].CONF_WEBROOT_URL.'representative/login.php',
				'xxsite_namexx' => CONF_SITE_NAME,
				'xxserver_namexx'=>$_SERVER['SERVER_NAME'],
				'xxwebrooturlxx'=>CONF_WEBROOT_URL,
				'xxsite_urlxx'=>'http://'.$_SERVER['SERVER_NAME'].CONF_WEBROOT_URL
				);
				
				foreach ($arr_replacements as $key=>$val){
					$subject=str_replace($key, $val, $subject);
					$message=str_replace($key, $val, $message);
				} 
				
				if($row_tpl['tpl_status'] == 1){ 
					sendMail($post['rep_email_address'], $subject, emailTemplateSuccess($message), $headers);
				}
				   ##############################################
				   
			}
			
            $msg->addMsg(T_lang('M_TXT_ADD_UPDATE_SUCCESSFULL'));
            redirectUser('?');
        }
        else{
            $msg->addError(t_lang('M_TXT_COULD_NOT_ADD_UPDATE') . $record->getError());
            $frm->fill($post);
        }
    }
}


	$srch=new SearchBase('tbl_representative', 'a');
	/* if($_REQUEST['status']=='approved'){
		$srch->addCondition('rep_status', '=', 1);
	}else if($_REQUEST['status']=='un-approved'){
		$srch->addCondition('rep_status', '=', 0);
	}else{
		$srch->addCondition('rep_status', '=', 1);
	} */ 
	
	if($post['mode']=='search')
	{

	 
		
		if($post['keyword'] != '') 	
		{
			
			$cnd=$srch->addDirectCondition('0');
			$cnd->attachCondition('a.rep_fname', 'like','%'. $post['keyword'].'%' ,'OR');
			$cnd->attachCondition('a.rep_lname', 'like','%'. $post['keyword'].'%' ,'OR');
			$cnd->attachCondition('a.rep_bussiness_name', 'like','%'. $post['keyword'].'%' ,'OR');
			$cnd->attachCondition('a.rep_address_line1', 'like','%'. $post['keyword'].'%' ,'OR');
			$cnd->attachCondition('a.rep_address_line2', 'like','%'. $post['keyword'].'%' ,'OR');
			$cnd->attachCondition('a.rep_address_line3', 'like','%'. $post['keyword'].'%' ,'OR');
			$cnd->attachCondition('a.rep_email_address', 'like','%'. $post['keyword'].'%' ,'OR');
			 
			 
		}
		if($post['rep_status'] != ''){
		
		$cnd=$srch->addDirectCondition('0');
		$cnd->attachCondition('rep_status', '=', $post['rep_status'],'OR'); 
		
		}
		if($post['sales_earning'] != ''){
			
			
		$srch1 = new SearchBase('tbl_companies', 'c');
		$srch1->joinTable('tbl_deals', 'INNER JOIN', 'c.company_id=d.deal_company', 'd');
		$srch1->addMultipleFields(array('c.company_rep_id'));
		$srch1->joinTable('tbl_order_deals', 'INNER JOIN', 'od.od_deal_id=d.deal_id ', 'od');
		$srch1->joinTable('tbl_orders', 'INNER JOIN', 'o.order_id=od.od_order_id ', 'o');
		$srch1->addCondition('o.order_payment_status', '!=', 0); 
		$srch1->addCondition('c.company_rep_id', '!=', 0); 
		$srch1->addMultipleFields(array("SUM((od.od_qty+od.od_gift_qty)*od.od_deal_price) as totalAmount")); 
		$srch1->addGroupBy('c.company_rep_id');
		
		$sales_earning=$post['sales_earning'];
		$company_rep_id=array(0);
		switch($sales_earning){
		case 1:
		/*$free_rep who don not belong to any company*/
		$co_rep_ids= fetchCompanyRepIds();
		$total_rep_ids=fetchTotalRepIds();
		$free_rep= array_diff($total_rep_ids,$co_rep_ids);
		$company_rep_id=$free_rep;
		$srch1->addHaving('totalAmount', '<=',1000,'AND',true); 
		break;
		case 5:
		$srch1->addHaving('totalAmount', '>',1000,'AND',true); 
		$srch1->addHaving('totalAmount', '<=',5000,'AND',true); 
		break;
		case 25:
		$srch1->addHaving('totalAmount', '>',5000,'AND',true); 
		$srch1->addHaving('totalAmount', '<=',25000,'AND',true); 
		break;
		case 50:
		$srch1->addHaving('totalAmount', '>',25000,'AND',true); 
		$srch1->addHaving('totalAmount', '<=',50000,'AND',true); 
		break;
		case 100:
		$srch1->addHaving('totalAmount', '>',50000,'AND',true); 
		$srch1->addHaving('totalAmount', '<=',100000,'AND',true); 
		break;
		case 1000:
		$srch1->addHaving('totalAmount', '>',100000,'AND',true); 
		//$srch1->addHaving('totalAmount', '<=',50000,'AND',true); 
		break;
		}
	//echo $srch1->getQuery();
			$rep_data = $srch1->getResultSet();
			$row1 = $db->fetch_all($rep_data);
			foreach($row1 as $key=> $val){
			$company_rep_id[] =$val['company_rep_id'];
			}
		$cnd=$srch->addDirectCondition('0');
		$cnd->attachCondition('rep_id', 'IN', $company_rep_id,'OR'); 
		
		}
	//	echo $srch1->getQuery();
		$Src_frm->fill($post);
		
	}
 
/* $srch->addOrder('rep_fname'.$_SESSION['lang_fld_prefix']); */
 

$srch->setPageNumber($page);
$srch->setPageSize($pagesize);
$rs_listing=$srch->getResultSet();

$pagestring='';

$pages=$srch->pages();

	$pagestring .= createHiddenFormFromPost('frmPaging', '?', array('page','status','keyword'), array('page'=>'','status'=>$_REQUEST['status'],'keyword'=>$_REQUEST['keyword']));
	
	$pagestring .= '<div class="pagination "><ul>';
	$pageStringContent='<a href="javascript:void(0);">' . t_lang('M_TXT_DISPLAYING_RECORDS') . ' ' . (($page - 1) * $pagesize + 1) .
            ' ' . t_lang('M_TXT_TO') . ' ' . (($page * $pagesize > $srch->recordCount()) ? $srch->recordCount() : ($page * $pagesize)) . ' ' . t_lang('M_TXT_OF') . ' ' . $srch->recordCount() . '</a>';
    $pagestring .= '<li><a href="javascript:void(0);">' . t_lang('M_TXT_GOTO') . ': </a></li>
		' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
                    , $srch->pages(), $page, '<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
    $pagestring .= '</div>';
	
	/* $pagestring .= '<div class="pagination fr"><ul><li><a href="javascript:void(0);">'.t_lang('M_TXT_DISPLAYING_RECORDS').' ' .  (($page - 1) * $pagesize + 1) . 
	' '.t_lang('M_TXT_TO').' ' . (($page * $pagesize > $srch->recordCount())?$srch->recordCount():($page * $pagesize)) . ' '.t_lang('M_TXT_OF').' ' . $srch->recordCount().'</a></li>';
	$pagestring .= '<li><a href="javascript:void(0);">'.t_lang('M_TXT_GOTO').': </a></li>
	' . getPageString('<li><a href="javascript:void(0);" onclick="setPage(xxpagexx,document.frmPaging);">xxpagexx</a> </li> '
	, $srch->pages(), $page,'<li class="selected"><a class="active" href="javascript:void(0);">xxpagexx</a></li>');
	$pagestring .= '</div>'; */

$arr_listing_fields=array(
'listserial'=>'S.N.',
'rep_fname'.$_SESSION['lang_fld_prefix']=>t_lang('M_FRM_FIRST_NAME'),
'rep_lname'.$_SESSION['lang_fld_prefix']=>t_lang('M_FRM_LAST_NAME'),
'rep_bussiness_name'.$_SESSION['lang_fld_prefix']=>t_lang('M_FRM_BUSINESS_NAME'),
'rep_address_line1'.$_SESSION['lang_fld_prefix']=>t_lang('M_TXT_ADDRESS'),
'rep_email_address'.$_SESSION['lang_fld_prefix']=>t_lang('M_FRM_EMAIL_ADDRESS'),

'total_signup'=>t_lang('M_TXT_TOTAL_MERCHANT_SIGNUPS'),
/* 'total_newsletter_signup'=>t_lang('M_TXT_NEWSLETTER_SIGN_UP'), */
'total_sale'=>t_lang('M_TXT_TOTAL_SALES'),
'rep_status'=>t_lang('M_FRM_STATUS'),
'action'=>t_lang('M_TXT_ACTION')
);


include 'header.php';
 
echo '<script language="javascript">
	selectCountryFirst="' . addslashes(t_lang('M_TXT_SELECT_COUNTRY_FIRST')).'"
	</script>';
	
$arr_bread=array(
'index.php'=>'<img class="home" alt="Home" src="images/home-icon.png">',
'javascript:void(0);'=>t_lang('M_TXT_USERS'),
''=>t_lang('M_TXT_REPRESENTATIVE')
);


echo '<script language="javascript">
selectedState=' . $selected_state . '
</script>';

 
?>
</div></td>
 			<td class="right-portion"><?php echo getAdminBreadCrumb($arr_bread);?>
                
                <div class="div-inline">
					<div class="page-name"><?php echo t_lang('M_TXT_REPRESENTATIVE'); ?> 
						<?php if(checkAdminAddEditDeletePermission(8,'','add')){?> 
							<ul class="actions right">
							   <li class="droplink">
									<a href="javascript:void(0)"><i class="ion-android-more-vertical icon"></i></a>
									<div class="dropwrap">
										<ul class="linksvertical">
											<li> 
											<a href="?page=<?php echo $page; ?>&add=new"><?php echo t_lang('M_TXT_ADD_NEW');?></a>
										 </li>
										</ul>
									</div>
								</li>
							</ul>
						<?php } ?>
					</div>
				</div>
				
				<div class="clear"></div>
				
				<?php if( (isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0])) ){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				 <?php } ?> 
				
				<?php  
				if(is_numeric($_REQUEST['edit']) || $_REQUEST['add']=='new'){
				 ?>
					<script type="text/javascript">
					$(document).ready(function(){ updateStates(document.frmRepresentative.rep_country.value);});</script>
					<?php  

					if((checkAdminAddEditDeletePermission(8,'','add')) ||(checkAdminAddEditDeletePermission(8,'','edit')) ){?>
						<div class="box"><div class="title"> <?php echo t_lang('M_TXT_REPRESENTATIVE');?> </div><div class="content"><?php echo  $frm->getFormHtml();?></div></div>
					<?php }else{
						die(t_lang('M_TXT_UNAUTHORIZED_ACCESS'));
					}
				}else{
				?>
	
				 
				 
 
<div class="box searchform_filter"><div class="title"> <?php echo t_lang('M_TXT_REPRESENTATIVE');?> </div><div class="content togglewrap" style="display:none;">		<?php echo $Src_frm->getFormHtml();?>
</div></div>	
 
<table class="tbl_data" width="100%">
<thead>
<tr>
<?php 
foreach ($arr_listing_fields as $val) echo '<th>' . $val . '</th>';
?>
</tr>
</thead>
<?php 
$balance = 0;
for($listserial=($page-1)*$pagesize+1; $row=$db->fetch($rs_listing); $listserial++){
	 
	$rep_id = $row['rep_id'];
    echo '<tr' . (($row[$colPrefix . 'approved']=='0')?' class="inactive"':'') . ' id = '.$row['cat_id'].'>';
    foreach ($arr_listing_fields as $key=>$val){
        echo '<td>';
        switch ($key){
			case 'listserial':
                echo $listserial;
                break;
			case 'rep_fname'.$_SESSION['lang_fld_prefix']:
				echo '<strong>'.$arr_lang_name[0].'</strong>'. ' ' .$row['rep_fname'].'<br>';
				echo '<strong>'.$arr_lang_name[1].'</strong>'. ' ' .$row['rep_fname_lang1'];
				break;
			case 'rep_address_line1'.$_SESSION['lang_fld_prefix']:
                echo $row['rep_address_line1'.$_SESSION['lang_fld_prefix']].' <br>'.$row['rep_address_line2'.$_SESSION['lang_fld_prefix']].' <br>'.$row['rep_address_line3'.$_SESSION['lang_fld_prefix']].'<br>'.$row['rep_city'];
                break;
			
			case 'total_signup':
                /** get number of signups **/
				$srch = new SearchBase('tbl_companies', 'c');
				$srch->addCondition('company_deleted', '=', 0);
				$srch->addCondition('c.company_rep_id', '=', $row['rep_id']);
				$srch->addMultipleFields(array( 'COUNT(*) as total'));
				//echo $srch->getQuery();
				$registration_data = $srch->getResultSet();

				$row1 = $db->fetch($registration_data);
				if($row1['total'] > 0){
					echo '<a href="companies.php?rep='.$row['rep_id'].'" title="'.t_lang('M_TXT_VIEW_MERCHANTS').'">' . $row1['total'].'</a>';
				}	
				
				
                break;
			/* case 'total_newsletter_signup':
                
				$srch = new SearchBase('tbl_newsletter_subscription', 'ns');
				
				$srch->addCondition('ns.subs_rep_id', '=', $rep_id, 'OR');
				
				$srch->addMultipleFields(array( 'COUNT(*) as total'));
				//echo $srch->getQuery();
				$newsletter_data = $srch->getResultSet();

				$row1 = $db->fetch($newsletter_data);
				if($row1['total'] > 0){
					echo '<a href="newsletter-subscribers.php?affiliate='.$row['rep_id'].'">' . $row1['total'].'</a>';
				}	
	
 
				
				
                break; */
			
            case 'total_sale':
                 
				$srch = new SearchBase('tbl_companies', 'c');
				$srch->addCondition('c.company_rep_id', '=', $row['rep_id'], 'OR');
				$srch->joinTable('tbl_deals', 'INNER JOIN', 'c.company_id=d.deal_company', 'd');
				$srch->addMultipleFields(array('c.company_id','d.deal_id'));
				$wallet_data = $srch->getResultSet();
				$company = array();
		 
				$deal = array();
				while ( $row1 = $db->fetch($wallet_data) ) {
					$company[] =$row1['company_id'];
					$deal[] =$row1['deal_id'];
				}
				
				
				if($db->total_records($wallet_data)>0){ 
					$srch = new SearchBase('tbl_orders', 'o');
					$srch->joinTable('tbl_order_deals', 'INNER JOIN', 'o.order_id=od.od_order_id ', 'od');
					$srch->joinTable('tbl_deals', 'INNER JOIN', 'od.od_deal_id=d.deal_id ', 'd');
					/* $srch->addCondition('d.deal_tipped_at', '!=','0000-00-00 00:00:00'); */
					$srch->addCondition('o.order_payment_status', '!=', 0); 
					$srch->addCondition('od.od_deal_id', 'IN', $deal);  
					$srch->addMultipleFields(array('od.*','o.*', "SUM((od.od_qty+od.od_gift_qty)*od.od_deal_price) as totalAmount")); 
					
					$data = $srch->getResultSet();
					$amountRow = $db->fetch($data); 
					/* while($row1=$db->fetch($data)){
					$rs1=$db->query("select * from tbl_representative where  rep_status =1 AND  rep_id=".$row['rep_id']);
					$row_name=$db->fetch($rs1);
								
								$arr = array(
									'rwh_rep_id'=>$row['rep_id'],
									'rwh_untipped_deal_id'=>$row1['deal_id'],
									'rwh_particulars'=>'Representative Commission For: '.$row['rep_fname'].' '.$row['rep_lname'],
									'rwh_amount'=>$row1['od_deal_price']*($row1['od_qty']+$row1['od_gift_qty'])*$row['rep_commission']/100,
									'rwh_time'=>$row1['order_date'],
									'rwh_trans_type'=>'A',
									'rwh_buyer_id'=>$row1['order_user_id']
									);
								$db->insert_from_array('tbl_representative_wallet_history', $arr, true);
					} */
					if($db->total_records($data)>0){ 
						echo '<a href="rep_list.php?uid=' . $rep_id .'" title="' . t_lang('M_TXT_CLICK_TO_VIEW_SALES_DATA') . '">' . CONF_CURRENCY . number_format($amountRow['totalAmount'], 2) . CONF_CURRENCY_RIGHT . '  </a>
						
						<br><ul class="actions"><li><a href="javascript:void(0);" onClick="return payNow(' . $rep_id .');" title="' . t_lang('M_TXT_ADD_TRANSACTION') . '"> <i class="ion-social-usd icon"></i> </a></li></ul>';	 
					}
				} 
                break;
			case 'rep_status':
					if($row['rep_status']==1){
						if(checkAdminAddEditDeletePermission(3,'','edit')){
							echo '<span class="statustab" id="comment-status'.$row['rep_id'].'" onclick="activeRepresentative('.$row['rep_id'].',0);">
								<span class="switch-labels" data-off="Active" data-on="Inactive"></span>
								<span class="switch-handles"></span>
							</span>';
						}
					}
					
					if($row['rep_status']==0){
						if(checkAdminAddEditDeletePermission(3,'','edit')){
							echo '<span class="statustab active" id="comment-status' . $row['rep_id'] . '" onclick="activeRepresentative('.$row['rep_id'].',1);">
								<span class="switch-labels" data-off="Active" data-on="Inactive"></span>
								<span class="switch-handles"></span>
							</span>';
						}	
					}
				break;
            case 'action':
				 
				echo '<ul class="actions">';
					
					$deleteRow = '<li><a href="?status='.$_REQUEST['status'].'&delete=' . $rep_id . '&page=' . $page . '" title="' . t_lang('M_TXT_DELETE') . '" onclick="return(confirm(\'' . t_lang('M_MSG_REALLY_WANT_TO_DELETE_THIS_RECORD') . '\'));"><i class="ion-android-delete icon"></i></a></li>';
				 
					if($_REQUEST['status']=='approved'){
						
						
					
					   if(checkAdminAddEditDeletePermission(8,'','edit')){
						echo '<li><a href="?status='.$_REQUEST['status'].'&edit=' . $rep_id . '&page=' . $page . '" title="' . t_lang('M_TXT_EDIT') . '"><i class="ion-edit icon"></i></a></li>';
						echo '<li><a href="rep-history.php?rep_id=' . $rep_id .'" title="' . t_lang('M_TXT_TRANSACTION_HISTORY') . '"><i class="ion-alert icon"></i></a></li>';
						
						}
						if(checkAdminAddEditDeletePermission(8,'','delete')){
						echo $deleteRow;
						}
						
					}else if($_REQUEST['status']=='un-approved'){
						 
						if(checkAdminAddEditDeletePermission(8,'','edit')){
						echo '<li><a href="?status='.$_REQUEST['status'].'&edit=' . $rep_id . '&page=' . $page . '" title="' . t_lang('M_TXT_EDIT') . '"><i class="ion-edit icon"></i></a></li>';
						}
						if(checkAdminAddEditDeletePermission(8,'','delete')){
						echo $deleteRow;
						}
					
					}else{
					if(checkAdminAddEditDeletePermission(8,'','edit')){
					echo '<li><a href="?status='.$_REQUEST['status'].'&edit=' . $rep_id . '&page=' . $page . '" title="' . t_lang('M_TXT_EDIT') . '"><i class="ion-edit icon"></i></a></li>';
					/* echo '&nbsp;<a href="rep_summary.php?uid=' . $rep_id .'">Commission Earnings</a>';  */
					}
					if(checkAdminAddEditDeletePermission(8,'','delete')){
						echo $deleteRow;
					}
					}
				echo '</ul>';
					
                break;
            default:
                echo $row[$key];
                break;
        }
        echo '</td>';
    }
    echo '</tr>';
}
if($db->total_records($rs_listing)==0) echo '<tr><td colspan="' . count($arr_listing_fields) . '">' . t_lang('M_TXT_NO_RECORD_FOUND') . '</td></tr>';
?>
</table>

	<?php  if ($srch->pages() > 1) {  ?>
			<div class="footinfo">
					<aside class="grid_1">
						<?php echo $pagestring; ?>	 
					</aside>  
					<aside class="grid_2"><span class="info"><?php echo $pageStringContent; ?></span></aside>
			   </div>
		<?php }
	} ?>

 </td>
<?php 
include 'footer.php';
?>
