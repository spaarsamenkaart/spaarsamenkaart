<?php 
require_once '../application-top.php';
require_once '../includes/navigation-functions.php';
require_once "../qrcode/qrlib.php";

if(!isCompanyUserLogged()){
	 $_SESSION['merchant_login_page']=$_SERVER['REQUEST_URI'];
	 redirectUser(CONF_WEBROOT_URL.'merchant/login.php');
}

if($_GET['used']!=""){
	voucherMarkUsed($_GET['used'],false,true);
	redirectUser('voucher-detail.php?id='.$_GET['used']);
}

/* ------ Insert voucher number start here-------- */
insertVoucherNumbers();
/*   ------ Insert voucher number End Here -------- */

$id = $_GET['id'];
$row_deal = array();
$message = '';
printVoucherDetail($id,$row_deal,$message,'merchant');

if(!isset($message) || $message === null || strlen($message) < 10){
	$msg->addError(t_lang('M_ERROR_INVALID_REQUEST'));
	if(!isset($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER'] == "")
		redirectUser('tipped-members.php');
	redirectUser($_SERVER['HTTP_REFERER']);
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="page-css/voucher-detail.css" />
<script src="<?php echo CONF_WEBROOT_URL;?>js/jquery-1.4.2.js" type="text/javascript"></script>
</head>
<body style="margin:0;padding:0;background-color:#f2f2f2;">
<?php 
 echo  '<table cellspacing="0" cellpadding="0" border="0" bgcolor="#f2f2f2" align="center" width="900">

		<tbody><tr>
		  <td style="text-align:center">';
		  if((isset($_SESSION['errs'][0])) || (isset($_SESSION['msgs'][0]))){ ?> 
				<div class="box" id="messages">
                    <div class="title-msg"> <?php echo t_lang('M_TXT_SYSTEM_MESSAGES');?> <a class="btn gray fr" href="javascript:void(0);" onclick="$(this).closest('#messages').hide(); return false;"><?php echo t_lang('M_TXT_HIDE');?></a></div>
                    <div class="content">
                      <?php if(isset($_SESSION['errs'][0])){?>
                      <div class="redtext"><?php echo $msg->display();?> </div>
                      <br>
                      <br>
					  <?php } 
					  if(isset($_SESSION['msgs'][0])){ 
					  ?>
                      <div class="greentext"> <?php echo $msg->display();?> </div>
                       <?php } ?>
                    </div>
                  </div>
				<?php }
	if($row_deal['active'] == 1 && $row_deal['canUse'] == 1){
		echo '<a href="?used='.$_GET['id'].'" class="btn green" >'.t_lang('M_TXT_MARK_AS_USED').'</a>';
	}else{
		//echo '<a   href="javascript:void(0);" onclick="alert(\'' . t_lang('M_MSG_VOUCHER_IS_NOT_ACTIVE_TO_USE') . '\')" class="btn gray">'.t_lang('M_TXT_MARK_AS_USED').'</a> '; 
	}
	echo '</td></tr></table>'.emailTemplate($message);
	echo '</body></html>';
?>