<?php
require_once '../includes/login-functions.php';
require_once '../includes/conf.php';
session_start();
clearLoggedMerchantLoginCookie();
session_destroy();
$url = CONF_WEBROOT_URL.'merchant/login.php';
header("Location: $url");
?>