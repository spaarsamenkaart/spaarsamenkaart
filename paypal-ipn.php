<?php 
require_once 'application-top.php';

if( !session_id( ) ) session_start();

require_once 'site-classes/order.cls.php';
require_once 'includes/buy-deal-functions.php';

$cart = new Cart();
$post = getPostedData();

$arr = array ( 
         'ott_order_id' => $post['custom'],
         'ott_transaction_id'=>$post['txn_id'],
         'ott_transaction_status'=>(strtoupper($post['payment_status'])=='COMPLETED')?1:0,
         'ott_gateway_response' => var_export($post, true),
    );
	 
if(!$db->insert_from_array('tbl_order_transactions_tracking', $arr)){
	die('Transaction details could not be updated.');
}

$ott_id = $db->insert_id();

$req = 'cmd=_notify-validate';
foreach ($post as $key => $value) {
	$value = urlencode($value);
	$req .= "&$key=$value";
}

// post back to PayPal system to validate
/* $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n";
$header .= "Content-Type: application/x-www-form-urlencoded\r\n";
$header .= "Content-Length: " . strlen($req) . "\r\n\r\n"; */
 $headers  = 'MIME-Version: 1.0' . "\r\n";
			 $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
			 $fromemail = CONF_EMAILS_FROM ;
			$fromname = CONF_EMAILS_FROM_NAME ;
			$headers .= "From: ".$fromname." <".$fromemail.">\r\n";

$rs = $db->query("select * from tbl_payment_options where po_id=1");
$row_paypal_setting = $db->fetch($rs);
//${$row['ps_name']}=(CONF_PAYMENT_PRODUCTION==0)?$row['ps_test_value']:$row['ps_production_value'];

$paypal_url = (CONF_PAYMENT_PRODUCTION==0)?$row_paypal_setting['po_test_address']:$row_paypal_setting['po_address'];

// assign posted variables to local variables
$item_name = $_POST['item_name'];
$item_number = $_POST['item_number'];
$payment_status = $_POST['payment_status'];
$payment_amount = $_POST['mc_gross'];
$payment_currency = $_POST['mc_currency'];
$txn_id = $_POST['txn_id'];
$receiver_email = $_POST['receiver_email'];
$payer_email = $_POST['payer_email'];

$res = validatePaypalIpn( $req );

if($ott_id > 0){
	$db->query("Update tbl_order_transactions_tracking set ott_user_verified='$res' where ott_id=".$ott_id);
}
 	
if ( strcmp ($res, "VERIFIED") == 0 ) {
    $arr=array(
         'ot_order_id'=>$post['custom'],
         'ot_transaction_id'=>$post['txn_id'],
         'ot_transaction_status' =>(strtoupper($post['payment_status'])=='COMPLETED')?1:0,
         'ot_gateway_response'=>var_export($post, true)
    );
	
	if(!$db->insert_from_array('tbl_order_transactions', $arr)){
        die('Transaction details could not be updated. Please contact administrator. Your payment is successful with transaction ID ' . $response->transaction_id);
    }  
	// check the payment_status is Completed
	// check that txn_id has not been previously processed
	// check that receiver_email is your Primary PayPal email
	// check that payment_amount/payment_currency are correct
	// process payment
	
	if(strtoupper($post['payment_status'])=='COMPLETED'){
			   
	    //if(!$db->query("update tbl_orders set order_payment_status=1 where order_id='" . $post['custom'] . "'")) die('Error.');
	    $order=new userOrder();
	    $order->markOrderPaid( $post['custom'] );
		$orderId = $post['custom'];
		 
		################ EMAIL TO USERS#################
		notifyAboutPurchase($orderId);
		################ EMAIL TO USERS#################
		
		$error='';
					
		//addTransaction($post, 'Paypal', $error);
		/*$record=new TableRecord('tbl_order_transactions');
		$record->setFldValue('ot_transaction_id', $post['txn_id']);
		$record->setFldValue('ot_gateway_response', serialize($post));
		$record->addNew();*/
		
	}
	
	$cart->clearCart();
}
?>