<?php
ini_set('memory_limit', '128M');
require_once 'application-top.php';
//header("Content-Type: image/png");
$get  = getQueryStringData();
$actual_image_path = '';
if (!is_numeric($get['id'])) {
    $name = 'defaults.jpg';
} else {
    $rs = $db->query("select admin_avtar from tbl_admin where admin_id=".$get['id']);
    if (!$row = $db->fetch($rs)) {
        $name = 'defaults.jpg';
    } else {
	
        $name = $row['admin_avtar'];
		if(empty($name)){
			$name = 'defaults.jpg';
		}
		
	echo $actual_image_path = realpath(dirname(__FILE__) . '/uploads/admin-images/'. $name);
    }
}

if (!file_exists('uploads/admin-images/'.$name) || $name == '') $name = 'defaults.jpg';

$img = new imageResize('uploads/admin-images/'.$name);

switch (strtoupper($get['type'])) {
	case 'ADMIN':
        $img->setMaxDimensions(300, 300);
        break;
    case 'PROFILE':
         $img->setMaxDimensions(100, 100);
        break;    
	default:
        $img->setMaxDimensions(400,400);
        break;
}

//$img->setResizeMethod(ImageResize::IMG_RESIZE_EXTRA_CROP);
$img->setCropMethod(imageResize::IMG_CROP_BOTH);
showImage($img, $actual_image_path); /* The function is defined to set common headers for images in /includes/site-functions.php */
//$img->displayImage();